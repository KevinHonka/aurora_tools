# AURORA4X TOOLS

This repository contains tools to statistically analyze the Aurora4X DB.

## Generate model
`sqlacodegen sqlite:///database.db`

## Setup
To use this tool, you will currently need Python 3.6 or higher installed on your computer.

1. Clone the project into a folder like:
    
    `git clone -C C:\workspace\auroratools`
    `cd C:\workspace\auroratools`
   


2. we need a virtual environment to not polute the global python installation.

    Windows:
   
    `python -m venv env`

    Linux/MacOS:

    `python3 -m venv env`


3. Activating the venv
    
    On macOS and Linux:

    `source env/bin/activate`

    On Windows:

    `.\env\Scripts\activate`


4. Installing the requirements
   
    `pip install -r requirements.txt`


5. Running the application

    `python -m app`
