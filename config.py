import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  # This is your Project Root
ICON_DIR = os.path.join(ROOT_DIR, 'icons')
EXPORT_TEMPLATE_DIR = os.path.join(ROOT_DIR, 'export_templates')
TEMPLATE_DIR = os.path.join(ROOT_DIR, 'templates')

DB_PATH = os.path.join(ROOT_DIR, '', 'ressources', 'AuroraDB.db')

GAME_NAME = "United Earth"
RACE_NAME = "United Earth"
START_DATE = "01-01-2020"
SEARCHT_SEPERATOR = ","


def init_config():
    pass


def get_icon(icon_name, file_ending="png"):
    icon_path = os.path.join(ICON_DIR, f"{icon_name}.{file_ending}")

    if os.path.exists(icon_path):
        return icon_path
    else:
        raise Exception(f"Icon not found in {icon_path}")
