from app.database import get_session
from app.database.models import DIMTraitsList


def get_traits():
    session = get_session()

    traits = session.query(DIMTraitsList).all()

    return traits
