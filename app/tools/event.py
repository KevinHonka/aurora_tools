from sqlalchemy import asc

from app.database import get_session
from app.database.models import DIMEventType, t_FCT_GameLog
from app.tools.config import get_config
from app.tools.filters import event_filter, get_event_type
from app.tools.utils import correct_time


def get_types():
    session = get_session()

    return session.query(DIMEventType).all()


def get_events(filters=None):
    session = get_session()
    config = get_config()

    query = t_FCT_GameLog.select().where(t_FCT_GameLog.c.GameID == config.game.GameID).where(
                                         t_FCT_GameLog.c.RaceID == config.race.RaceID)

    if filters:
        query = event_filter(query, filters)

    return session.execute(query)


def get_eventlog(event_filters=None):

    event_list = []

    events = get_events(filters=event_filters)

    for event in events:
        event_type = get_event_type(event_type_id=event.EventType)

        time = correct_time(event.Time)

        event_list.append({'event': event_type.Description,
                           'message': event.MessageText,
                           'date': time})

    return event_list
