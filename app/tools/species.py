from app.database import get_session
from app.database.models import FCTSpecies, FCTPopulation
from app.tools.config import get_config


def get_species(species_id=None):
    session = get_session()
    config = get_config()

    return session.query(FCTSpecies).filter(FCTSpecies.GameID == config.game.GameID,
                                            FCTSpecies.SpeciesID == species_id).one()


def get_list():
    session = get_session()
    config = get_config()

    subquery = session.query(FCTPopulation).filter(FCTPopulation.GameID == config.game.GameID,
                                                   FCTPopulation.RaceID == config.race.RaceID).subquery()
    species = session.query(FCTSpecies).filter(FCTSpecies.GameID == config.game.GameID,
                                               FCTSpecies.SpeciesID == subquery.c.SpeciesID).all()

    return species
