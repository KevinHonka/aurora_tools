import time
from datetime import datetime

from sqlalchemy.orm import Query

from app.tools.component import get_component_types
from app.tools.config import Config, get_config
from app.tools.event import get_types as get_event_types
from app.tools.game import get_game
from app.tools.medal import get_medals
from app.tools.race import get_race
from app.tools.rank import get_ranks
from app.tools.species import get_species, get_list as get_species_list
from app.tools.tech import get_race_tech, get_tech_types


# Init tools
def init_tools(race_name=None, game_name=None):
    config = get_config()

    if not race_name:
        race_name = config.RACE_NAME

    if not game_name:
        game_name = config.GAME_NAME

    config.game = get_game(game_name)
    config.race = get_race(race_name, config.game.GameID)
    config.event_types = get_event_types()
    config.race_tech = get_race_tech()
    config.tech_types = get_tech_types()
    config.component_types = get_component_types()
    config.medals = get_medals()
    config.ranks = get_ranks()
    config.species_list = get_species_list()

















