from app.database import get_session, FCTGame


def get_game(game_name: str):
    session = get_session()

    game = session.query(FCTGame).filter_by(GameName=game_name).one()

    return game


def get_list():
    session = get_session()

    return session.query(FCTGame).all()
