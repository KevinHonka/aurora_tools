from app.database import get_session
from app.database.models import FCTRaceMedal
from app.tools.config import get_config


def get_medals():
    session = get_session()
    config = get_config()

    return session.query(FCTRaceMedal).filter(FCTRaceMedal.GameID == config.game.GameID,
                                              FCTRaceMedal.RaceID == config.race.RaceID).all()
