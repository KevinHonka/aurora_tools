from app.database import get_session
from app.database.models import FCTPopulationInstallation, DIMPlanetaryInstallation, FCTPopulation, FCTRace, FCTGame
from app.tools.config import get_config
from app.tools.models import Race, Colony
from app.tools.utils import get_system_name, get_ressources


def get_installations(pop_id=None):
    session = get_session()
    config = get_config()

    installation_list = []

    installations = session.query(FCTPopulationInstallation).filter(
        FCTPopulationInstallation.GameID == config.game.GameID,
        FCTPopulationInstallation.PopID == pop_id).all()

    for installation in installations:
        installation_name = session.query(DIMPlanetaryInstallation).filter(
            DIMPlanetaryInstallation.PlanetaryInstallationID == installation.PlanetaryInstallationID).one().Name

        installation_list.append({'Amount': installation.Amount, 'Name': installation_name})

    return installation_list


def get_race_stats():
    session = get_session()
    config = get_config()

    race_stats = Race()

    colonies = session.query(FCTPopulation).filter(FCTPopulation.GameID == config.game.GameID,
                                                   FCTPopulation.RaceID == config.race.RaceID,
                                                   FCTPopulation.Population > 0).all()

    race_stats.name = config.race.RaceTitle
    race_stats.wealth = config.race.WealthPoints

    for colony in colonies:
        system_name = get_system_name(colony.SystemID)

        if system_name not in race_stats.colonies.keys():
            race_stats.colonies[system_name] = []

        installations = get_installations(pop_id=colony.PopulationID)
        ressources = get_ressources(colony)

        race_stats.colonies[system_name].append(
            Colony(colony.PopName, colony.Population, colony.Capital, installations, ressources))

    return race_stats


def get_race(race_name: str, game_id: int):
    config = get_config()

    session = get_session()

    query = session.query(FCTRace).filter(FCTRace.RaceTitle == race_name, FCTRace.NPR != 1)

    if game_id:
        query = query.filter(FCTRace.GameID == game_id)

    race = query.one_or_none()
    if not race:
        raise Exception("Please specify a valid race!")

    return race


def get_list(game_name=None, game_id=None):
    session = get_session()

    query = session.query(FCTRace).filter(FCTRace.NPR != 1)

    if game_name:
        subquery = session.query(FCTGame).filter(FCTGame.GameName == game_name).subquery()
        query = query.filter(FCTRace.GameID == subquery.c.GameID)

    if game_id:
        query = query.filter(FCTRace.GameID == game_id)

    return query.all()

