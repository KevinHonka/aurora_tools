from sqlalchemy import asc, desc

from app.database import get_session
from app.database.models import DIMTechType, FCTRaceTech, FCTTechSystem
from app.tools.config import get_config


def get_tech_type(tech_type_id: int = None, tech_type_name: str = None):
    config = get_config()
    for tech_type in config.tech_types:
        if tech_type_id and tech_type['id'] == tech_type_id:
            return tech_type
        if tech_type_name and tech_type['description'] == tech_type_name:
            return tech_type


def get_tech_types():
    session = get_session()

    tech_list = []

    for tech_type in session.query(DIMTechType).order_by(asc(DIMTechType.TechTypeID)).all():
        tech_list.append({'id': tech_type.TechTypeID, 'description': tech_type.Description})

    return tech_list


def get_tech(tech_id: int):
    session = get_session()
    config = get_config()

    subquery = session.query(FCTRaceTech.TechID).filter(FCTRaceTech.RaceID == config.race.RaceID,
                                                        FCTRaceTech.GameID == config.game.GameID).subquery()
    query = session.query(FCTTechSystem).filter(FCTTechSystem.TechSystemID == subquery.c.TechID,
                                                FCTTechSystem.TechTypeID == tech_id).order_by(
        desc(FCTTechSystem.TechSystemID)).limit(1)

    tech = query.one()

    return tech


def get_armor():
    session = get_session()
    config = get_config()

    armors = {}

    subquery = session.query(FCTRaceTech.TechID).filter(FCTRaceTech.RaceID == config.race.RaceID,
                                                        FCTRaceTech.GameID == config.game.GameID).subquery()
    query = session.query(FCTTechSystem).filter(FCTTechSystem.TechSystemID == subquery.c.TechID,
                                                FCTTechSystem.TechTypeID == 84,
                                                FCTTechSystem.AdditionalInfo2 != 0).order_by(
        asc(FCTTechSystem.TechSystemID))

    armor_list = query.all()

    for armor in armor_list:
        armors[armor.Name] = armor

    config.armors = armors

    return armors


def get_race_tech():
    session = get_session()
    config = get_config()

    tech_list = {}

    racetech = session.query(FCTRaceTech).filter(FCTRaceTech.RaceID == config.race.RaceID,
                                                 FCTRaceTech.GameID == config.game.GameID).all()

    for tech in racetech:
        tech_list[tech.TechID] = tech.Obsolete

    return tech_list
