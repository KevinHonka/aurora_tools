import sqlalchemy
from sqlalchemy import asc

from multiprocessing import Process, Value, Manager

from app.database import get_session
from app.database.models import FCTCommander, FCTCommanderMedal, FCTCommanderHistory, FCTCommanderTraits
from app.tools.config import get_config
from app.tools.filters import commander_filter, commandtype_filter
from app.tools.medal import get_medals
from app.tools.models import Commander, Medal, CommanderHistory
from app.tools.rank import get_rank
from app.tools.ship import get_ship_name_from_shipid
from app.tools.species import get_species
from app.tools.trait import get_traits
from app.tools.utils import get_location_from_popid, get_groundunit_from_unitid, correct_time


def get_commanders(filters=None):
    session = get_session()
    config = get_config()
    com_list = []

    query = session.query(FCTCommander).filter(FCTCommander.GameID == config.game.GameID,
                                               FCTCommander.RaceID == config.race.RaceID)

    if filters:
        query = commander_filter(query, filters)

    commanders = query.all()

    for commander in commanders:

        assignment = get_assignment(commandtype_filter(commandtype_id=commander.CommandType), commander.CommandID)

        history = get_commander_history(commander.CommanderID)
        species = get_species(commander.SpeciesID)
        medal_list = get_commander_medals(commander.CommanderID)
        traits = get_commander_traits(commander.CommanderID)

        try:
            rank = get_rank(commander.RankID)
        except sqlalchemy.orm.exc.NoResultFound:
            rank = None

        com_list.append(Commander(commander.Name,
                                  rank,
                                  history,
                                  medal_list,
                                  species.SpeciesName,
                                  commander.Retired,
                                  traits,
                                  cmdr_type=commander.CommanderType,
                                  assignment=assignment))

    return com_list


def get_assignment(commandtype, command_id):
    if not commandtype or commandtype['id'] == 0:
        assignment = "unassigned"
    elif commandtype['id'] == 1:
        assignment = f"{commandtype['name']} {get_ship_name_from_shipid(command_id)}"
    elif commandtype['id'] == 3:
        assignment = f"{commandtype['name']} {get_location_from_popid(command_id)}"
    elif commandtype['id'] == 5:
        assignment = f"{commandtype['name']} {get_groundunit_from_unitid(command_id)}"
    elif commandtype['id'] == 7:
        assignment = f"{commandtype['name']}"
    elif commandtype['id'] == 8:
        assignment = f"{commandtype['name']} {get_ship_name_from_shipid(command_id)}"
    elif commandtype['id'] == 9:
        assignment = f"{commandtype['name']} {get_ship_name_from_shipid(command_id)}"
    elif commandtype['id'] == 10:
        assignment = f"{commandtype['name']} {get_ship_name_from_shipid(command_id)}"
    elif commandtype['id'] == 11:
        assignment = f"{commandtype['name']} {get_ship_name_from_shipid(command_id)}"
    elif commandtype['id'] == 12:
        assignment = "{commandtype['name']}"
    elif commandtype['id'] == 17:
        assignment = f"{commandtype['name']} {get_location_from_popid(command_id)}"
    else:
        assignment = "unknown"

    return assignment


def get_commander_traits(commander_id):
    session = get_session()
    config = get_config()
    traits = get_traits()

    traits_list = []

    cmdr_trait_list = session.query(FCTCommanderTraits).filter(FCTCommanderTraits.GameID == config.game.GameID,
                                                               FCTCommanderTraits.CmdrID == commander_id)

    for cmdr_trait in cmdr_trait_list:
        for trait in traits:
            if cmdr_trait.TraitID == trait.TraitID:
                traits_list.append(trait.Name)
                break

    return traits_list


def get_commander_medals(commander_id):
    session = get_session()
    medal_list = []

    commander_medals = session.query(FCTCommanderMedal).filter(FCTCommanderMedal.CommanderID == commander_id).all()
    medals = get_medals()

    for c_medal in commander_medals:
        for medal in medals:
            if c_medal.MedalID == medal.MedalID:
                medal_list.append(Medal(medal.MedalName, c_medal.AwardReason))

    return medal_list


def get_commander_history(commander_id):
    session = get_session()
    com_history = []

    history = session.query(FCTCommanderHistory).filter(
        FCTCommanderHistory.CommanderID == commander_id).order_by(asc(FCTCommanderHistory.GameTime),
                                                                  asc(FCTCommanderHistory.CommanderID)).all()

    for entry in history:
        com_history.append(CommanderHistory(entry.HistoryText, correct_time(entry.GameTime)))

    return com_history
