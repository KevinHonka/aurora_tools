from app.database import FCTGame
from app.database.models import FCTRace


class Config:
    RACE_NAME: str = None
    GAME_NAME: str = None
    START_DATE: str = None
    game: FCTGame = None
    race: FCTRace = None
    event_types: list = None
    race_tech: dict = None
    tech_types: list = None
    component_types: list = None
    traits = []
    medals: list = []
    weapons: dict = {}
    armors: dict = {}
    ranks: dict = {}
    species_list: list = {}


def get_config():
    global __c

    if not __c:
        __c = Config()
    return __c


__c = Config()
