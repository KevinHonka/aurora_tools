import config
from app.database import get_session
from app.database.models import FCTSpecies, FCTCommander, FCTRank, FCTFleet, FCTShipDesignComponent, t_FCT_GameLog, \
    FCTShipClass, FCTShip, FCTClassComponent

weapon_type = {"Laser": 15, "Particle Beam": 30, "Railgun": 35, "Gauss Cannon": 45}

commandtypes = [
    {"name": "unassigned", "id": 0},
    {"name": "Commanding Officer", "id": 1},
    {"name": "Planetary Governor", "id": 3},
    {"name": "Unit Commander", "id": 5},
    {"name": "Scientist on Project", "id": 7},
    {"name": "Executive Officer", "id": 8},
    {"name": "Chief Engineer", "id": 9},
    {"name": "Science Officer", "id": 10},
    {"name": "Tactical Officer", "id": 11},
    {"name": "Command Position", "id": 12},
    {"name": "Academy Commandant", "id": 17}
]


def commander_filter(query, filters):
    session = get_session()

    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == "name":
            query = query.filter(FCTCommander.Name.like(f'%{filter_value}%'))
        if filter_name.lower() == "species":
            sub_species = session.query(FCTSpecies).subquery()
            query = query.outerjoin(sub_species, FCTCommander.SpeciesID == sub_species.c.SpeciesID).filter(
                FCTSpecies.SpeciesName.like(f"%{filter_value}%"))
        if filter_name.lower() == "rank":
            sub_rank = session.query(FCTRank).filter(FCTRank.RankName == filter_value).subquery()
            query = query.filter(FCTCommander.RankID == sub_rank.c.RankID)
        if filter_name.lower() == "type":
            query = query.filter(FCTCommander.CommandType == commandtype_filter(command_name=filter_value)['id'])

    return query


def commandtype_filter(commandtype_id=None, command_name=None):
    if commandtype_id:
        for commandtype in commandtypes:
            if commandtype['id'] == commandtype_id:
                return commandtype

    if command_name:
        for commandtype in commandtypes:
            if commandtype['name'] == command_name:
                return commandtype


def shipclass_filter(query, filters):
    from app.tools import get_config
    session = get_session()
    local_config = get_config()

    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == "id":
            query = query.filter(FCTShipClass.ShipClassID == filter_value)

        if filter_name.lower() == 'name':
            query = query.filter(FCTShipClass.ClassName.like(f'%{filter_value}%'))

        if filter_name.lower() == 'component':
            component_query = session.query(FCTShipDesignComponent).filter(
                FCTShipDesignComponent.Name == filter_value).subquery()
            component_ship_query = session.query(FCTClassComponent).filter(
                FCTClassComponent.ComponentID == component_query.c.SDComponentID).subquery()
            query = query.filter(FCTShipClass.RaceID == local_config.race.RaceID,
                                 FCTShipClass.GameID == local_config.game.GameID,
                                 FCTShipClass.ShipClassID == component_ship_query.c.ClassID)

    return query


def fleet_filter(query, filters):
    from app.tools import get_config
    session = get_session()
    local_config = get_config()

    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == 'name':
            query = query.filter(FCTFleet.FleetName.like(f'%{filter_value}%'))

        if filter_name.lower() == 'ship_class':
            shipclass_query = session.query(FCTShipClass).filter(FCTShipClass.RaceID == local_config.race.RaceID,
                                                                 FCTShipClass.GameID == local_config.game.GameID,
                                                                 FCTShipClass.ClassName == filter_value).subquery()
            ship_query = session.query(FCTShip).filter(FCTShip.ShipClassID == shipclass_query.c.ShipClassID).subquery()
            query = query.filter(FCTFleet.FleetID == ship_query.c.FleetID)

        if filter_name.lower() == 'commercial' and filter_value == 'True':
            query = query.filter(FCTFleet.CivilianFunction == 0)

    return query


def component_filter(query, filters):
    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == 'id':
            query = query.filter(FCTShipDesignComponent.SDComponentID == filter_value)
        if filter_name.lower() == 'name':
            query = query.filter(FCTShipDesignComponent.Name.like(f"%{filter_value}%"))
        if filter_name.lower() == 'weapontype':
            query = query.filter(FCTShipDesignComponent.ComponentTypeID == weapon_type[filter_value])
        if filter_name.lower() == 'turret':
            query = query.filter(FCTShipDesignComponent.TurretWeaponID == filter_value)
        if filter_name.lower() == 'beamweapon':
            query = query.filter(FCTShipDesignComponent.BeamWeapon == filter_value)

    return query


def event_filter(query, filters):
    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == "text":
            query = query.where(t_FCT_GameLog.c.MessageText.like(f"%{filter_value}%"))
        if filter_name.lower() == "type":
            query = query.where(t_FCT_GameLog.c.EventType == get_event_type(event_type_name=filter_value).EventTypeID)

    return query


def get_event_type(event_type_id=None, event_type_name=None):
    from app.tools import get_config
    config = get_config()

    for event_type in config.event_types:
        if event_type.EventTypeID == event_type_id:
            return event_type
        if event_type.Description == event_type_name:
            return event_type
    return None


def obsolete_filter(filters=None):
    for filter_data in filters.split(config.SEARCHT_SEPERATOR):
        filter_name, filter_value = filter_data.split("=")

        if filter_name.lower() == "obsolete":
            if filter_value.lower() == "true":
                return True
            elif filter_value.lower() == "false":
                return False
            else:
                return None
        else:
            return None
