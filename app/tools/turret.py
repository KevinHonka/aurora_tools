import math

from sqlalchemy import desc
from sqlalchemy.orm import make_transient

from app.database import get_session
from app.database.models import FCTShipDesignComponent, FCTTechSystem, FCTRaceTech
from app.tools.component import get_component_type
from app.tools.config import get_config
from app.tools.models import Turret
from app.tools.tech import get_tech, get_tech_type


def get_turret(selected_weapon, tracking_speed: int, number_weapons: int, armor_amount: int, selected_armor: str):
    session = get_session()
    config = get_config()

    try:
        weapon = session.query(FCTShipDesignComponent).filter(FCTShipDesignComponent.GameID == config.game.GameID,
                                                              FCTShipDesignComponent.SDComponentID == config.weapons[
                                                                  selected_weapon].SDComponentID).one()
    except Exception as ex:
        raise ex

    turret = Turret()

    turret.weapon = weapon

    tracking_tech = get_tech(75)

    gear_percent = tracking_speed / tracking_tech.AdditionalInfo * 10
    gear_size = weapon.Size * gear_percent / 100

    turret_crew = weapon.Crew

    if number_weapons == 1:
        turret_option = 1
    elif number_weapons == 2:
        turret_option = 2
        gear_size *= 0.9
        turret_crew *= 1.8
    elif number_weapons == 3:
        turret_option = 3
        gear_size *= 0.85
        turret_crew *= 2.55
    else:
        turret_option = 4
        gear_size *= 0.8
        turret_crew *= 3.2

    turret.crew = round(turret_crew)
    turret.gear_size = gear_size
    turret.gear_percent = gear_percent
    turret.number_weapons = turret_option
    turret.armor_cost = calculate_armor_cost(weapon.Size * turret.number_weapons, armor_amount)
    turret.armor_size = calculate_armor_size(weapon.Size * turret.number_weapons, armor_amount,
                                             config.armors[selected_armor].AdditionalInfo)

    if get_component_type(component_type_id=weapon.ComponentTypeID).TypeDescription == 'Gauss Cannon':
        maxRange = weapon.RangeModifier
    elif get_component_type(component_type_id=weapon.ComponentTypeID).TypeDescription == 'Microwave':
        maxRange = weapon.RangeModifier
    elif get_component_type(component_type_id=weapon.ComponentTypeID).TypeDescription == 'Particle Beam':
        maxRange = weapon.RangeModifier
    elif get_component_type(component_type_id=weapon.ComponentTypeID).TypeDescription == 'Railgun':
        maxRange = weapon.RangeModifier * weapon.DamageOutput
    else:
        maxRange = weapon.RangeModifier * weapon.PowerRequirement

    turret.max_range = maxRange

    turret.size = weapon.Size * number_weapons + gear_size + turret.armor_size
    turret.cost = (weapon.Cost * number_weapons) + (gear_size * 5) + turret.armor_cost

    turret.hit_to_kill = (weapon.HTK + armor_amount) * number_weapons

    return turret


def add_turret(turret_name, selected_weapon, tracking_speed: int, number_weapons: int, armor_amount: int,
               selected_armor: str, instant: bool):
    session = get_session()
    config = get_config()

    turret: Turret = get_turret(selected_weapon, tracking_speed, number_weapons, armor_amount, selected_armor)
    tech_system: FCTTechSystem = session.query(FCTTechSystem).filter(
        FCTTechSystem.TechSystemID == turret.weapon.SDComponentID).order_by(desc(FCTTechSystem.TechTypeID)).limit(
        1).one()
    session.expunge(tech_system)
    make_transient(tech_system)

    tech_system.TechSystemID = None
    tech_system.Name = turret_name
    tech_system.TechTypeID = get_tech_type(tech_type_name='RD Turret')['id']
    tech_system.DevelopCost = round(turret.cost * 10)
    tech_system.TechDescription = "Race-designed Turret"

    session.add(tech_system)
    session.commit()

    design_component = session.query(FCTShipDesignComponent).filter(
        FCTShipDesignComponent.SDComponentID == turret.weapon.SDComponentID).one()
    session.expunge(design_component)
    make_transient(design_component)

    design_component.SDComponentID = tech_system.TechSystemID
    design_component.Name = turret_name
    design_component.Crew = turret.crew
    design_component.Size = turret.size
    design_component.Cost = turret.cost
    design_component.PowerRequirement *= turret.number_weapons
    design_component.RechargeRate *= turret.number_weapons
    design_component.TrackingSpeed = tracking_speed
    design_component.HTK = turret.hit_to_kill
    design_component.NumberOfShots *= turret.number_weapons
    design_component.Duranium = (turret.weapon.Duranium * turret.number_weapons) + (turret.gear_size * 5)
    design_component.Neutronium = turret.armor_cost
    design_component.Corbomite *= turret.number_weapons
    design_component.Tritanium *= turret.number_weapons
    design_component.Boronide *= turret.number_weapons
    design_component.Mercassium *= turret.number_weapons
    design_component.Vendarite *= turret.number_weapons
    design_component.Sorium *= turret.number_weapons
    design_component.Uridium *= turret.number_weapons
    design_component.Corundium *= turret.number_weapons
    design_component.Gallicite *= turret.number_weapons
    design_component.TurretWeaponID = turret.weapon.SDComponentID

    session.add(design_component)
    session.commit()

    if instant:
        race_tech = FCTRaceTech()

        race_tech.RaceID = config.race.RaceID
        race_tech.GameID = config.game.GameID
        race_tech.Obsolete = 0
        race_tech.TechID = design_component.SDComponentID

        session.add(race_tech)
        session.commit()

    return design_component


def calculate_rate_of_fire(weapon: FCTShipDesignComponent):
    if weapon.RechargeRate:
        rof = (25 * weapon.PowerRequirement) / (5 * weapon.RechargeRate)
        if rof % 5 > 0:
            rof = rof + 5 - (rof % 5)
    else:
        rof = weapon.NumberOfShots
    return rof


def calculate_armor_size(hull_size, turret_armor_level, armor_level):
    armor_size = turret_armor_level * (
            math.pow(math.pow(hull_size * 0.75 / math.pi, 1.0 / 3.0), 2.0) * 4 * math.pi / 4.0) / armor_level
    return armor_size


def calculate_armor_cost(hull_size, turret_armor_level):
    armor_size = turret_armor_level * (
            math.pow(math.pow(hull_size * 0.75 / math.pi, 1.0 / 3.0), 2.0) * 4 * math.pi / 4.0) / 10
    armor_cost = armor_size * 10
    return armor_cost
