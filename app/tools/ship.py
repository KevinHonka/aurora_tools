from app.database import get_session
from app.database.models import FCTShipClass, FCTShip
from app.tools.component import get_component, get_component_type
from app.tools.config import get_config
from app.tools.filters import shipclass_filter
from app.tools.models import Shipdesign


def get_shipclass_ids():
    session = get_session()
    config = get_config()

    return session.query(FCTShipClass).filter(FCTShipClass.GameID == config.game.GameID,
                                              FCTShipClass.RaceID == config.race.RaceID).all()


def get_shipclass(filters=None):
    from app.tools.component import get_component_ids
    session = get_session()
    config = get_config()

    shipclass_list = []

    query = session.query(FCTShipClass).filter(FCTShipClass.GameID == config.game.GameID,
                                               FCTShipClass.RaceID == config.race.RaceID)

    if filters:
        query = shipclass_filter(query, filters)

    shipclasses = query.all()

    for shipclass in shipclasses:
        component_list = {}

        class_components = get_component_ids(shipclass_id=shipclass.ShipClassID)

        for class_component in class_components:

            component = get_component(class_component.ComponentID)

            component_type = get_component_type(component.ComponentTypeID)

            if component_type.TypeDescription in component_list.keys():
                component_list[component_type.TypeDescription].append(component)
            else:
                component_list[component_type.TypeDescription] = [component]

        shipclass_list.append(Shipdesign(name=shipclass.ClassName, components=component_list))

    return shipclass_list


def get_ship_name_from_shipid(ship_id):
    session = get_session()
    config = get_config()

    ship = session.query(FCTShip).filter(FCTShip.GameID == config.game.GameID,
                                         FCTShip.RaceID == config.race.RaceID,
                                         FCTShip.ShipID == ship_id).one()

    ship_name = f"{ship.ShipName}"

    return ship_name
