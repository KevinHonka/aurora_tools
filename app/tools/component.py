from sqlalchemy import asc

from app.database import get_session
from app.database.models import DIMComponentType, FCTShipDesignComponent, FCTRaceTech, FCTClassComponent, FCTShipClass
from app.tools.config import get_config
from app.tools.filters import component_filter, obsolete_filter
from app.tools.models import ShipComponent


def get_component_types():
    session = get_session()

    return session.query(DIMComponentType).all()


def get_component_type(component_type_id: int = None, component_type_name: str = None):
    config = get_config()

    for component_type in config.component_types:
        if component_type_id == component_type.ComponentTypeID:
            return component_type
        elif component_type_name == component_type.TypeDescription:
            return component_type
    return None


def get_component(component_id):
    session = get_session()
    config = get_config()

    query = session.query(FCTShipDesignComponent).filter(FCTShipDesignComponent.SDComponentID == component_id)

    return query.one()


def get_components(filters=None):
    session = get_session()
    config = get_config()

    if filters:
        obsolete = obsolete_filter(filters)
    else:
        obsolete = None

    component_list = []

    shipclass_ids = []

    shipclass_list = session.query(FCTShipClass).filter(FCTShipClass.GameID == config.game.GameID,
                                                       FCTShipClass.RaceID == config.race.RaceID).all()

    for shipclass in shipclass_list:
        shipclass_ids.append(shipclass.ShipClassID)

    component_ids = get_component_ids(shipclass_ids=shipclass_ids)

    query = session.query(FCTShipDesignComponent).filter(FCTShipDesignComponent.GameID == config.game.GameID,
                                                         FCTShipDesignComponent.SDComponentID.in_(component_ids))
    if filters:
        query = component_filter(query, filters)

    components = query.order_by(asc(FCTShipDesignComponent.Name)).all()

    for component in components:
        ship_component = ShipComponent(component.Name, get_component_status(component.SDComponentID))
        if obsolete is None:
            component_list.append(ship_component)
        else:
            if ship_component.obsolete == obsolete:
                component_list.append(ship_component)

    return component_list


def get_component_status(component_id):
    session = get_session()
    config = get_config()

    racetech = session.query(FCTRaceTech).filter(FCTRaceTech.RaceID == config.race.RaceID,
                                                 FCTRaceTech.GameID == config.game.GameID,
                                                 FCTRaceTech.TechID == component_id).one()

    if racetech.Obsolete == 1:
        return True
    else:
        return False


def get_component_ids(shipclass_id=None, shipclass_ids=None):
    session = get_session()
    config = get_config()

    class_components = []

    query = session.query(FCTClassComponent)

    if shipclass_id:
        query = query.filter(FCTClassComponent.GameID == config.game.GameID,
                             FCTClassComponent.ClassID == shipclass_id)

        return query.all()

    elif shipclass_ids:
        query = query.filter(FCTClassComponent.GameID == config.game.GameID,
                             FCTClassComponent.ClassID.in_(shipclass_ids))

        for comp in query.all():
            class_components.append(comp.ComponentID)

        return class_components


def get_weapons(filters=None):
    session = get_session()
    config = get_config()

    query = session.query(FCTShipDesignComponent).filter(FCTShipDesignComponent.GameID == config.game.GameID,
                                                         FCTShipDesignComponent.SDComponentID.in_(
                                                             list(config.race_tech.keys())))

    if filters:
        query = component_filter(query, f"weapontype={filters},turret=0,beamweapon=1")

        weapon_list = query.all()

        weapons = {}

        for weapon in weapon_list:
            weapons[weapon.Name] = weapon

        config.weapons = weapons

    return weapons
