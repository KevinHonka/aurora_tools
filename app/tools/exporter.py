import jinja2

import config
from app.tools.commander import get_commanders
from app.tools.component import get_components
from app.tools.event import get_eventlog
from app.tools.race import get_race_stats
from app.tools.ship import get_shipclass
from app.tools.shipyard import get_shipyards


def export_empire(export_dir, template_name="empire"):
    template_loader = jinja2.FileSystemLoader(config.EXPORT_TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    race = get_race_stats()
    shipyards = get_shipyards()

    template = template_env.get_template(f"{template_name}.j2")
    output = template.render(race=race, shipyards=shipyards)

    with open(f"{export_dir}/{template_name}.txt", "w+") as file_handler:
        file_handler.write(output)


def export_commanders(export_dir, search_filter, template_name="commanders"):
    template_loader = jinja2.FileSystemLoader(config.EXPORT_TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    commanders = get_commanders(search_filter)

    template = template_env.get_template(f"{template_name}.j2")
    output = template.render(commanders=commanders)

    with open(f"{export_dir}/{template_name}.txt", "w+") as file_handler:
        file_handler.write(output)


def export_events(export_dir, search_filter, template_name="events"):
    template_loader = jinja2.FileSystemLoader(config.EXPORT_TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    events = get_eventlog(event_filters=search_filter)

    template = template_env.get_template(f"{template_name}.j2")
    output = template.render(events=events)

    with open(f"{export_dir}/{template_name}.txt", "w+") as file_handler:
        file_handler.write(output)


def export_components(export_dir, search_filter, template_name="components"):
    template_loader = jinja2.FileSystemLoader(config.EXPORT_TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    components = get_components(search_filter)

    template = template_env.get_template(f"{template_name}.j2")
    output = template.render(components=components)

    with open(f"{export_dir}/{template_name}.txt", "w+") as file_handler:
        file_handler.write(output)


def export_shipclasses(export_dir, search_filter, template_name="shipclass"):
    template_loader = jinja2.FileSystemLoader(config.EXPORT_TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    shipclasses = get_shipclass(search_filter)

    template = template_env.get_template(f"{template_name}.j2")
    output = template.render(shipclasses=shipclasses)

    with open(f"{export_dir}/{template_name}.txt", "w+") as file_handler:
        file_handler.write(output)
