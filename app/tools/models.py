from datetime import datetime

from app.database.models import FCTShipDesignComponent


class Shipyard:
    name: str = None
    location: str = None
    capacity: float = None
    tooled_for: str = None
    slipways: int = None

    def __init__(self, name, location, capacity, tooled_for, slipways):
        self.name = name
        self.location = location
        self.capacity = capacity
        self.tooled_for = tooled_for
        self.slipways = slipways


class Rank:
    name: str = None
    abbrev: str = None
    rank_type: int = None

    def __init__(self, name, abbrev, rank_type):
        self.name = name
        self.abbrev = abbrev
        self.rank_type = rank_type


class CommanderHistory:
    text: str = None
    time: datetime = None

    def __init__(self, text, time):
        self.text = text
        self.time = time


class Commander:
    name: str = None
    species: str = None
    rank: Rank = None
    medals: list = None
    history: list = None
    traits: list = None
    assignment: str = None

    __status = "unknown"
    __cmdr_type = None

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, status):

        if status == 1:
            self.__status = "retired"
        elif status == 0:
            self.__status = "active"
        else:
            self.__status = "dead"

    @property
    def cmdr_type(self):
        return self.__cmdr_type

    @cmdr_type.setter
    def cmdr_type(self, cmdr_type):

        if cmdr_type == 0:
            self.__cmdr_type = "Naval Commander"
        elif cmdr_type == 1:
            self.__cmdr_type = "Ground Force Commander"
        elif cmdr_type == 2:
            self.__cmdr_type = "Administrator"
        elif cmdr_type == 3:
            self.__cmdr_type = "Scientist"
        else:
            self.__cmdr_type = cmdr_type

    def __init__(self, name, rank: Rank, history, medals: list, species, status, traits, cmdr_type, assignment):
        self.name = name
        self.rank = rank
        self.medals = medals
        self.history = history
        self.species = species
        self.status = status
        self.traits = traits
        self.cmdr_type = cmdr_type
        self.assignment = assignment


class Ship:
    name: str = None
    shipclass: str = None

    def __init__(self, name, shipclass):
        self.name = name
        self.shipclass = shipclass


class Race:
    name: str = None
    wealth: float = None
    colonies: dict = {}

    @property
    def population(self):
        pop = 0
        for system_name, colony_list in self.colonies.items():
            for colony in colony_list:
                pop += colony.population

        return pop


class Medal:
    name: str = None
    reason: str = None

    def __init__(self, name, reason):
        self.name = name
        self.reason = reason


class Colony:
    name: str = None
    population: float = None
    capital: bool = False
    installations: list = []
    ressources: dict = {}

    def __init__(self, name, population, capital, installations, ressources):
        self.name = name
        self.population = population
        self.installations = installations
        self.ressources = ressources

        if capital == 1:
            self.capital = True


class ShipComponent:
    name: str = None
    __obsolete: bool = False

    @property
    def obsolete(self):
        return self.__obsolete

    @obsolete.setter
    def obsolete(self, obsolete):

        if obsolete == 1:
            self.__obsolete = True
        elif obsolete == 0:
            self.__obsolete = False
        else:
            self.__obsolete = obsolete

    def __init__(self, name, obsolete):
        self.name = name
        self.obsolete = obsolete


class Shipdesign:
    name: str = None
    components: dict = None

    def __init__(self, name=None, components={}):
        self.name = name
        self.components = components


class Weapon:
    crew: int = None
    size: int = None
    cost: int = None
    capacitor: int = None
    power: int = None
    rechargeRate: int = None
    htk: int = None
    damage: int = None
    shots: int = None
    rangeMod: int = None
    duranium: int = None
    neutronium: int = None
    corbomite: int = None
    tritanium: int = None
    boronide: int = None
    mercassium: int = None
    vendarite: int = None
    sorium: int = None
    uridium: int = None
    corundium: int = None
    gallicite: int = None
    materials: list = None
    componentValue: int = None


class Turret:
    gear_size: float = None
    gear_percent: float = None

    armor_cost: float = None
    armor_size: float = None

    max_range: int = None
    size: float = None
    cost: float = None
    crew: float = None

    number_weapons: int = None
    hit_to_kill: int = None

    weapon: FCTShipDesignComponent = None
