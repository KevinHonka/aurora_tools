from app.database import get_session
from app.database.models import FCTShipyard
from app.tools.config import get_config
from app.tools.models import Shipyard
from app.tools.ship import get_shipclass
from app.tools.utils import get_location_from_popid


def get_shipyards():
    session = get_session()
    config = get_config()
    shipyard_list = []

    shipyards = session.query(FCTShipyard).filter(FCTShipyard.GameID == config.game.GameID,
                                                  FCTShipyard.RaceID == config.race.RaceID).all()

    for shipyard in shipyards:
        location = get_location_from_popid(shipyard.PopulationID)

        if shipyard.BuildClassID != 0:
            ship_class = get_shipclass(f"id={shipyard.BuildClassID}")[0].ClassName
        else:
            ship_class = "None"

        shipyard_list.append(Shipyard(shipyard.ShipyardName,
                                      location,
                                      shipyard.Capacity,
                                      ship_class,
                                      shipyard.Slipways))
    return shipyard_list
