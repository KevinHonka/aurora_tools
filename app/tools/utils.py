import time
from datetime import datetime

from app.database import get_session
from app.database.models import FCTPopulation, FCTGroundUnitFormation, FCTRaceSysSurvey
from app.tools.config import get_config


def correct_time(oldtime: float):
    config = get_config()

    starttime_stamp = time.mktime(datetime.strptime(config.START_DATE, "%Y").timetuple())

    return datetime.fromtimestamp(starttime_stamp + oldtime).strftime("%Y-%m-%d")


def get_times(timeframe: str, gamestart):
    times = timeframe.split(" ")

    startpoint = datetime.timestamp(datetime.strptime(gamestart, "%d-%m-%Y"))

    starttime = datetime.timestamp(datetime.strptime(times[0], "%d-%m-%Y")) - startpoint
    stoptime = datetime.timestamp(datetime.strptime(times[1], "%d-%m-%Y")) - startpoint

    return starttime, stoptime


def get_location_from_popid(pop_id):
    session = get_session()
    config = get_config()

    population = session.query(FCTPopulation).filter(FCTPopulation.GameID == config.game.GameID,
                                                     FCTPopulation.RaceID == config.race.RaceID,
                                                     FCTPopulation.PopulationID == pop_id).one_or_none()

    location = f"{get_system_name(population.SystemID)} - {population.PopName}"

    return location


def get_groundunit_from_unitid(unit_id):
    session = get_session()
    config = get_config()

    groundunit = session.query(FCTGroundUnitFormation).filter(FCTGroundUnitFormation.GameID == config.game.GameID,
                                                              FCTGroundUnitFormation.RaceID == config.race.RaceID,
                                                              FCTGroundUnitFormation.FormationID == unit_id).one()

    if groundunit.PopulationID:
        status = get_location_from_popid(groundunit.PopulationID)
    else:
        status = "in transi"

    unit = f"{groundunit.Name}: {status}"

    return unit


def get_ressources(colony):
    ressources = {'Duranium': {'available': colony.Duranium, 'reserve': colony.ReserveDuranium},
                  'Neutronium': {'available': colony.Neutronium, 'reserve': colony.ReserveNeutronium},
                  'Corbomite': {'available': colony.Corbomite, 'reserve': colony.ReserveCorbomite},
                  'Tritanium': {'available': colony.Tritanium, 'reserve': colony.ReserveTritanium},
                  'Boronide': {'available': colony.Boronide, 'reserve': colony.ReserveBoronide},
                  'Mercassium': {'available': colony.Mercassium, 'reserve': colony.ReserveMercassium},
                  'Vendarite': {'available': colony.Vendarite, 'reserve': colony.ReserveVendarite},
                  'Sorium': {'available': colony.Sorium, 'reserve': colony.ReserveSorium},
                  'Uridium': {'available': colony.Uridium, 'reserve': colony.ReserveUridium},
                  'Corundium': {'available': colony.Corundium, 'reserve': colony.ReserveCorundium},
                  'Gallicite': {'available': colony.Gallicite, 'reserve': colony.ReserveGallicite}}

    return ressources


def get_system_name(system_id):
    session = get_session()
    config = get_config()

    system_name = session.query(FCTRaceSysSurvey).filter(FCTRaceSysSurvey.GameID == config.game.GameID,
                                                         FCTRaceSysSurvey.RaceID == config.race.RaceID,
                                                         FCTRaceSysSurvey.SystemID == system_id).one().Name

    return system_name
