from app.database import get_session
from app.database.models import FCTRank
from app.tools.config import get_config
from app.tools.models import Rank


def get_rank(rank_id):
    session = get_session()
    config = get_config()

    rank = session.query(FCTRank).filter(FCTRank.GameID == config.game.GameID,
                                         FCTRank.RaceID == config.race.RaceID,
                                         FCTRank.RankID == rank_id).one()

    return Rank(rank.RankName, rank.RankAbbrev, rank.RankType)


def get_ranks():
    session = get_session()
    config = get_config()

    rank = session.query(FCTRank).filter(FCTRank.GameID == config.game.GameID,
                                         FCTRank.RaceID == config.race.RaceID).all()

    ranks = {'Naval': [], 'Groundcombat': []}

    for r in rank:
        if r.RankType == 0:
            ranks['Naval'].append(r)
        elif r.RankType == 1:
            ranks['Groundcombat'].append(r)

    return ranks
