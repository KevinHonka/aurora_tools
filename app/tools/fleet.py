from app.database import get_session
from app.database.models import FCTFleet, FCTShip
from app.tools.config import get_config
from app.tools.filters import fleet_filter
from app.tools.models import Ship
from app.tools.ship import get_shipclass


def get_ships_of_fleet(fleet_id):
    session = get_session()
    config = get_config()

    ship_names = []

    ship_list = session.query(FCTShip).filter(FCTShip.GameID == config.game.GameID,
                                              FCTShip.FleetID == fleet_id).all()

    return ship_list


def get_fleets(filters=None):
    session = get_session()
    config = get_config()

    fleets = {}

    query = session.query(FCTFleet).filter(FCTFleet.GameID == config.game.GameID,
                                           FCTFleet.RaceID == config.race.RaceID)

    if filters:
        query = fleet_filter(query, filters)

    fleet_list = query.all()

    for fleet in fleet_list:

        if fleet.FleetName not in fleets.keys():
            fleets[fleet.FleetName] = []

        ships = get_ships_of_fleet(fleet.FleetID)

        for ship in ships:
            shipclass = get_shipclass(f"id={ship.ShipClassID}")[0]

            fleets[fleet.FleetName].append(Ship(name=ship.ShipName, shipclass=shipclass.name))

    return fleets
