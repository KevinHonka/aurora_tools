# coding: utf-8
from sqlalchemy import Boolean, Column, Float, Integer, Numeric, SmallInteger, String, Table, Text, text
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class DIMAssignmentTypeFilter(Base):
    __tablename__ = 'DIM_AssignmentTypeFilter'

    AssignmentTypeID = Column(Integer, primary_key=True, server_default=text("NULL"))
    LeaderType = Column(Integer, server_default=text("NULL"))
    Description = Column(Text, server_default=text("NULL"))
    DisplayOrder = Column(Integer, server_default=text("NULL"))
    Ship = Column(Numeric, server_default=text("NULL"))
    Team = Column(Numeric, server_default=text("NULL"))
    Population = Column(Numeric, server_default=text("NULL"))
    Sector = Column(Numeric, server_default=text("NULL"))
    Ground = Column(Numeric, server_default=text("NULL"))
    Fighter = Column(Numeric, server_default=text("NULL"))
    Armed = Column(Numeric, server_default=text("NULL"))
    Freighters = Column(Numeric, server_default=text("NULL"))
    NavalAdmin = Column(Numeric, server_default=text("NULL"))
    Survey = Column(Integer)
    RankPriorityModifier = Column(Integer)
    ShipCommandType = Column(Integer)
    Academy = Column(Integer, server_default=text("0"))


class DIMAutomatedClassDesign(Base):
    __tablename__ = 'DIM_AutomatedClassDesign'

    AutomatedClassID = Column(Integer, primary_key=True, server_default=text("NULL"))
    DesignID = Column(Text, server_default=text("NULL"))
    DefaultClassName = Column(Text, server_default=text("NULL"))
    ShippingLineName = Column(Text, server_default=text("NULL"))
    KeyTechA = Column(Integer, server_default=text("0"))
    KeyTechB = Column(Integer, server_default=text("0"))
    ECM = Column(Integer, server_default=text("NULL"))
    HullID = Column(Integer, server_default=text("0"))
    HullSize = Column(Float, server_default=text("0"))
    WeaponType = Column(Integer, server_default=text("0"))
    MissileType = Column(Integer, server_default=text("0"))
    EngineType = Column(Integer, server_default=text("0"))
    EngineNumberType = Column(Integer, server_default=text("0"))
    EngineNumber = Column(Integer, server_default=text("0"))
    RandomEngineElement = Column(Integer, server_default=text("0"))
    EngineeringType = Column(Integer, server_default=text("0"))
    Engineering = Column(Integer, server_default=text("0"))
    DeploymentDuration = Column(Float, server_default=text("0"))
    FuelDuration = Column(Float, server_default=text("0"))
    AdditionalArmour = Column(Numeric, server_default=text("NULL"))
    ArmourAdjustment = Column(Integer, server_default=text("0"))
    Hangar = Column(Integer, server_default=text("0"))
    Shields = Column(Integer, server_default=text("0"))
    SecondaryActive = Column(Integer, server_default=text("0"))
    NumFireControls = Column(Integer, server_default=text("0"))
    MissileFireControl = Column(Integer, server_default=text("0"))
    ThermalFixed = Column(Integer, server_default=text("0"))
    ThermalRandom = Column(Integer, server_default=text("0"))
    EMFixed = Column(Integer, server_default=text("0"))
    EMRandom = Column(Integer, server_default=text("0"))
    CargoHolds = Column(Integer, server_default=text("0"))
    CargoHandling = Column(Integer, server_default=text("0"))
    CryogenicModules = Column(Integer, server_default=text("0"))
    LuxuryAccomodation = Column(Integer, server_default=text("0"))
    SalvageModules = Column(Integer, server_default=text("0"))
    JGCS = Column(Numeric, server_default=text("0"))
    CIWSFixed = Column(Integer, server_default=text("0"))
    CIWSRandom = Column(Integer, server_default=text("0"))
    JumpDriveType = Column(Integer, server_default=text("0"))
    ArmamentType = Column(Integer, server_default=text("0"))
    HullClass = Column(Text)
    BeamFireControl = Column(Integer, server_default=text("0"))
    AuxiliaryControl = Column(Integer, server_default=text("0"))
    ScienceDepartment = Column(Integer, server_default=text("0"))
    MainEngineering = Column(Integer, server_default=text("0"))
    CIC = Column(Integer, server_default=text("0"))
    PrimaryFlightControl = Column(Integer, server_default=text("0"))
    SpinalLaser = Column(Integer, server_default=text("0"))
    ECCM = Column(Integer, server_default=text("0"))
    FuelTransferSystem = Column(Integer, server_default=text("0"))
    AssignAsTanker = Column(Integer, server_default=text("0"))
    AssignAsCollier = Column(Integer, server_default=text("0"))
    GeoSurvey = Column(Integer, server_default=text("0"))
    GravSurvey = Column(Integer, server_default=text("0"))
    TroopTransportBase = Column(Integer, server_default=text("0"))
    TroopTransportRandom = Column(Integer, server_default=text("0"))
    Notes = Column(Text)
    SecondaryWeapon = Column(Boolean, server_default=text("FALSE"))
    PrimaryActive = Column(Integer, server_default=text("0"))
    FillSpace = Column(Boolean, server_default=text("TRUE"))
    BioEnergyStorage = Column(Integer, server_default=text("0"))
    HullSizeType = Column(Integer, server_default=text("0"))
    SwarmDesign = Column(Boolean, server_default=text("0"))
    HiveShipSizeClass = Column(Integer, server_default=text("FALSE"))
    EnergyEscort = Column(Integer, server_default=text("0"))
    SurrenderStatus = Column(Integer, server_default=text("0"))
    MaxFuelHullPercentage = Column(Float, server_default=text("0"))


t_DIM_AutomatedGroundTemplateDesign = Table(
    'DIM_AutomatedGroundTemplateDesign', metadata,
    Column('AutomatedTemplateID', Integer, server_default=text("0")),
    Column('Name', Text),
    Column('Abbreviation', Text),
    Column('StandardProportion', Float, server_default=text("0")),
    Column('Priority', Integer, server_default=text("0")),
    Column('SpecialNPR', Integer, server_default=text("0")),
    Column('FieldPosition', Integer, server_default=text("0")),
    Column('PrimaryFunction', Integer, server_default=text("0")),
    Column('RequiredTechID', Integer, server_default=text("0"))
)

t_DIM_AutomatedGroundTemplateElements = Table(
    'DIM_AutomatedGroundTemplateElements', metadata,
    Column('AutomatedTemplateID', Integer),
    Column('BaseAmount', Integer),
    Column('DiceAmount', Integer),
    Column('Multiple', Integer),
    Column('DiceSize', Integer),
    Column('GUClassType', Integer),
    Column('PrimaryClass', Boolean),
    Column('ClassName', Text),
    Column('HQ', Boolean, server_default=text("FALSE")),
    Column('Logistics', Boolean, server_default=text("FALSE")),
    Column('Construction', Boolean, server_default=text("0"))
)

t_DIM_CommanderBonusType = Table(
    'DIM_CommanderBonusType', metadata,
    Column('BonusID', Integer, server_default=text("0")),
    Column('Description', Text, server_default=text("NULL")),
    Column('Percentage', Numeric, server_default=text("NULL")),
    Column('Naval', Numeric, server_default=text("NULL")),
    Column('Ground', Numeric, server_default=text("NULL")),
    Column('Civilian', Numeric, server_default=text("NULL")),
    Column('Scientist', Numeric, server_default=text("NULL")),
    Column('DisplayOrder', Integer),
    Column('BonusAbbrev', Text),
    Column('MaximumBonus', Float, server_default=text("0"))
)


class DIMCommanderNameTheme(Base):
    __tablename__ = 'DIM_CommanderNameTheme'

    NameThemeID = Column(Integer, primary_key=True)
    Description = Column(Text, server_default=text("NULL"))
    NameOne = Column(Integer, server_default=text("0"))
    NameTwo = Column(Integer, server_default=text("0"))
    NameThree = Column(Integer, server_default=text("0"))
    SpecialRule = Column(Integer, server_default=text("0"))
    NameOneAddition = Column(Text)
    NameOneAdditionChance = Column(Integer, server_default=text("0"))


t_DIM_CommanderNames = Table(
    'DIM_CommanderNames', metadata,
    Column('NameThemeID', Integer, server_default=text("NULL")),
    Column('FirstName', Boolean, server_default=text("NULL")),
    Column('Name', Text, server_default=text("NULL")),
    Column('Female', Boolean, server_default=text("0")),
    Column('ThirdName', Boolean),
    Column('FamilyName', Boolean)
)


class DIMCompanyName(Base):
    __tablename__ = 'DIM_CompanyNames'

    ID = Column(Integer, primary_key=True)
    CompanySuffix = Column(String(255), server_default=text("NULL"))
    CompanyType = Column(Integer, server_default=text("1"))


class DIMComponentType(Base):
    __tablename__ = 'DIM_ComponentType'

    ComponentTypeID = Column(Integer, primary_key=True)
    TypeDescription = Column(Text, server_default=text("NULL"))
    EmptySpaceModifier = Column(Float, server_default=text("0"))
    RatingDescription = Column(Text, server_default=text("NULL"))
    ClassDisplayOrder = Column(Integer, server_default=text("NULL"))
    RepairPriority = Column(Integer, server_default=text("0"))
    ShowInClassDisplay = Column(Numeric, server_default=text("0"))
    SingleSystem = Column(Numeric, server_default=text("0"))


class DIMCondition(Base):
    __tablename__ = 'DIM_Condition'

    ConditionID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(String(50), server_default=text("NULL"))
    DisplayOrder = Column(Integer, server_default=text("NULL"))


t_DIM_DesignTheme = Table(
    'DIM_DesignTheme', metadata,
    Column('DesignThemeID', Integer, server_default=text("0")),
    Column('Description', Text),
    Column('Name', Text),
    Column('EngineProportionBase', Integer),
    Column('EngineProportionRange', Integer),
    Column('WarshipHullBase', Integer, server_default=text("0")),
    Column('WarshipHullRange', Integer, server_default=text("0")),
    Column('NumEnginesSmall', Text),
    Column('MissileSizeBase', Integer),
    Column('MissileSizeRange', Integer),
    Column('ArmourMultiplier', Float),
    Column('ShieldProportionBase', Integer),
    Column('ShieldProportionRange', Integer),
    Column('RandomWeight', Integer),
    Column('PrimaryBeamWeapon', Integer),
    Column('SecondaryBeamWeapon', Integer),
    Column('PointDefenceWeapon', Integer),
    Column('StartingTechPointModifier', Float, server_default=text("1")),
    Column('FighterFactories', Boolean, server_default=text("1")),
    Column('OrdnanceFactories', Boolean, server_default=text("1")),
    Column('MaxGeo', Integer, server_default=text("4")),
    Column('MaxGrav', Integer, server_default=text("4")),
    Column('MaxScout', Integer, server_default=text("4")),
    Column('DeploymentStrategy', Integer, server_default=text("1")),
    Column('MissileStandard', Boolean),
    Column('MissileFAC', Boolean),
    Column('MissilePD', Boolean),
    Column('MissileMine', Boolean),
    Column('TechProgressionType', Integer),
    Column('GroundTemplateDistribution', Integer, server_default=text("1")),
    Column('SpecialNPRID', Integer, server_default=text("0")),
    Column('OpGroupProgressionType', Integer, server_default=text("0")),
    Column('ShippingLines', Boolean, server_default=text("FALSE")),
    Column('ColonyDensityBase', Integer, server_default=text("5")),
    Column('ColonyDensityRange', Integer, server_default=text("0")),
    Column('MaxStabilisation', Integer),
    Column('MaxSalvage', Integer, server_default=text("0")),
    Column('MaxTanker', Integer),
    Column('SpecialModifications', Integer, server_default=text("0")),
    Column('GroundForceDeploymentThemeID', Integer, server_default=text("1")),
    Column('MaxDiplomatic', Integer, server_default=text("0")),
    Column('PlayerEligible', Boolean, server_default=text("0"))
)

t_DIM_DesignThemeGroundForceDeployments = Table(
    'DIM_DesignThemeGroundForceDeployments', metadata,
    Column('GroundForceDeploymentThemeID', Integer, server_default=text("0")),
    Column('AutomatedTemplateID', Integer, server_default=text("0")),
    Column('NumberRequired', Integer, server_default=text("0")),
    Column('PopulationValue', Integer, server_default=text("0")),
    Column('Priority', Integer, server_default=text("0"))
)

t_DIM_DesignThemeTechProgression = Table(
    'DIM_DesignThemeTechProgression', metadata,
    Column('TechTypeID', Integer),
    Column('TechGroupID', Integer),
    Column('ProgressionOrder', Float),
    Column('Description', Text),
    Column('Notes', Text),
    Column('ResearchField', Integer, server_default=text("0")),
    Column('EstimatedRP', Integer, server_default=text("0")),
    Column('TechProgressionCategoryID', Integer, server_default=text("0")),
    Column('Mandatory', Boolean, server_default=text("FALSE"))
)


class DIMEmpireTitle(Base):
    __tablename__ = 'DIM_EmpireTitles'

    EmpireTitleID = Column(Integer, primary_key=True)
    Title = Column(Text, server_default=text("NULL"))
    Prefix = Column(Numeric, server_default=text("NULL"))
    Suffix = Column(Numeric, server_default=text("NULL"))
    MaxChance = Column(Integer, server_default=text("NULL"))


class DIMEventType(Base):
    __tablename__ = 'DIM_EventType'

    EventTypeID = Column(Integer, primary_key=True)
    Description = Column(Text, server_default=text("NULL"))
    CombatDisplay = Column(Integer, server_default=text("0"))
    DamageDisplay = Column(Integer, server_default=text("0"))
    AttackEvent = Column(Numeric, server_default=text("NULL"))
    PlayerInterrupt = Column(Integer, server_default=text("0"))
    AIInterrupt = Column(Integer, server_default=text("0"))


class DIMGase(Base):
    __tablename__ = 'DIM_Gases'

    GasID = Column(Integer, primary_key=True)
    Name = Column(Text, server_default=text("NULL"))
    Symbol = Column(Text, server_default=text("NULL"))
    Weight = Column(Integer, server_default=text("NULL"))
    BoilingPoint = Column(Integer, server_default=text("NULL"))
    GHGas = Column(Numeric, server_default=text("NULL"))
    AntiGHGas = Column(Numeric, server_default=text("NULL"))
    Dangerous = Column(Integer, server_default=text("NULL"))
    DangerousLevel = Column(Float)


class DIMGroundComponentType(Base):
    __tablename__ = 'DIM_GroundComponentType'

    Size = Column(Float, server_default=text("0"))
    Static = Column(Boolean, server_default=text("False"))
    Infantry = Column(Boolean, server_default=text("False"))
    Vehicle = Column(Boolean, server_default=text("False"))
    SuperHeavyVehicle = Column(Boolean, server_default=text("False"))
    Penetration = Column(Float, server_default=text("0"))
    Damage = Column(Float, server_default=text("0"))
    Shots = Column(Integer, server_default=text("0"))
    TechSystemID = Column(Integer, server_default=text("0"))
    ComponentName = Column(Text)
    ComponentTypeID = Column(Integer, primary_key=True, server_default=text("0"))
    Abbreviation = Column(Text)
    HeavyVehicle = Column(Boolean)
    UltraHeavyVehicle = Column(Boolean)
    STO = Column(Integer)
    Construction = Column(Float)
    FireDirection = Column(Integer)
    HQMaxSize = Column(Integer)
    LightVehicle = Column(Boolean, server_default=text("False"))
    BombardmentWeapon = Column(Integer, server_default=text("False"))
    Geosurvey = Column(Float, server_default=text("0"))
    LogisticsPoints = Column(Integer, server_default=text("0"))
    SupplyUse = Column(Float, server_default=text("0"))
    AAWeapon = Column(Integer, server_default=text("0"))
    DisplayOrder = Column(Integer, server_default=text("0"))
    Xenoarchaeology = Column(Float, server_default=text("0"))


t_DIM_GroundUnitArmour = Table(
    'DIM_GroundUnitArmour', metadata,
    Column('TechSystemID', Integer, server_default=text("0")),
    Column('Name', Text),
    Column('ArmourStrength', Float, server_default=text("0")),
    Column('BaseUnitType', Integer, server_default=text("0")),
    Column('ArmourTypeID', Integer, server_default=text("0"))
)

t_DIM_GroundUnitBaseType = Table(
    'DIM_GroundUnitBaseType', metadata,
    Column('Name', Text),
    Column('HitPoints', Integer, server_default=text("1")),
    Column('MaxSelfFortification', Float, server_default=text("1")),
    Column('MaxFortification', Float, server_default=text("1")),
    Column('UnitBaseTypeID', Integer, server_default=text("0")),
    Column('Size', Integer, server_default=text("0")),
    Column('TechSystemID', Integer),
    Column('Abbreviation', Text),
    Column('ToHitModifier', Float, server_default=text("1")),
    Column('ComponentSlots', Integer, server_default=text("1")),
    Column('DisplayOrder', Integer, server_default=text("0"))
)

t_DIM_GroundUnitCapability = Table(
    'DIM_GroundUnitCapability', metadata,
    Column('CapabilityID', Integer),
    Column('CapabilityName', Text),
    Column('TechSystemID', Integer),
    Column('InfantryOnly', Boolean),
    Column('CostMultiplier', Float, server_default=text("1")),
    Column('HPEnhancement', Float, server_default=text("0"))
)


class DIMKnownSystem(Base):
    __tablename__ = 'DIM_KnownSystems'

    KnownSystemID = Column(Integer, primary_key=True)
    Name = Column(Text, server_default=text("NULL"))
    Primary = Column(Text, server_default=text("NULL"))
    Component1ID = Column(Integer, server_default=text("NULL"))
    Component2ID = Column(Integer, server_default=text("0"))
    C2Orbit = Column(Float, server_default=text("0"))
    Component3ID = Column(Integer, server_default=text("0"))
    C3Orbit = Column(Float, server_default=text("0"))
    C3OrbitType = Column(Integer, server_default=text("0"))
    Component4ID = Column(Integer, server_default=text("0"))
    C4Orbit = Column(Float, server_default=text("0"))
    C4OrbitType = Column(Integer, server_default=text("0"))
    X = Column(Float, server_default=text("0"))
    Y = Column(Float, server_default=text("0"))
    Z = Column(Float, server_default=text("0"))
    Distance = Column(Float, server_default=text("NULL"))
    Checked = Column(Text)


t_DIM_LineNames = Table(
    'DIM_LineNames', metadata,
    Column('LineNameID', Integer),
    Column('LineNames', String(50), server_default=text("NULL"))
)

t_DIM_MedalCondition = Table(
    'DIM_MedalCondition', metadata,
    Column('MedalConditionID', Integer, server_default=text("0")),
    Column('Description', Text),
    Column('MeasurementType', Integer, server_default=text("0")),
    Column('AmountRequired', Integer, server_default=text("1")),
    Column('DisplayOrder', Integer, server_default=text("0"))
)


class DIMMiningName(Base):
    __tablename__ = 'DIM_MiningNames'

    MNID = Column(Integer, primary_key=True)
    MiningName = Column(String(50), server_default=text("NULL"))


class DIMMoveAction(Base):
    __tablename__ = 'DIM_MoveAction'

    MoveActionID = Column(Integer, primary_key=True, server_default=text("0"))
    Description = Column(Text, server_default=text("NULL"))
    Completed = Column(Numeric, server_default=text("0"))
    MoveRequirementID = Column(Integer, server_default=text("0"))
    DestinationItemType = Column(Integer, server_default=text("0"))
    DisplayOrder = Column(Integer, server_default=text("NULL"))
    TransitOrder = Column(Numeric, server_default=text("NULL"))
    MinDistanceOption = Column(Numeric, server_default=text("0"))
    SpecifyQuanitity = Column(Numeric, server_default=text("0"))
    NoOrderChange = Column(Numeric, server_default=text("NULL"))
    MinQuantity = Column(Numeric, server_default=text("NULL"))
    LoadGroundUnit = Column(Numeric, server_default=text("0"))
    NoGasGiants = Column(Numeric, server_default=text("NULL"))
    ReserveLevel = Column(Numeric, server_default=text("NULL"))
    WarpPoint = Column(Numeric, server_default=text("NULL"))
    SystemBody = Column(Numeric, server_default=text("NULL"))
    SurveyLocation = Column(Numeric, server_default=text("NULL"))
    Fleet = Column(Numeric, server_default=text("NULL"))
    Waypoint = Column(Numeric, server_default=text("NULL"))
    Contact = Column(Numeric, server_default=text("NULL"))
    Lifepod = Column(Numeric, server_default=text("NULL"))
    Wreck = Column(Numeric, server_default=text("NULL"))
    LagrangePoint = Column(Numeric, server_default=text("NULL"))
    Wormhole = Column(Numeric, server_default=text("NULL"))
    DesignModeOnly = Column(Numeric, server_default=text("NULL"))


t_DIM_NamingTheme = Table(
    'DIM_NamingTheme', metadata,
    Column('NameThemeID', Integer, server_default=text("NULL")),
    Column('Name', String(255), server_default=text("NULL"))
)


class DIMNamingThemeType(Base):
    __tablename__ = 'DIM_NamingThemeTypes'

    ThemeID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(Text, server_default=text("NULL"))
    RaceNameEligible = Column(Boolean, server_default=text("False"))


t_DIM_NavalAdminCommandType = Table(
    'DIM_NavalAdminCommandType', metadata,
    Column('CommandTypeID', Integer, server_default=text("0")),
    Column('Description', Text),
    Column('Abbrev', Text),
    Column('Radius', Float),
    Column('Survey', Float),
    Column('Industrial', Float),
    Column('FleetTraining', Float),
    Column('Engineering', Float, server_default=text("0")),
    Column('CrewTraining', Float, server_default=text("0")),
    Column('Tactical', Float, server_default=text("0")),
    Column('Reaction', Float, server_default=text("0")),
    Column('Logistics', Float),
    Column('DisplayOrder', Integer, server_default=text("0"))
)

t_DIM_OperationalGroup = Table(
    'DIM_OperationalGroup', metadata,
    Column('Description', Text),
    Column('TechRequirementA', Integer, server_default=text("0")),
    Column('PrimaryStandingOrder', Integer),
    Column('SecondaryStandingOrder', Integer),
    Column('MainFunction', Integer, server_default=text("0")),
    Column('AvoidDanger', Boolean),
    Column('OperationalGroupID', Integer),
    Column('ChangeStandingToFuel', Boolean, server_default=text("FALSE")),
    Column('MainFunctionPriority', Integer, server_default=text("0")),
    Column('MobileMilitary', Integer, server_default=text("0")),
    Column('OffensiveForce', Boolean, server_default=text("FALSE")),
    Column('StaticForce', Boolean, server_default=text("FALSE"))
)

t_DIM_OperationalGroupElement = Table(
    'DIM_OperationalGroupElement', metadata,
    Column('AutomatedDesignID', Integer),
    Column('NumShips', Integer),
    Column('KeyElement', Boolean, server_default=text("false")),
    Column('OperationalGroupID', Integer),
    Column('Description', Text),
    Column('RandomNumShips', Integer, server_default=text("0"))
)

t_DIM_OperationalGroupProgression = Table(
    'DIM_OperationalGroupProgression', metadata,
    Column('OpGroupProgressionType', Integer),
    Column('ProgressionOrder', Integer),
    Column('NumGroups', Integer),
    Column('CountGroupType', Integer, server_default=text("0")),
    Column('OperationalGroupID', Integer, server_default=text("1")),
    Column('Description', Text)
)


class DIMPlanetaryInstallation(Base):
    __tablename__ = 'DIM_PlanetaryInstallation'

    PlanetaryInstallationID = Column(Integer, primary_key=True)
    Name = Column(Text, server_default=text("NULL"))
    Cost = Column(Float, server_default=text("NULL"))
    ConversionFrom = Column(Integer, server_default=text("0"))
    ConversionTo = Column(Integer, server_default=text("0"))
    CargoPoints = Column(Integer, server_default=text("0"))
    ConstructionValue = Column(Float, server_default=text("0"))
    OrdnanceProductionValue = Column(Float, server_default=text("0"))
    FighterProductionValue = Column(Float, server_default=text("0"))
    RefineryProductionValue = Column(Float, server_default=text("0"))
    ResearchValue = Column(Float, server_default=text("0"))
    TerraformValue = Column(Float, server_default=text("0"))
    MaintenanceValue = Column(Float, server_default=text("0"))
    MiningProductionValue = Column(Float, server_default=text("0"))
    InfrastructureValue = Column(Float, server_default=text("0"))
    LGInfrastructureValue = Column(Float, server_default=text("0"))
    SensorValue = Column(Float, server_default=text("0"))
    GroundTrainingValue = Column(Float, server_default=text("0"))
    AcademyValue = Column(Float, server_default=text("0"))
    SectorCommandValue = Column(Float, server_default=text("0"))
    MassDriverValue = Column(Float, server_default=text("0"))
    FinancialProductionValue = Column(Float, server_default=text("0"))
    CargoShuttleValue = Column(Float, server_default=text("0"))
    GeneticModificationValue = Column(Float, server_default=text("0"))
    MassRefuelling = Column(Integer, server_default=text("0"))
    Workers = Column(Float, server_default=text("0"))
    WorkerDesciption = Column(Text, server_default=text("NULL"))
    ThermalSignature = Column(Float, server_default=text("0"))
    EMsignature = Column(Float, server_default=text("0"))
    TargetSize = Column(Integer, server_default=text("NULL"))
    CivilianInstallation = Column(Numeric, server_default=text("0"))
    SwarmUse = Column(Numeric, server_default=text("NULL"))
    NoBuild = Column(Numeric, server_default=text("NULL"))
    CivMove = Column(Numeric, server_default=text("NULL"))
    DisplayOrder = Column(Float, server_default=text("NULL"))
    RequiredTechID = Column(Integer, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))
    NavalHeadquartersValue = Column(Integer, server_default=text("0"))
    MassOrdnanceTransfer = Column(Integer, server_default=text("0"))
    Abbreviation = Column(Text)
    TaxableWorkers = Column(Boolean, server_default=text("0"))


t_DIM_PlanetaryTerrain = Table(
    'DIM_PlanetaryTerrain', metadata,
    Column('TerrainID', Integer),
    Column('Name', Text),
    Column('Abbreviation', Text),
    Column('FortificationModifier', Float),
    Column('ToHitModifier', Float),
    Column('MinimumHydro', Float),
    Column('MinimumOxygen', Float),
    Column('MaximumHydro', Float),
    Column('MinimumTemperature', Float),
    Column('MaximumTemperature', Float),
    Column('MaximumTectonics', Integer),
    Column('MinimumTectonics', Integer),
    Column('BaseTerrainType', Integer, server_default=text("0"))
)


class DIMPopPoliticalStatu(Base):
    __tablename__ = 'DIM_PopPoliticalStatus'

    StatusID = Column(Integer, primary_key=True, server_default=text("NULL"))
    StatusName = Column(String(50), server_default=text("NULL"))
    ProductionMod = Column(Float, server_default=text("NULL"))
    WealthMod = Column(Float, server_default=text("NULL"))
    SPRequired = Column(Integer, server_default=text("NULL"))
    NextStatusID = Column(Integer, server_default=text("NULL"))
    OccupationForceMod = Column(Float, server_default=text("NULL"))
    ProtectionRequired = Column(Float, server_default=text("NULL"))
    ServiceSector = Column(Float, server_default=text("NULL"))


class DIMRankThemeType(Base):
    __tablename__ = 'DIM_RankThemeTypes'

    ThemeID = Column(Integer, primary_key=True)
    ThemeName = Column(Text, server_default=text("NULL"))
    RankDone = Column(Numeric, server_default=text("NULL"))


class DIMRankTheme(Base):
    __tablename__ = 'DIM_RankThemes'

    ThemeID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    ThemeRankID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    RankName = Column(Text, nullable=False)
    GFRankName = Column(Text, server_default=text("NULL"))
    CivilianRank = Column(Boolean, server_default=text("NULL"))
    NavalRankAbbrev = Column(Text, server_default=text("NULL"))
    GroundRankAbbrev = Column(Text, server_default=text("NULL"))


class DIMResearchCategory(Base):
    __tablename__ = 'DIM_ResearchCategories'

    CategoryID = Column(Integer, primary_key=True, server_default=text("0"))
    CheckTech = Column(Boolean, server_default=text("NULL"))
    Name = Column(String(50), server_default=text("NULL"))
    CompanyNameType = Column(Integer, server_default=text("1"))
    PlayerDefined = Column(Boolean, server_default=text("NULL"))
    SourceTechType0 = Column(Integer, server_default=text("0"))
    SecondPrimaryTech = Column(Integer, server_default=text("0"))
    SourceTechType1 = Column(Integer, server_default=text("0"))
    SourceTechType2 = Column(Integer, server_default=text("0"))
    SourceTechType3 = Column(Integer, server_default=text("0"))
    SourceTechType4 = Column(Integer, server_default=text("0"))
    SourceTechType5 = Column(Integer, server_default=text("0"))
    SourceTechType6 = Column(Integer, server_default=text("0"))
    SourceTechType7 = Column(Integer, server_default=text("0"))
    Sort0 = Column(Boolean, server_default=text("NULL"))
    Sort1 = Column(Boolean, server_default=text("NULL"))
    Sort2 = Column(Boolean, server_default=text("NULL"))
    Sort3 = Column(Boolean, server_default=text("NULL"))
    Sort4 = Column(Boolean, server_default=text("NULL"))
    Sort5 = Column(Boolean, server_default=text("NULL"))
    Sort6 = Column(Boolean, server_default=text("NULL"))
    Sort7 = Column(Boolean, server_default=text("NULL"))
    Components = Column(Boolean, server_default=text("NULL"))
    NoteField = Column(String(255), server_default=text("NULL"))


class DIMResearchField(Base):
    __tablename__ = 'DIM_ResearchField'

    ResearchFieldID = Column(Integer, primary_key=True, server_default=text("NULL"))
    FieldName = Column(Text, server_default=text("NULL"))
    ShortName = Column(Text, server_default=text("NULL"))
    Abbreviation = Column(Text, server_default=text("NULL"))
    DoNotDisplay = Column(Boolean, server_default=text("FALSE"))


class DIMRuin(Base):
    __tablename__ = 'DIM_Ruin'

    RuinID = Column(Integer, primary_key=True, server_default=text("NULL"))
    MaxChance = Column(Integer, server_default=text("NULL"))
    Description = Column(Text, server_default=text("NULL"))
    AnnualTechChance = Column(Integer, server_default=text("NULL"))
    ExploitedChance = Column(Integer, server_default=text("NULL"))
    FactoriesBase = Column(Integer, server_default=text("NULL"))
    FactoriesRandom = Column(Integer, server_default=text("NULL"))
    DefenceBases = Column(Integer, server_default=text("0"))
    OffenceBases = Column(Integer, server_default=text("0"))
    Fleet = Column(Integer, server_default=text("0"))
    Squadron = Column(Integer, server_default=text("0"))
    Patrol = Column(Integer, server_default=text("0"))
    STO = Column(Integer)
    Regiment = Column(Integer)
    FixedDSTS = Column(Integer)
    RandomDSTS = Column(Integer)


class DIMSetAtmosphere(Base):
    __tablename__ = 'DIM_SetAtmosphere'

    AtmosphereID = Column(Integer, primary_key=True)
    TempBand = Column(Integer, server_default=text("NULL"))
    MaxChance = Column(Integer, server_default=text("NULL"))
    Gas1 = Column(Integer, server_default=text("0"))
    Gas2 = Column(Integer, server_default=text("0"))
    Gas3 = Column(Integer, server_default=text("0"))
    Description = Column(String(100), server_default=text("NULL"))


class DIMShipyardCompany(Base):
    __tablename__ = 'DIM_ShipyardCompanies'

    NameID = Column(Integer, primary_key=True)
    Suffix = Column(String(150), server_default=text("NULL"))


class DIMSolSystemBody(Base):
    __tablename__ = 'DIM_SolSystemBodies'

    SystemBodyID = Column(Integer, primary_key=True)
    Name = Column(Text, server_default=text("' '"))
    PlanetNumber = Column(Integer, server_default=text("0"))
    OrbitNumber = Column(Integer, server_default=text("0"))
    TrojanPlanet = Column(Integer, server_default=text("0"))
    TrojanLocation = Column(Integer, server_default=text("0"))
    TrojanOffset = Column(Float, server_default=text("0"))
    OrbitalDistance = Column(Float, server_default=text("0"))
    CurrentDistance = Column(Float, server_default=text("0"))
    HeadingInward = Column(Numeric, server_default=text("NULL"))
    Bearing = Column(Float, server_default=text("0"))
    BodyClass = Column(Integer, server_default=text("0"))
    Density = Column(Float, server_default=text("0"))
    Radius = Column(Integer, server_default=text("0"))
    Gravity = Column(Float, server_default=text("0"))
    ParentBodyType = Column(Integer, server_default=text("0"))
    BodyTypeID = Column(Integer, server_default=text("0"))
    Mass = Column(Float, server_default=text("0"))
    Escape = Column(Float, server_default=text("0"))
    Year = Column(Float, server_default=text("0"))
    Day = Column(Float, server_default=text("0"))
    TidalForce = Column(Float, server_default=text("0"))
    TidalLock = Column(Numeric, server_default=text("0"))
    Tilt = Column(Integer, server_default=text("0"))
    Eccentricity = Column(Float, server_default=text("0"))
    Roche = Column(Float, server_default=text("0"))
    TectonicActivity = Column(Integer, server_default=text("1"))
    Ring = Column(Numeric, server_default=text("0"))
    MagneticField = Column(Float, server_default=text("0"))
    BaseTemp = Column(Float, server_default=text("0"))
    SurfaceTemp = Column(Float, server_default=text("0"))
    HydroID = Column(Integer, server_default=text("1"))
    HydroExt = Column(Integer, server_default=text("0"))
    AtmosPress = Column(Float, server_default=text("0"))
    Albedo = Column(Float, server_default=text("1"))
    GHFactor = Column(Float, server_default=text("1"))
    RGE = Column(Numeric, server_default=text("0"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("0"))
    PlanetIcon = Column(Text, server_default=text("NULL"))
    DominantTerrain = Column(Integer, server_default=text("0"))


class DIMStandingOrder(Base):
    __tablename__ = 'DIM_StandingOrders'

    Description = Column(Text, server_default=text("NULL"))
    DisplayOrder = Column(Integer, server_default=text("NULL"))
    NPROnly = Column(Numeric, server_default=text("NULL"))
    SystemCheck = Column(Text, server_default=text("S"))
    Standing = Column(Integer, server_default=text("True"))
    Conditional = Column(Integer, server_default=text("false"))
    OrderID = Column(Integer, primary_key=True, server_default=text("NULL"))


class DIMStellarType(Base):
    __tablename__ = 'DIM_StellarType'

    StellarTypeID = Column(Integer, primary_key=True)
    SpectralClass = Column(Text, server_default=text("NULL"))
    SpectralNumber = Column(Integer, server_default=text("NULL"))
    SpecialSystemType = Column(Integer, server_default=text("0"))
    Checked = Column(Boolean, server_default=text("NULL"))
    NotPS = Column(Boolean, server_default=text("NULL"))
    MaxChance = Column(Integer, server_default=text("NULL"))
    BDMaxChance = Column(Integer, server_default=text("0"))
    SizeText = Column(Text, server_default=text("NULL"))
    SizeID = Column(Integer, server_default=text("NULL"))
    Luminosity = Column(Float, server_default=text("NULL"))
    Mass = Column(Float, server_default=text("NULL"))
    Temperature = Column(Integer, server_default=text("NULL"))
    Radius = Column(Float, server_default=text("NULL"))
    AgeRangeID = Column(Integer, server_default=text("NULL"))
    Red = Column(Integer, server_default=text("NULL"))
    Green = Column(Integer, server_default=text("NULL"))
    Blue = Column(Integer, server_default=text("NULL"))
    MaxChanceOld = Column(Integer, server_default=text("NULL"))


class DIMSystemAbundance(Base):
    __tablename__ = 'DIM_SystemAbundance'

    AbundanceID = Column(Integer, primary_key=True)
    Description = Column(Text, server_default=text("NULL"))
    MaxChance = Column(Integer, server_default=text("NULL"))
    Modifier = Column(Integer, server_default=text("NULL"))


class DIMSystemAge(Base):
    __tablename__ = 'DIM_SystemAge'

    AgeID = Column(Integer, primary_key=True)
    SpectralClass = Column(Text, server_default=text("NULL"))
    TotalLife = Column(Float, server_default=text("NULL"))
    Lum1 = Column(Float, server_default=text("NULL"))
    Lum2 = Column(Float, server_default=text("NULL"))
    Lum3 = Column(Float, server_default=text("NULL"))
    Lum4 = Column(Float, server_default=text("NULL"))
    Lum5 = Column(Float, server_default=text("NULL"))
    Lum6 = Column(Float, server_default=text("NULL"))
    Lum7 = Column(Float, server_default=text("NULL"))
    Lum8 = Column(Float, server_default=text("NULL"))
    Lum9 = Column(Float, server_default=text("NULL"))
    Lum10 = Column(Float, server_default=text("NULL"))
    Age1 = Column(Float, server_default=text("NULL"))
    Age2 = Column(Float, server_default=text("NULL"))
    Age3 = Column(Float, server_default=text("NULL"))
    Age4 = Column(Float, server_default=text("NULL"))
    Age5 = Column(Float, server_default=text("NULL"))
    Age6 = Column(Float, server_default=text("NULL"))
    Age7 = Column(Float, server_default=text("NULL"))
    Age8 = Column(Float, server_default=text("NULL"))
    Age9 = Column(Float, server_default=text("NULL"))
    Age10 = Column(Float, server_default=text("NULL"))


t_DIM_TechProgressionCategory = Table(
    'DIM_TechProgressionCategory', metadata,
    Column('TechProgressionCategoryID', Integer),
    Column('Standard', Boolean),
    Column('StandardGauss', Boolean),
    Column('StandardEnergy', Boolean),
    Column('StandardEnergyGauss', Boolean),
    Column('StandardJump', Boolean),
    Column('StandardJumpGauss', Boolean),
    Column('StandardJumpShields', Boolean),
    Column('StandardJumpGaussShields', Boolean),
    Column('Precursor', Boolean),
    Column('StarSwarm', Boolean),
    Column('Invader', Boolean),
    Column('Ork', Boolean),
    Column('Description', Text),
    Column('StandardEnergyJump', Boolean, server_default=text("0")),
    Column('StandardEnergyJumpGauss', Boolean, server_default=text("0"))
)


class DIMTechType(Base):
    __tablename__ = 'DIM_TechType'

    TechTypeID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(Text, server_default=text("NULL"))
    FieldID = Column(Integer, server_default=text("NULL"))
    DistributeLowerTech = Column(Boolean, server_default=text("TRUE"))


class DIMTradeGood(Base):
    __tablename__ = 'DIM_TradeGoods'

    TradeGoodID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(Text, server_default=text("NULL"))
    PopRequired = Column(Float, server_default=text("0"))
    Category = Column(Integer, server_default=text("NULL"))
    RareGood = Column(Boolean, server_default=text("0"))


class DIMTraitGroup(Base):
    __tablename__ = 'DIM_TraitGroup'

    TraitGroupID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(String(50), server_default=text("NULL"))
    Opposite1 = Column(Integer, server_default=text("NULL"))
    Opposite2 = Column(Integer, server_default=text("NULL"))


class DIMTraitsList(Base):
    __tablename__ = 'DIM_TraitsList'

    TraitID = Column(Integer, primary_key=True)
    GroupID = Column(Integer, server_default=text("2"))
    Name = Column(Text, server_default=text("NULL"))


class DIMWealthUse(Base):
    __tablename__ = 'DIM_WealthUse'

    WealthUseID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Description = Column(String(50), server_default=text("NULL"))
    Income = Column(Boolean, server_default=text("NULL"))
    DisplayOrder = Column(Float, server_default=text("NULL"))


class DefaultEventColour(Base):
    __tablename__ = 'DefaultEventColour'

    EventTypeID = Column(Integer, primary_key=True, nullable=False, index=True, server_default=text("NULL"))
    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    AlertColour = Column(Integer, server_default=text("0"))
    TextColour = Column(Integer, server_default=text("NULL"))
    CreationOrder = Column(Integer, server_default=text("NULL"))
    Test = Column(Text)


t_FCT_AcidAttack = Table(
    'FCT_AcidAttack', metadata,
    Column('TargetShipID', Integer, server_default=text("0")),
    Column('ArmourColumn', Integer, server_default=text("0")),
    Column('PointOfDamageTime', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('RemainingDamage', Integer, server_default=text("0")),
    Column('LastDamageTime', Float, server_default=text("0"))
)

t_FCT_AetherRift = Table(
    'FCT_AetherRift', metadata,
    Column('SystemID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('Xcor', Float, server_default=text("0")),
    Column('Ycor', Float, server_default=text("0")),
    Column('Diameter', Float, server_default=text("0"))
)

t_FCT_AlienClass = Table(
    'FCT_AlienClass', metadata,
    Column('AlienClassID', Integer, server_default=text("0")),
    Column('ActualClassID', Integer),
    Column('AlienRaceID', Integer, server_default=text("0")),
    Column('ArmourStrength', Integer, server_default=text("0")),
    Column('AverageDamage', Integer, server_default=text("0")),
    Column('ClassName', Text),
    Column('ECMStrength', Integer, server_default=text("0")),
    Column('FirstDetected', Float, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('HullID', Integer, server_default=text("0")),
    Column('JumpDistance', Integer, server_default=text("0")),
    Column('MaxSpeed', Integer, server_default=text("0")),
    Column('Notes', Text),
    Column('ShieldRecharge', Float, server_default=text("0")),
    Column('ShieldStrength', Integer, server_default=text("0")),
    Column('ShipCount', Integer, server_default=text("0")),
    Column('Summary', Text),
    Column('TCS', Integer, server_default=text("0")),
    Column('ThermalSignature', Float, server_default=text("0")),
    Column('ViewRaceID', Integer, server_default=text("0")),
    Column('MaxEnergyPDShots', Integer, server_default=text("0")),
    Column('AlienClassRole', Integer, server_default=text("0")),
    Column('ObservedMissileDefence', Boolean, server_default=text("False")),
    Column('TotalEnergyPDHits', Integer, server_default=text("0")),
    Column('TotalEnergyPDShots', Integer, server_default=text("0")),
    Column('DiplomaticShip', Boolean, server_default=text("FALSE")),
    Column('EngineType', Integer, server_default=text("0"))
)

t_FCT_AlienClassSensor = Table(
    'FCT_AlienClassSensor', metadata,
    Column('AlienClassID', Integer, server_default=text("0")),
    Column('AlienSensorID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0"))
)

t_FCT_AlienClassTech = Table(
    'FCT_AlienClassTech', metadata,
    Column('AlienClassID', Integer, server_default=text("NULL")),
    Column('TechID', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("0"))
)

t_FCT_AlienClassWeapon = Table(
    'FCT_AlienClassWeapon', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('AlienClassID', Integer, server_default=text("NULL")),
    Column('WeaponID', Integer, server_default=text("NULL")),
    Column('Amount', Integer, server_default=text("NULL")),
    Column('Range', Float, server_default=text("NULL")),
    Column('ROF', Integer, server_default=text("NULL")),
    Column('LastFired', Float, server_default=text("0"))
)

t_FCT_AlienGroundUnitClass = Table(
    'FCT_AlienGroundUnitClass', metadata,
    Column('AlienRaceID', Integer, server_default=text("0")),
    Column('ActualUnitClassID', Integer, server_default=text("0")),
    Column('ViewRaceID', Integer, server_default=text("0")),
    Column('Name', Text),
    Column('Hits', Integer, server_default=text("0")),
    Column('Penetrated', Integer, server_default=text("0")),
    Column('Destroyed', Integer, server_default=text("0")),
    Column('AlienGroundUnitClassID', Integer),
    Column('GameID', Integer, server_default=text("0")),
    Column('WeaponsKnown', Boolean, server_default=text("FALSE"))
)

t_FCT_AlienPopulation = Table(
    'FCT_AlienPopulation', metadata,
    Column('ViewingRaceID', Integer, server_default=text("0")),
    Column('AlienRaceID', Integer, server_default=text("0")),
    Column('PopulationID', Integer),
    Column('Installations', Integer, server_default=text("0")),
    Column('Mines', Integer, server_default=text("0")),
    Column('Factories', Integer, server_default=text("0")),
    Column('Refineries', Integer, server_default=text("0")),
    Column('ResearchFacilities', Integer, server_default=text("0")),
    Column('MaintenanceFacilities', Integer, server_default=text("0")),
    Column('GFTF', Integer, server_default=text("0")),
    Column('Spaceport', Boolean),
    Column('NavalHeadquarters', Boolean),
    Column('SectorCommand', Boolean),
    Column('RefuellingStation', Boolean),
    Column('OrdnanceTransfer', Boolean),
    Column('CargoStation', Boolean),
    Column('AlienPopulationIntelligencePoints', Float, server_default=text("0")),
    Column('PopulationAmount', Float, server_default=text("0")),
    Column('PopulationName', Text),
    Column('GameID', Integer, server_default=text("0")),
    Column('MaxIntelligence', Float, server_default=text("0")),
    Column('PreviousMaxIntelligence', Float, server_default=text("0")),
    Column('ThermalSignature', Float, server_default=text("0")),
    Column('EMSignature', Float, server_default=text("0"))
)


class FCTAlienRace(Base):
    __tablename__ = 'FCT_AlienRace'

    AlienRaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    ViewRaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    AlienRaceName = Column(Text, server_default=text("NULL"))
    FixedRelationship = Column(Integer, server_default=text("0"))
    ClassThemeID = Column(Integer, server_default=text("0"))
    FirstDetected = Column(Float, server_default=text("NULL"))
    ContactStatus = Column(Integer, server_default=text("0"))
    Abbrev = Column(Text, server_default=text("NULL"))
    CommStatus = Column(Integer, server_default=text("0"))
    CommModifier = Column(Float, server_default=text("0"))
    CommEstablished = Column(Float, server_default=text("0"))
    DiplomaticPoints = Column(Float, server_default=text("0"))
    TradeTreaty = Column(Boolean, server_default=text("NULL"))
    TechTreaty = Column(Boolean, server_default=text("NULL"))
    GeoTreaty = Column(Boolean, server_default=text("NULL"))
    GravTreaty = Column(Boolean, server_default=text("NULL"))
    RealClassNames = Column(Integer, server_default=text("0"))
    AlienRaceIntelligencePoints = Column(Float, server_default=text("0"))


t_FCT_AlienRaceSensor = Table(
    'FCT_AlienRaceSensor', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('Strength', Integer, server_default=text("NULL")),
    Column('Resolution', Integer, server_default=text("NULL")),
    Column('Range', Float, server_default=text("0")),
    Column('IntelligencePoints', Float, server_default=text("0")),
    Column('ActualSensor', Integer, server_default=text("0")),
    Column('ActualMissile', Integer, server_default=text("0")),
    Column('AlienRaceID', Integer, server_default=text("0")),
    Column('ViewingRaceID', Integer, server_default=text("0")),
    Column('AlienSensorID', Integer, server_default=text("0")),
    Column('Name', Text),
    Column('ActualGroundUnitClass', Integer, server_default=text("0"))
)

t_FCT_AlienRaceSpecies = Table(
    'FCT_AlienRaceSpecies', metadata,
    Column('AlienRaceID', Integer, server_default=text("NULL")),
    Column('SpeciesID', Integer, server_default=text("NULL")),
    Column('DetectRaceID', Integer, server_default=text("NULL")),
    Column('GameID', Integer)
)

t_FCT_AlienRaceSystemStatus = Table(
    'FCT_AlienRaceSystemStatus', metadata,
    Column('AlienRaceID', Integer, server_default=text("0")),
    Column('ProtectionStatusID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('SystemID', Integer, server_default=text("0")),
    Column('ViewingRaceID', Integer, server_default=text("0"))
)

t_FCT_AlienShip = Table(
    'FCT_AlienShip', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('AlienRaceID', Integer, server_default=text("NULL")),
    Column('AlienClassID', Integer, server_default=text("NULL")),
    Column('ViewRaceID', Integer, server_default=text("0")),
    Column('ShipID', Integer, server_default=text("0")),
    Column('Name', Text, server_default=text("' '")),
    Column('Speed', Integer, server_default=text("0")),
    Column('LastX', Float, server_default=text("0")),
    Column('LastY', Float, server_default=text("0")),
    Column('LastSysID', Integer, server_default=text("0")),
    Column('LastContactTime', Float, server_default=text("0")),
    Column('FirstDetected', Float, server_default=text("0")),
    Column('Destroyed', Boolean, server_default=text("NULL")),
    Column('DamageTaken', Integer, server_default=text("0")),
    Column('GameTimeDamaged', Float, server_default=text("0")),
    Column('ShieldDamage', Integer, server_default=text("0")),
    Column('ArmourDamage', Integer, server_default=text("0")),
    Column('PenetratingDamage', Integer, server_default=text("0"))
)

t_FCT_AlienSystem = Table(
    'FCT_AlienSystem', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('SystemID', Integer, server_default=text("NULL")),
    Column('AlienRaceID', Integer, server_default=text("NULL")),
    Column('DetectRaceID', Integer, server_default=text("NULL"))
)


class FCTAncientConstruct(Base):
    __tablename__ = 'FCT_AncientConstruct'

    AncientConstructID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    SystemBodyID = Column(Integer, server_default=text("NULL"))
    AncientConstructTypeID = Column(Integer, server_default=text("NULL"))
    ResearchField = Column(Integer, server_default=text("NULL"))
    ResearchBonus = Column(Float, server_default=text("NULL"))
    Active = Column(Boolean, server_default=text("FALSE"))


class FCTArmourDamage(Base):
    __tablename__ = 'FCT_ArmourDamage'

    ShipID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    ArmourColumn = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    Damage = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("16"))


t_FCT_AtmosphericGas = Table(
    'FCT_AtmosphericGas', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('SystemBodyID', Integer, server_default=text("NULL")),
    Column('AtmosGasID', Integer, server_default=text("NULL")),
    Column('AtmosGasAmount', Float, server_default=text("0")),
    Column('GasAtm', Float, server_default=text("0")),
    Column('FrozenOut', Boolean, server_default=text("NULL"))
)

t_FCT_BannedBodies = Table(
    'FCT_BannedBodies', metadata,
    Column('SystemBodyID', Integer, server_default=text("0")),
    Column('RaceID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTClassComponent(Base):
    __tablename__ = 'FCT_ClassComponent'

    GameID = Column(Integer, primary_key=True, server_default=text("0"))
    ClassID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    ComponentID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    NumComponent = Column(Float, server_default=text("NULL"))
    ChanceToHit = Column(Integer, server_default=text("0"))
    ElectronicCTH = Column(Integer, server_default=text("0"))


t_FCT_ClassMaterials = Table(
    'FCT_ClassMaterials', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ClassID', Integer, server_default=text("NULL")),
    Column('MaterialID', Integer, server_default=text("NULL")),
    Column('Amount', Float, server_default=text("NULL"))
)

t_FCT_ClassOrdnanceTemplate = Table(
    'FCT_ClassOrdnanceTemplate', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ShipClassID', Integer, server_default=text("NULL")),
    Column('MissileID', Integer, server_default=text("NULL")),
    Column('Amount', Integer, server_default=text("NULL"))
)

t_FCT_ClassSC = Table(
    'FCT_ClassSC', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ShipClassID', Integer, server_default=text("0")),
    Column('FighterClassID', Integer, server_default=text("0")),
    Column('Number', Integer, server_default=text("0"))
)


class FCTCommander(Base):
    __tablename__ = 'FCT_Commander'

    CommanderID = Column(Integer, primary_key=True)
    GameID = Column(Integer, index=True, server_default=text("NULL"))
    RaceID = Column(Integer, index=True, server_default=text("NULL"))
    SpeciesID = Column(Integer, server_default=text("NULL"))
    Name = Column(Text, server_default=text("NULL"))
    ResSpecID = Column(Integer, server_default=text("0"))
    CommanderType = Column(Integer, server_default=text("0"))
    Title = Column(Text, server_default=text("NULL"))
    RankID = Column(Integer, index=True, server_default=text("0"))
    PromotionScore = Column(Integer, server_default=text("0"))
    PopPromotionScore = Column(Integer, server_default=text("0"))
    GameTimePromoted = Column(Float, server_default=text("0"))
    GameTimeAssigned = Column(Float, server_default=text("0"))
    DoNotRelieve = Column(Integer, server_default=text("0"))
    CommandType = Column(Integer, server_default=text("0"))
    CommandID = Column(Integer, server_default=text("0"))
    PopLocationID = Column(Integer, server_default=text("0"))
    TransportShipID = Column(Integer, server_default=text("0"))
    Seniority = Column(Integer, server_default=text("0"))
    Retired = Column(Boolean, server_default=text("NULL"))
    LifepodID = Column(Integer, server_default=text("0"))
    Orders = Column(Text)
    Notes = Column(Text)
    HomeworldID = Column(Integer, server_default=text("NULL"))
    POWRaceID = Column(Integer, server_default=text("0"))
    CareerStart = Column(Float, server_default=text("1"))
    Deceased = Column(Boolean, server_default=text("0"))
    Loyalty = Column(Integer, server_default=text("100"))
    HealthRisk = Column(Integer, server_default=text("NULL"))
    Female = Column(Boolean, server_default=text("0"))
    KillTonnageCommercial = Column(Integer, server_default=text("0"))
    KillTonnageMilitary = Column(Integer, server_default=text("0"))
    EducationColony = Column(Integer)
    StoryCharacter = Column(Boolean, server_default=text("FALSE"))
    DoNotPromote = Column(Boolean, server_default=text("FALSE"))


t_FCT_CommanderBonuses = Table(
    'FCT_CommanderBonuses', metadata,
    Column('CommanderID', Integer),
    Column('BonusID', Integer),
    Column('BonusValue', Float),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTCommanderHistory(Base):
    __tablename__ = 'FCT_CommanderHistory'

    GameID = Column(Integer, server_default=text("0"), primary_key=True)
    CommanderID = Column(Integer, server_default=text("0"), primary_key=True)
    HistoryText = Column(Text, server_default=text("NULL"), primary_key=True)
    GameTime = Column(Float, primary_key=True)


t_FCT_CommanderMeasurement = Table(
    'FCT_CommanderMeasurement', metadata,
    Column('CommanderID', Integer),
    Column('MeasurementType', Integer, server_default=text("0")),
    Column('Amount', Float, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTCommanderMedal(Base):
    __tablename__ = 'FCT_CommanderMedal'

    NumAwarded = Column(Integer, server_default=text("1"))
    MedalID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    CommanderID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))
    AwardReason = Column(Text)


class FCTCommanderTraits(Base):
    __tablename__ = 'FCT_CommanderTraits'

    CmdrID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    TraitID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("16"))


class FCTContact(Base):
    __tablename__ = 'FCT_Contacts'

    UniqueID = Column(Integer, primary_key=True)
    ContactID = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    DetectRaceID = Column(Integer, server_default=text("NULL"))
    ContactRaceID = Column(Integer, server_default=text("0"))
    ContactName = Column(Text, server_default=text("NULL"))
    CreationTime = Column(Float, server_default=text("NULL"))
    Reestablished = Column(Float, server_default=text("0"))
    LastUpdate = Column(Float, server_default=text("NULL"))
    ContactMethod = Column(Integer, server_default=text("0"))
    ContactType = Column(Integer, server_default=text("NULL"))
    ContactStrength = Column(Float, server_default=text("NULL"))
    ContactNumber = Column(Integer, server_default=text("1"))
    Resolution = Column(Integer, server_default=text("0"))
    ContinualContactTime = Column(Integer, server_default=text("0"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    LastXcor = Column(Float, server_default=text("NULL"))
    LastYcor = Column(Float, server_default=text("NULL"))
    Speed = Column(Integer, server_default=text("0"))
    Msg = Column(Boolean, server_default=text("NULL"))
    IncrementStartX = Column(Float, server_default=text("0"))
    IncrementStartY = Column(Float, server_default=text("0"))


t_FCT_DamageControlQueue = Table(
    'FCT_DamageControlQueue', metadata,
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('RepairOrder', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("16")),
    Column('ComponentID', Integer, server_default=text("NULL"))
)


class FCTDamagedComponent(Base):
    __tablename__ = 'FCT_DamagedComponent'

    ShipID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    ComponentID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    Number = Column(Integer, server_default=text("1"))


class FCTDesignPhilosophy(Base):
    __tablename__ = 'FCT_DesignPhilosophy'

    GameID = Column(Integer, server_default=text("0"))
    ActiveAntiFAC = Column(Integer, server_default=text("0"))
    ActiveAntiFighter = Column(Integer, server_default=text("0"))
    ActiveLarge = Column(Integer, server_default=text("0"))
    ActiveVeryLarge = Column(Integer, server_default=text("0"))
    ActiveResolution = Column(Integer, server_default=text("0"))
    PrimaryBeamPreference = Column(Integer, server_default=text("0"))
    LauncherSize = Column(Integer, server_default=text("0"))
    NumSalvos = Column(Integer, server_default=text("0"))
    WarshipArmour = Column(Integer, server_default=text("0"))
    WarshipEngineering = Column(Integer, server_default=text("0"))
    WarshipHullSize = Column(Integer, server_default=text("0"))
    ActiveAntiMissile = Column(Integer, server_default=text("0"))
    FireControlAntiMissile = Column(Integer, server_default=text("0"))
    FireControlAntiFAC = Column(Integer, server_default=text("0"))
    FireControlAntiFighter = Column(Integer, server_default=text("0"))
    FireControlStandardMissile = Column(Integer, server_default=text("0"))
    JumpDriveBattlecruiser = Column(Integer, server_default=text("0"))
    FireControlBeamRange = Column(Integer, server_default=text("0"))
    FireControlBeamSpeed = Column(Integer, server_default=text("0"))
    JumpDriveCruiser = Column(Integer, server_default=text("0"))
    JumpDriveDestroyer = Column(Integer, server_default=text("0"))
    NumCommercialEngines = Column(Integer, server_default=text("0"))
    JumpDriveSurvey = Column(Integer, server_default=text("0"))
    EMSensorSize1 = Column(Integer, server_default=text("0"))
    EMSensorSize2 = Column(Integer, server_default=text("0"))
    EMSensorSize3 = Column(Integer, server_default=text("0"))
    EMSensorSize6 = Column(Integer, server_default=text("0"))
    EngineCommercial = Column(Integer, server_default=text("0"))
    EngineMilitary = Column(Integer, server_default=text("0"))
    EngineFAC = Column(Integer, server_default=text("0"))
    EngineFighter = Column(Integer, server_default=text("0"))
    MissileCaptorMine = Column(Integer, server_default=text("0"))
    LauncherFAC = Column(Integer, server_default=text("0"))
    FireControlFACMissile = Column(Integer, server_default=text("0"))
    Gauss = Column(Integer, server_default=text("0"))
    LaserLarge = Column(Integer, server_default=text("0"))
    LauncherStandard = Column(Integer, server_default=text("0"))
    Meson = Column(Integer, server_default=text("0"))
    LauncherMine = Column(Integer, server_default=text("0"))
    LauncherMineSize = Column(Integer, server_default=text("0"))
    MissileMineSecondStage = Column(Integer, server_default=text("0"))
    LaserPointDefence = Column(Integer, server_default=text("0"))
    LauncherPointDefence = Column(Integer, server_default=text("0"))
    MesonPointDefence = Column(Integer, server_default=text("0"))
    MissilePointDefence = Column(Integer, server_default=text("0"))
    PointDefencePreference = Column(Integer, server_default=text("0"))
    PointDefenceWeapon = Column(Integer, server_default=text("0"))
    Railgun = Column(Integer, server_default=text("0"))
    MissileStandard = Column(Integer, server_default=text("0"))
    EngineSurvey = Column(Integer, server_default=text("0"))
    ParticleBeam = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, primary_key=True, server_default=text("NULL"))
    Carronade = Column(Integer, server_default=text("0"))
    CIWS = Column(Integer, server_default=text("0"))
    Cloak = Column(Integer, server_default=text("0"))
    ActiveStandard = Column(Integer, server_default=text("0"))
    EngineSizeMilitary = Column(Integer, server_default=text("0"))
    EngineSizeCommercial = Column(Integer, server_default=text("25"))
    MissileFAC = Column(Integer, server_default=text("0"))
    LaserSpinal = Column(Integer, server_default=text("0"))
    ThermalSensorSize1 = Column(Integer, server_default=text("0"))
    ThermalSensorSize2 = Column(Integer, server_default=text("0"))
    ThermalSensorSize3 = Column(Integer, server_default=text("0"))
    ThermalSensorSize6 = Column(Integer, server_default=text("0"))
    ShieldProportion = Column(Float, server_default=text("0"))
    NumWarshipEngines = Column(Integer, server_default=text("2"))
    WarshipEngineProportion = Column(Integer, server_default=text("0"))
    SecondaryBeamPreference = Column(Integer, server_default=text("0"))
    SurveyEngineBoost = Column(Float, server_default=text("0.7"))
    SurveySensors = Column(Integer, server_default=text("2"))
    TerraformModules = Column(Integer, server_default=text("0"))
    HarvesterModules = Column(Integer, server_default=text("0"))
    MiningModules = Column(Integer, server_default=text("0"))
    HighPowerMicrowaveLarge = Column(Integer, server_default=text("0"))
    HighPowerMicrowaveSmall = Column(Integer, server_default=text("0"))
    ActiveSmall = Column(Integer)
    ActiveNavigation = Column(Integer, server_default=text("0"))
    JumpDriveMediumHive = Column(Integer, server_default=text("0"))
    JumpDriveLargeHive = Column(Integer, server_default=text("0"))
    JumpDriveVeryLargeHive = Column(Integer, server_default=text("0"))


t_FCT_ECCMAssignment = Table(
    'FCT_ECCMAssignment', metadata,
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('ECCMID', Integer, server_default=text("NULL")),
    Column('ECCMNum', Integer, server_default=text("NULL")),
    Column('FCTypeID', Integer, server_default=text("0")),
    Column('FCNum', Integer, server_default=text("NULL")),
    Column('GameID', Text, server_default=text("0"))
)

t_FCT_ElementRecharge = Table(
    'FCT_ElementRecharge', metadata,
    Column('GameID', Integer),
    Column('ElementID', Integer, server_default=text("0")),
    Column('RechargeRemaining', Integer, server_default=text("0"))
)


class FCTEventColour(Base):
    __tablename__ = 'FCT_EventColour'

    EventTypeID = Column(Integer, primary_key=True, nullable=False)
    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    AlertColour = Column(Integer, server_default=text("0"))
    TextColour = Column(Integer, server_default=text("NULL"))


t_FCT_FireControlAssignment = Table(
    'FCT_FireControlAssignment', metadata,
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("NULL")),
    Column('FCTypeID', Integer, server_default=text("0")),
    Column('FCNum', Integer, server_default=text("NULL")),
    Column('TargetID', Integer, server_default=text("NULL")),
    Column('TargetType', Integer, server_default=text("NULL")),
    Column('OpenFire', Boolean, server_default=text("0")),
    Column('NodeExpand', Boolean, server_default=text("1")),
    Column('PointDefenceMode', Integer, server_default=text("0")),
    Column('PointDefenceRange', Integer, server_default=text("0")),
    Column('FiredThisPhase', Boolean, server_default=text("0"))
)


class FCTFleet(Base):
    __tablename__ = 'FCT_Fleet'

    FleetID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    FleetName = Column(Text, server_default=text("NULL"))
    AssignedPopulationID = Column(Integer, server_default=text("0"))
    ParentCommandID = Column(Integer, server_default=text("0"))
    OrbitBodyID = Column(Integer, server_default=text("0"))
    OrbitDistance = Column(Integer, server_default=text("0"))
    OrbitBearing = Column(Float, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("0"))
    TradeLocation = Column(Integer, server_default=text("0"))
    CivilianFunction = Column(Integer, server_default=text("0"))
    NPRHomeGuard = Column(Boolean, server_default=text("NULL"))
    TFTraining = Column(Boolean, server_default=text("NULL"))
    SpecialOrderID = Column(Integer, server_default=text("0"))
    SpecialOrderID2 = Column(Integer, server_default=text("0"))
    Speed = Column(Integer, server_default=text("1"))
    MaxNebulaSpeed = Column(Integer, server_default=text("1"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("0"))
    LastXcor = Column(Float, server_default=text("0"))
    LastYcor = Column(Float, server_default=text("0"))
    LastMoveTime = Column(Float, server_default=text("0"))
    IncrementStartX = Column(Float, server_default=text("0"))
    IncrementStartY = Column(Float, server_default=text("0"))
    EntryJPID = Column(Integer, server_default=text("0"))
    CycleMoves = Column(Integer, server_default=text("0"))
    JustDivided = Column(Integer, server_default=text("0"))
    AxisContactID = Column(Integer, server_default=text("0"))
    Distance = Column(Integer, server_default=text("0"))
    OffsetBearing = Column(Integer, server_default=text("0"))
    ConditionOne = Column(Integer, server_default=text("0"))
    ConditionTwo = Column(Integer, server_default=text("0"))
    ConditionalOrderOne = Column(Integer, server_default=text("0"))
    ConditionalOrderTwo = Column(Integer, server_default=text("0"))
    AvoidDanger = Column(Boolean)
    AvoidAlienSystems = Column(Boolean)
    NPROperationalGroupID = Column(Integer, server_default=text("0"))
    DisplaySensors = Column(Boolean)
    DisplayWeapons = Column(Boolean)
    AssignedFormationID = Column(Integer, server_default=text("0"))
    ShippingLine = Column(Integer, server_default=text("0"))
    UseMaximumSpeed = Column(Boolean, server_default=text("TRUE"))
    RedeployOrderGiven = Column(Boolean, server_default=text("FALSE"))
    MaxStandingOrderDistance = Column(Integer, server_default=text("0"))
    NoSurrender = Column(Boolean, server_default=text("FALSE"))
    SpecificThreatID = Column(Integer, server_default=text("0"))
    AnchorFleetID = Column(Integer, server_default=text("0"))
    AnchorFleetDistance = Column(Float, server_default=text("0"))
    AnchorFleetBearingOffset = Column(Float, server_default=text("0"))
    GuardNearestHostileContact = Column(Boolean, server_default=text("FALSE"))
    UseAnchorDestination = Column(Boolean, server_default=text("FALSE"))
    GuardNearestKnownWarship = Column(Boolean, server_default=text("FALSE"))


t_FCT_FleetHistory = Table(
    'FCT_FleetHistory', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('FleetID', Integer, server_default=text("NULL")),
    Column('Description', Text, server_default=text("NULL")),
    Column('GameTime', Float, server_default=text("NULL"))
)


class FCTGame(Base):
    __tablename__ = 'FCT_Game'

    GameID = Column(Integer, primary_key=True)
    AutoJumpGates = Column(Integer, server_default=text("0"))
    CivilianShippingLinesActive = Column(Integer, server_default=text("1"))
    DefaultRaceID = Column(Integer, server_default=text("0"))
    DifficultyModifier = Column(Integer, server_default=text("100"))
    GameName = Column(Text, server_default=text("NULL"))
    GameTime = Column(Float, server_default=text("1"))
    GenerateNonTNOnly = Column(Integer, server_default=text("0"))
    GenerateNPRs = Column(Integer, server_default=text("0"))
    InexpFleets = Column(Integer, server_default=text("1"))
    Invaders = Column(Integer, server_default=text("0"))
    LastGrowthTime = Column(Float, server_default=text("0"))
    LastViewed = Column(Float, server_default=text("0"))
    LocalSystemChance = Column(Integer, server_default=text("50"))
    LocalSystemSpread = Column(Integer, server_default=text("15"))
    MinComets = Column(Integer, server_default=text("0"))
    NewRuinCreationChance = Column(Integer, server_default=text("20"))
    NoOverhauls = Column(Integer, server_default=text("0"))
    NonPlayerSystemDetection = Column(Integer, server_default=text("0"))
    NPRsGenerateSpoilers = Column(Integer, server_default=text("0"))
    NumberOfSystems = Column(Integer, server_default=text("1000"))
    OrbitalMotion = Column(Integer, server_default=text("0"))
    OrbitalMotionAst = Column(Integer, server_default=text("0"))
    PlayerExplorationTime = Column(Float, server_default=text("NULL"))
    PoliticalAdmirals = Column(Integer, server_default=text("1"))
    Precursors = Column(Integer, server_default=text("0"))
    RaceChance = Column(Integer, server_default=text("30"))
    RaceChanceNPR = Column(Integer, server_default=text("10"))
    RealisticPromotions = Column(Integer, server_default=text("1"))
    SMPassword = Column(Text, server_default=text("NULL"))
    StarSwarm = Column(Integer, server_default=text("0"))
    StartYear = Column(Integer, server_default=text("2020"))
    SubPulseLength = Column(Integer, server_default=text("0"))
    TruceCountdown = Column(Float, server_default=text("0"))
    UseKnownStars = Column(Integer, server_default=text("0"))
    MinConstructionPeriod = Column(Integer, server_default=text("432000"))
    MinGroundCombatPeriod = Column(Integer, server_default=text("28800"))
    LastGroundCombatTime = Column(Float, server_default=text("0"))
    SolDisaster = Column(Integer, server_default=text("0"))
    TechFromConquest = Column(Integer, server_default=text("1"))
    ResearchSpeed = Column(Integer, server_default=text("100"))
    TerraformingSpeed = Column(Integer, server_default=text("100"))
    AllowCivilianHarvesters = Column(Integer, server_default=text("1"))
    Rakhas = Column(Integer, server_default=text("0"))
    HumanNPRs = Column(Integer, server_default=text("0"))
    CurrentGroundCombat = Column(Boolean, server_default=text("FALSE"))
    SurveySpeed = Column(Integer, server_default=text("100"))
    MaxEventDays = Column(Integer, server_default=text("60"))
    MaxEventCount = Column(Integer, server_default=text("300"))


t_FCT_GameLog = Table(
    'FCT_GameLog', metadata,
    Column('IncrementID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("NULL")),
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('SMOnly', Integer, server_default=text("NULL")),
    Column('Time', Float, server_default=text("NULL")),
    Column('EventType', Integer, server_default=text("0")),
    Column('MessageText', Text, server_default=text("NULL")),
    Column('SystemID', Integer, server_default=text("NULL")),
    Column('Xcor', Float, server_default=text("NULL")),
    Column('Ycor', Float, server_default=text("NULL")),
    Column('IDType', Integer, server_default=text("0")),
    Column('PopulationID', Integer, server_default=text("0"))
)

t_FCT_GroundUnitCapability = Table(
    'FCT_GroundUnitCapability', metadata,
    Column('GroundUnitClassID', Integer),
    Column('CapabilityID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTGroundUnitClas(Base):
    __tablename__ = 'FCT_GroundUnitClass'

    BaseType = Column(Integer)
    GroundUnitClassID = Column(Integer, primary_key=True, server_default=text("0"))
    TechSystemID = Column(Integer, server_default=text("0"))
    ClassName = Column(Text)
    ArmourType = Column(Integer, server_default=text("0"))
    ComponentA = Column(Integer, server_default=text("-1"))
    ComponentB = Column(Integer, server_default=text("-1"))
    ComponentC = Column(Integer, server_default=text("-1"))
    ArmourStrengthModifier = Column(Float, server_default=text("1"))
    WeaponStrengthModifier = Column(Float, server_default=text("1"))
    Size = Column(Float, server_default=text("0"))
    Cost = Column(Float, server_default=text("0"))
    GameID = Column(Integer)
    STOWeapon = Column(Integer, server_default=text("-1"))
    ComponentD = Column(Integer, server_default=text("-1"))
    TrackingSpeed = Column(Integer, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))
    ECCM = Column(Integer, server_default=text("0"))
    GUClassType = Column(Integer, server_default=text("0"))
    UnitSupplyCost = Column(Float, server_default=text("0"))
    ActiveSensorRange = Column(Integer, server_default=text("0"))
    SensorStrength = Column(Float, server_default=text("0"))
    RechargeTime = Column(Integer, server_default=text("0"))
    MaxWeaponRange = Column(Integer, server_default=text("0"))
    MaxFireControlRange = Column(Integer, server_default=text("0"))
    ConstructionRating = Column(Float, server_default=text("0"))
    HQCapacity = Column(Integer, server_default=text("0"))
    NonCombatClass = Column(Boolean, server_default=text("FALSE"))
    ClassGroundUnitSeriesPriority = Column(Integer, server_default=text("0"))
    ClassGroundUnitSeries = Column(Integer, server_default=text("0"))


class FCTGroundUnitFormation(Base):
    __tablename__ = 'FCT_GroundUnitFormation'

    FormationID = Column(Integer, primary_key=True)
    Name = Column(Text)
    Abbreviation = Column(Text)
    RaceID = Column(Integer)
    GameID = Column(Integer)
    OriginalTemplateID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    ShipID = Column(Integer, server_default=text("0"))
    ParentFormationID = Column(Integer, server_default=text("0"))
    BoardingStatus = Column(Integer, server_default=text("0"))
    HideSubUnits = Column(Boolean, server_default=text("False"))
    FieldPosition = Column(Integer, server_default=text("0"))
    RequiredRank = Column(Integer, server_default=text("0"))
    AssignedFormationID = Column(Integer, server_default=text("0"))
    ActiveSensorsOn = Column(Boolean, server_default=text("false"))
    Civilian = Column(Boolean, server_default=text("FALSE"))
    ReplacementTemplateID = Column(Integer, server_default=text("0"))
    UseForReplacements = Column(Boolean, server_default=text("FALSE"))
    ReplacementPriority = Column(Integer, server_default=text("10"))


t_FCT_GroundUnitFormationElement = Table(
    'FCT_GroundUnitFormationElement', metadata,
    Column('GameID', Integer),
    Column('FormationID', Integer),
    Column('Units', Integer),
    Column('ClassID', Integer),
    Column('TemplateID', Integer),
    Column('SpeciesID', Integer, server_default=text("0")),
    Column('Morale', Integer, server_default=text("0")),
    Column('FortificationLevel', Float, server_default=text("1")),
    Column('CurrentSupply', Integer, server_default=text("10")),
    Column('TargetSelection', Integer, server_default=text("0")),
    Column('FiringDistribution', Integer, server_default=text("0")),
    Column('ElementID', Integer, server_default=text("0"))
)

t_FCT_GroundUnitFormationElementTemplates = Table(
    'FCT_GroundUnitFormationElementTemplates', metadata,
    Column('FormationTemplateID', Integer),
    Column('GameID', Integer),
    Column('ClassID', Integer),
    Column('Units', Integer)
)

t_FCT_GroundUnitFormationTemplate = Table(
    'FCT_GroundUnitFormationTemplate', metadata,
    Column('GameID', Integer),
    Column('RaceID', Integer),
    Column('Name', Text),
    Column('Abbreviation', Text),
    Column('TemplateID', Integer),
    Column('RequiredRank', Integer, server_default=text("0")),
    Column('AutomatedTemplateID', Integer, server_default=text("0"))
)

t_FCT_GroundUnitSeries = Table(
    'FCT_GroundUnitSeries', metadata,
    Column('GroundUnitSeriesID', Integer, server_default=text("0")),
    Column('Description', Text),
    Column('GameID', Integer, server_default=text("0")),
    Column('RaceID', Integer, server_default=text("0"))
)

t_FCT_GroundUnitSeriesClass = Table(
    'FCT_GroundUnitSeriesClass', metadata,
    Column('GroundUnitSeriesID', Integer, server_default=text("0")),
    Column('GroundUnitClassID', Integer, server_default=text("0")),
    Column('RaceID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('Priority', Integer, server_default=text("0"))
)


class FCTGroundUnitTraining(Base):
    __tablename__ = 'FCT_GroundUnitTraining'

    TaskID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    TotalBP = Column(Float, server_default=text("0"))
    CompletedBP = Column(Float, server_default=text("0"))
    FormationTemplateID = Column(Integer, server_default=text("0"))
    FormationName = Column(Text, server_default=text("NULL"))


t_FCT_GroundUnitTrainingQueue = Table(
    'FCT_GroundUnitTrainingQueue', metadata,
    Column('QueueID', Integer),
    Column('GameID', Integer),
    Column('PopulationID', Integer),
    Column('FormationTemplateID', Integer, server_default=text("0")),
    Column('FormationName', Text, server_default=text("\"\""))
)

t_FCT_HideEvents = Table(
    'FCT_HideEvents', metadata,
    Column('RaceID', Integer, nullable=False),
    Column('EventID', Integer, nullable=False),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTHullDescription(Base):
    __tablename__ = 'FCT_HullDescription'

    HullDescriptionID = Column(Integer, primary_key=True)
    Description = Column(String(50), server_default=text("NULL"))
    HullAbbr = Column(String(10), server_default=text("NULL"))


class FCTIncrement(Base):
    __tablename__ = 'FCT_Increments'

    IncrementID = Column(Integer, primary_key=True, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    GameTime = Column(Float, server_default=text("0"))
    Length = Column(Integer, server_default=text("0"))


class FCTIndustrialProject(Base):
    __tablename__ = 'FCT_IndustrialProjects'

    ProjectID = Column(Integer, primary_key=True, server_default=text("0"))
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    PopulationID = Column(Integer, server_default=text("NULL"))
    SpeciesID = Column(Integer, server_default=text("0"))
    Percentage = Column(Float, server_default=text("NULL"))
    ProductionType = Column(Integer, server_default=text("NULL"))
    ProductionID = Column(Integer, server_default=text("NULL"))
    RefitClassID = Column(Integer, server_default=text("0"))
    WealthUse = Column(Integer, server_default=text("NULL"))
    Amount = Column(Float, server_default=text("NULL"))
    PartialCompletion = Column(Float, server_default=text("0"))
    ProdPerUnit = Column(Float, server_default=text("NULL"))
    Description = Column(String(255), server_default=text("NULL"))
    Pause = Column(Boolean, server_default=text("NULL"))
    Queue = Column(Integer, server_default=text("0"))
    FuelRequired = Column(Integer, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))


class FCTJumpPoint(Base):
    __tablename__ = 'FCT_JumpPoint'

    WarpPointID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    SystemID = Column(Integer, server_default=text("0"))
    Distance = Column(Float, server_default=text("0"))
    Bearing = Column(Integer, server_default=text("0"))
    WPLink = Column(Integer, server_default=text("0"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    JumpGateStrength = Column(Integer, server_default=text("0"))
    JumpGateRaceID = Column(Integer, server_default=text("0"))


class FCTKnownRuinRace(Base):
    __tablename__ = 'FCT_KnownRuinRace'

    RuinRaceID = Column(Integer, primary_key=True, nullable=False)
    RaceID = Column(Integer, primary_key=True, nullable=False)
    GameID = Column(Integer, server_default=text("0"))


t_FCT_KnownSpecies = Table(
    'FCT_KnownSpecies', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('SpeciesID', Integer, server_default=text("0")),
    Column('ViewRaceID', Integer, server_default=text("0")),
    Column('Status', Integer, server_default=text("0"))
)


class FCTLagrangePoint(Base):
    __tablename__ = 'FCT_LagrangePoint'

    LagrangePointID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    SystemID = Column(Integer, server_default=text("NULL"))
    StarID = Column(Integer, server_default=text("NULL"))
    PlanetID = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    Distance = Column(Float, server_default=text("NULL"))
    Bearing = Column(Float, server_default=text("NULL"))


class FCTLifepod(Base):
    __tablename__ = 'FCT_Lifepods'

    LifepodID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SpeciesID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    ClassID = Column(Integer, server_default=text("0"))
    ShipName = Column(String(50), server_default=text("NULL"))
    Crew = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    CreationTime = Column(Float, server_default=text("NULL"))
    GradePoints = Column(Float, server_default=text("NULL"))


t_FCT_MapLabel = Table(
    'FCT_MapLabel', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('Caption', Text, server_default=text("NULL")),
    Column('Colour', Integer, server_default=text("NULL")),
    Column('FontSize', Integer, server_default=text("NULL")),
    Column('FontBold', Boolean, server_default=text("NULL")),
    Column('FontItalic', Boolean, server_default=text("NULL")),
    Column('FontName', Text, server_default=text("NULL")),
    Column('Xcor', Float, server_default=text("NULL")),
    Column('Ycor', Float, server_default=text("NULL"))
)


class FCTMassDriverPacket(Base):
    __tablename__ = 'FCT_MassDriverPackets'

    PacketID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SysID = Column(Integer, server_default=text("NULL"))
    DestID = Column(Integer, server_default=text("NULL"))
    TotalSize = Column(Float, server_default=text("NULL"))
    Speed = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    LastXcor = Column(Float, server_default=text("NULL"))
    LastYcor = Column(Float, server_default=text("NULL"))
    IncrementStartX = Column(Float, server_default=text("0"))
    IncrementStartY = Column(Float, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))


t_FCT_MedalConditionAssignment = Table(
    'FCT_MedalConditionAssignment', metadata,
    Column('MedalConditionID', Integer, server_default=text("0")),
    Column('MedalID', Integer, server_default=text("0")),
    Column('GameID', Integer)
)

t_FCT_MineralDeposit = Table(
    'FCT_MineralDeposit', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('MaterialID', Integer, server_default=text("NULL")),
    Column('SystemID', Integer, server_default=text("NULL")),
    Column('SystemBodyID', Integer, server_default=text("NULL")),
    Column('Amount', Float, server_default=text("NULL")),
    Column('Accessibility', Float, server_default=text("NULL")),
    Column('HalfOriginalAmount', Float, server_default=text("NULL")),
    Column('OriginalAcc', Float, server_default=text("NULL"))
)

t_FCT_MissileAssignment = Table(
    'FCT_MissileAssignment', metadata,
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('MissileID', Integer, server_default=text("NULL")),
    Column('WeaponID', Integer, server_default=text("NULL")),
    Column('WeaponNum', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTMissileGeoSurvey(Base):
    __tablename__ = 'FCT_MissileGeoSurvey'

    SystemBodyID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    SurveyPoints = Column(Float, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))


class FCTMissileSalvo(Base):
    __tablename__ = 'FCT_MissileSalvo'

    MissileSalvoID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    ShipID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))
    SystemID = Column(Integer, server_default=text("NULL"))
    OrbitBodyID = Column(Integer, server_default=text("0"))
    MissileID = Column(Integer, server_default=text("NULL"))
    MissileNum = Column(Integer, server_default=text("NULL"))
    LaunchTime = Column(Float, server_default=text("1"))
    FireControlID = Column(Integer, server_default=text("0"))
    FCNum = Column(Integer, server_default=text("0"))
    TargetID = Column(Integer, server_default=text("0"))
    TargetType = Column(Integer, server_default=text("0"))
    MissileSpeed = Column(Integer, server_default=text("0"))
    ModifierToHit = Column(Float, server_default=text("1"))
    Endurance = Column(Float, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    LastXcor = Column(Float, server_default=text("NULL"))
    LastYcor = Column(Float, server_default=text("NULL"))
    LastTargetX = Column(Float, server_default=text("0"))
    LastTargetY = Column(Float, server_default=text("0"))
    IncrementStartX = Column(Float, server_default=text("0"))
    IncrementStartY = Column(Float, server_default=text("0"))
    HomingMethod = Column(Integer, server_default=text("0"))


class FCTMissile(Base):
    __tablename__ = 'FCT_Missiles'

    MissileID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    Name = Column(Text, server_default=text("NULL"))
    MissileSeriesID = Column(Integer, server_default=text("0"))
    EngineID = Column(Integer, server_default=text("0"))
    NumEngines = Column(Integer, server_default=text("0"))
    PrecursorMissile = Column(Boolean, server_default=text("NULL"))
    MissilesRequired = Column(Integer, server_default=text("0"))
    MissilesAvailable = Column(Integer, server_default=text("0"))
    Cost = Column(Float, server_default=text("0"))
    Size = Column(Float, server_default=text("NULL"))
    Speed = Column(Float, server_default=text("NULL"))
    RadDamage = Column(Integer, server_default=text("1"))
    FuelRequired = Column(Integer, server_default=text("0"))
    Endurance = Column(Float, server_default=text("0"))
    MaxRange = Column(Float, server_default=text("0"))
    WarheadStrength = Column(Integer, server_default=text("NULL"))
    SensorStrength = Column(Float, server_default=text("0"))
    SensorResolution = Column(Integer, server_default=text("0"))
    SensorRange = Column(Integer, server_default=text("0"))
    ThermalStrength = Column(Float, server_default=text("0"))
    EMStrength = Column(Float, server_default=text("0"))
    Geostrength = Column(Float, server_default=text("0"))
    TotalFlightTime = Column(Float, server_default=text("0"))
    ECM = Column(Integer, server_default=text("0"))
    MR = Column(Integer, server_default=text("10"))
    SecondStageID = Column(Integer, server_default=text("0"))
    NumSS = Column(Integer, server_default=text("0"))
    SeparationRange = Column(Integer, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))
    PreTNT = Column(Boolean, server_default=text("NULL"))
    MSPReactor = Column(Float, server_default=text("0"))
    MSPWarhead = Column(Float, server_default=text("0"))
    MSPEngine = Column(Float, server_default=text("0"))
    MSPFuel = Column(Float, server_default=text("0"))
    MSPAgility = Column(Float, server_default=text("0"))
    MSPActive = Column(Float, server_default=text("0"))
    MSPThermal = Column(Float, server_default=text("0"))
    MSPEM = Column(Float, server_default=text("0"))
    MSPGeo = Column(Float, server_default=text("0"))
    MSPArmour = Column(Float, server_default=text("0"))
    ECCM = Column(Integer, server_default=text("0"))
    EMSensitivity = Column(Integer, server_default=text("10"))
    GroundAP = Column(Float, server_default=text("0"))
    GroundDamage = Column(Float, server_default=text("0"))
    GroundShots = Column(Float, server_default=text("0"))
    GroundBaseDamage = Column(Float, server_default=text("0"))
    PowerMod = Column(Float, server_default=text("1"))


t_FCT_MoveOrderTemplate = Table(
    'FCT_MoveOrderTemplate', metadata,
    Column('OrderTemplateID', Integer, server_default=text("NULL")),
    Column('PopulationID', Integer, server_default=text("0")),
    Column('MinDistance', Float, server_default=text("0")),
    Column('OrbDistance', Integer, server_default=text("0")),
    Column('MoveActionID', Integer, server_default=text("1")),
    Column('NewSystemID', Integer, server_default=text("0")),
    Column('Description', Text, server_default=text("NULL")),
    Column('OrderDelay', Integer, server_default=text("NULL")),
    Column('DestinationType', Integer, server_default=text("NULL")),
    Column('DestinationID', Integer, server_default=text("0")),
    Column('NewJumpPointID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('DestinationItemID', Integer, server_default=text("0")),
    Column('MaxItems', Integer, server_default=text("0")),
    Column('MessageText', Text, server_default=text("0")),
    Column('LoadSubUnits', Boolean, server_default=text("FALSE")),
    Column('MinQuantity', Float),
    Column('StartSystemID', Integer, server_default=text("0")),
    Column('MoveIndex', Integer, server_default=text("0")),
    Column('DestinationItemType', Integer),
    Column('SurveyPointsRequired', Float, server_default=text("0"))
)


class FCTMoveOrder(Base):
    __tablename__ = 'FCT_MoveOrders'

    MoveOrderID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("0"))
    FleetID = Column(Integer, server_default=text("0"))
    MoveActionID = Column(Integer, server_default=text("1"))
    MoveOrder = Column(Integer, server_default=text("0"))
    StartSystemID = Column(Integer, server_default=text("0"))
    DestinationType = Column(Integer, server_default=text("NULL"))
    DestinationID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    DestinationItemType = Column(Integer, server_default=text("0"))
    DestinationItemID = Column(Integer, server_default=text("0"))
    MaxItems = Column(Float, server_default=text("0"))
    OrderDelay = Column(Integer, server_default=text("0"))
    OrbDistance = Column(Integer, server_default=text("0"))
    MinDistance = Column(Float, server_default=text("0"))
    NewSystemID = Column(Integer, server_default=text("0"))
    NewWarpPointID = Column(Integer, server_default=text("0"))
    Description = Column(Text, server_default=text("NULL"))
    Arrived = Column(Boolean, server_default=text("0"))
    SurveyPointsRequired = Column(Float, server_default=text("0"))
    TimeRequired = Column(Integer, server_default=text("0"))
    MessageText = Column(Text, server_default=text("NULL"))
    LoadSubUnits = Column(Boolean, server_default=text("0"))
    MinQuantity = Column(Float, server_default=text("0"))
    OrderDelayRemaining = Column(Integer, server_default=text("0"))


class FCTNavalAdminCommand(Base):
    __tablename__ = 'FCT_NavalAdminCommand'

    NavalAdminCommandID = Column(Integer, primary_key=True, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    ParentAdminCommandID = Column(Integer, server_default=text("0"))
    AdminCommandName = Column(Text, server_default=text("NULL"))
    AdminCommandTypeID = Column(Integer, server_default=text("0"))


class FCTOrderTemplate(Base):
    __tablename__ = 'FCT_OrderTemplate'

    OrderTemplateID = Column(Integer, primary_key=True)
    TemplateName = Column(Text, server_default=text("NULL"))
    StartSystemID = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))


t_FCT_PausedResearch = Table(
    'FCT_PausedResearch', metadata,
    Column('TechSystemID', Integer, server_default=text("NULL")),
    Column('PopulationID', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("NULL")),
    Column('PointsAccumulated', Integer, server_default=text("NULL"))
)


class FCTPopComponent(Base):
    __tablename__ = 'FCT_PopComponent'

    ComponentID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    PopulationID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    Amount = Column(Float, server_default=text("NULL"))


t_FCT_PopInstallationDemand = Table(
    'FCT_PopInstallationDemand', metadata,
    Column('PopulationID', Integer, server_default=text("NULL")),
    Column('InstallationID', Integer, server_default=text("NULL")),
    Column('Amount', Float, server_default=text("NULL")),
    Column('Export', Boolean, server_default=text("NULL")),
    Column('GameID', Integer),
    Column('NonEssential', Boolean, server_default=text("FALSE"))
)


class FCTPopMDChange(Base):
    __tablename__ = 'FCT_PopMDChanges'

    PopulationID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))


class FCTPopTradeBalance(Base):
    __tablename__ = 'FCT_PopTradeBalance'

    PopulationID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    TradeGoodID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    ProductionRate = Column(Float, server_default=text("NULL"))
    TradeBalance = Column(Float, server_default=text("NULL"))
    LastTradeBalance = Column(Float, server_default=text("NULL"))


class FCTPopulation(Base):
    __tablename__ = 'FCT_Population'

    PopulationID = Column(Integer, primary_key=True, unique=True)
    GameID = Column(Integer, index=True)
    RaceID = Column(Integer, index=True)
    SpeciesID = Column(Integer)
    PopName = Column(Text)
    AcademyOfficers = Column(Float)
    Capital = Column(Numeric)
    TerraformStatus = Column(Integer)
    PurchaseCivilianMinerals = Column(Numeric)
    ColonistDestination = Column(Integer)
    Efficiency = Column(Float)
    FighterDestFleetID = Column(Integer)
    FuelProdStatus = Column(Numeric)
    FuelStockpile = Column(Float)
    GenModSpeciesID = Column(Integer)
    GroundAttackID = Column(Integer)
    GroundAttackType = Column(Integer)
    LastColonyCost = Column(Float)
    MaintenanceStockpile = Column(Float)
    MaintProdStatus = Column(Numeric)
    MassDriverDest = Column(Integer)
    MaxAtm = Column(Float)
    NoStatusChange = Column(Integer)
    PoliticalStatus = Column(Integer)
    Population = Column(Float)
    PreviousUnrest = Column(Float)
    ProvideColonists = Column(Integer)
    ReqInf = Column(Integer)
    StatusPoints = Column(Float)
    SystemID = Column(Integer)
    SystemBodyID = Column(Integer)
    TempMF = Column(Integer)
    TerraformingGasID = Column(Integer)
    UnrestPoints = Column(Float)
    Duranium = Column(Float)
    Neutronium = Column(Float)
    Corbomite = Column(Float)
    Tritanium = Column(Float)
    Boronide = Column(Float)
    Mercassium = Column(Float)
    Vendarite = Column(Float)
    Sorium = Column(Float)
    Uridium = Column(Float)
    Corundium = Column(Float)
    Gallicite = Column(Float)
    LastDuranium = Column(Float)
    LastNeutronium = Column(Float)
    LastCorbomite = Column(Float)
    LastTritanium = Column(Float)
    LastBoronide = Column(Float)
    LastMercassium = Column(Float)
    LastVendarite = Column(Float)
    LastSorium = Column(Float)
    LastUridium = Column(Float)
    LastCorundium = Column(Float)
    LastGallicite = Column(Float)
    ReserveDuranium = Column(Float)
    ReserveNeutronium = Column(Float)
    ReserveCorbomite = Column(Float)
    ReserveTritanium = Column(Float)
    ReserveBoronide = Column(Float)
    ReserveMercassium = Column(Float)
    ReserveVendarite = Column(Float)
    ReserveSorium = Column(Float)
    ReserveUridium = Column(Float)
    ReserveCorundium = Column(Float)
    ReserveGallicite = Column(Float)
    GroundGeoSurvey = Column(Float)
    DestroyedInstallationSize = Column(Integer)
    AIValue = Column(Integer)
    InvasionStagingPoint = Column(Numeric)
    OriginalRaceID = Column(Integer)
    DoNotDelete = Column(Numeric)
    MilitaryRestrictedColony = Column(Numeric)
    AcademyName = Column(Text)
    SpaceStationDestFleetID = Column(Integer, server_default=text("0"))
    Importance = Column(Integer, server_default=text("0"))
    AutoAssign = Column(Boolean, server_default=text("FALSE"))
    BonusOne = Column(Integer, server_default=text("8"))
    BonusTwo = Column(Integer, server_default=text("20"))
    BonusThree = Column(Integer, server_default=text("6"))


class FCTPopulationInstallation(Base):
    __tablename__ = 'FCT_PopulationInstallations'

    GameID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    PopID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    PlanetaryInstallationID = Column(Integer, primary_key=True, nullable=False, server_default=text("0"))
    Amount = Column(Float, server_default=text("0"))


# t_FCT_PopulationInstallations = Table(
#     'FCT_PopulationInstallations', metadata,
#     Column('GameID', Integer, server_default=text("0")),
#     Column('PopID', Integer, server_default=text("0")),
#     Column('PlanetaryInstallationID', Integer, server_default=text("0")),
#     Column('Amount', Float, server_default=text("0"))
# )


t_FCT_PopulationWeapon = Table(
    'FCT_PopulationWeapon', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('PopulationID', Integer, server_default=text("NULL")),
    Column('MissileID', Integer, server_default=text("NULL")),
    Column('Amount', Float, server_default=text("NULL"))
)

t_FCT_Prisoners = Table(
    'FCT_Prisoners', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('PopulationID', Integer, server_default=text("NULL")),
    Column('PrisonerRaceID', Integer, server_default=text("NULL")),
    Column('PrisonerSpeciesID', Integer, server_default=text("NULL")),
    Column('NumPrisoners', Integer, server_default=text("NULL"))
)


class FCTRace(Base):
    __tablename__ = 'FCT_Race'

    RaceID = Column(Integer, primary_key=True)
    GameID = Column(Integer)
    ConventionalStart = Column(Numeric)
    NPR = Column(Numeric)
    SpecialNPRID = Column(Integer)
    PreIndustrial = Column(Numeric)
    RaceGridSize = Column(Integer)
    RaceName = Column(Text)
    RaceTitle = Column(Text)
    WealthPoints = Column(Float)
    PreviousWealth = Column(Float)
    StartingWealth = Column(Float)
    AnnualWealth = Column(Integer)
    GovTypeID = Column(Integer)
    UnreadMessages = Column(Numeric)
    FlagPic = Column(Text)
    HullPic = Column(Text)
    Contacts = Column(Integer)
    Colour = Column(Integer)
    Red = Column(Integer)
    Green = Column(Integer)
    Blue = Column(Integer)
    Password = Column(Text)
    ThemeID = Column(Integer)
    ClassThemeID = Column(Integer)
    SystemThemeID = Column(Integer)
    NameThemeID = Column(Integer)
    MinSystem = Column(Integer)
    MaxSystem = Column(Integer)
    MapSize = Column(Integer)
    DisplayGrade = Column(Integer)
    ShowHighlight = Column(Integer)
    MapRed = Column(Integer)
    MapGreen = Column(Integer)
    MapBlue = Column(Integer)
    FleetViewOption = Column(Integer)
    SelectRace = Column(Integer)
    FleetsVisible = Column(Numeric)
    LastMapSystemViewed = Column(Integer)
    chkAllowAny = Column(Integer)
    chkAutoAssign = Column(Integer)
    chkTons = Column(Integer)
    StandardTour = Column(Integer)
    LastAssignment = Column(Float)
    CurrentXenophobia = Column(Integer)
    AcademyCrewmen = Column(Float)
    MaintenanceCapacity = Column(Integer)
    OrdnanceCapacity = Column(Float)
    FighterCapacity = Column(Float)
    ShipBuilding = Column(Float)
    FuelProduction = Column(Float)
    ConstructionProduction = Column(Float)
    OrdnanceProduction = Column(Float)
    FighterProduction = Column(Float)
    MineProduction = Column(Float)
    Research = Column(Integer)
    PlanetarySensorStrength = Column(Integer)
    TrainingLevel = Column(Integer)
    GUStrength = Column(Integer)
    GUTrained = Column(Integer)
    TerraformingRate = Column(Float)
    ColonizationSkill = Column(Float)
    StartTechPoints = Column(Integer)
    StartBuildPoints = Column(Integer)
    WealthCreationRate = Column(Float)
    EconomicProdModifier = Column(Float)
    ShipyardOperations = Column(Float)
    MSPProduction = Column(Integer)
    MaxRefuellingRate = Column(Integer)
    UnderwayReplenishmentRate = Column(Float, server_default=text("0"))
    Hostile = Column(Integer)
    Neutral = Column(Integer)
    Friendly = Column(Integer)
    Allied = Column(Integer)
    Civilian = Column(Integer)
    DisplayFunction = Column(Integer)
    chkPlanets = Column(Integer)
    chkDwarf = Column(Integer)
    chkMoons = Column(Integer)
    chkAst = Column(Integer)
    chkWP = Column(Integer)
    chkStarOrbits = Column(Integer)
    chkPlanetOrbits = Column(Integer)
    chkDwarfOrbits = Column(Integer)
    chkMoonOrbits = Column(Integer)
    chkAsteroidOrbits = Column(Integer)
    chkStarNames = Column(Integer)
    chkPlanetNames = Column(Integer)
    chkDwarfNames = Column(Integer)
    chkMoonNames = Column(Integer)
    chkAstNames = Column(Integer)
    chkFleets = Column(Integer)
    chkMoveTail = Column(Integer)
    chkColonies = Column(Integer)
    chkCentre = Column(Integer)
    chkSL = Column(Integer)
    chkWaypoint = Column(Integer)
    chkOrder = Column(Integer)
    chkNoOverlap = Column(Integer)
    chkActiveSensors = Column(Integer)
    chkTracking = Column(Integer)
    chkActiveOnly = Column(Integer)
    chkShowDist = Column(Integer)
    chkSBSurvey = Column(Integer)
    chkMinerals = Column(Integer)
    chkCometPath = Column(Integer)
    chkAstColOnly = Column(Integer)
    chkAstMinOnly = Column(Integer)
    chkTAD = Column(Integer)
    chkFiringRanges = Column(Integer)
    chkSalvoOrigin = Column(Integer)
    chkSalvoTarget = Column(Integer)
    chkEscorts = Column(Integer)
    chkFireControlRange = Column(Integer)
    PassiveSensor = Column(Integer)
    ActiveSensor = Column(Integer)
    DetRange = Column(Integer)
    chkHideIDs = Column(Integer)
    chkHideSL = Column(Integer)
    chkEvents = Column(Integer)
    chkPackets = Column(Integer)
    chkMPC = Column(Integer)
    chkLifepods = Column(Integer)
    chkWrecks = Column(Integer)
    chkHostileSensors = Column(Integer)
    chkGeoPoints = Column(Integer)
    chkBearing = Column(Integer)
    chkCoordinates = Column(Integer)
    chkLostContacts = Column(Integer)
    chkHideCivNames = Column(Integer)
    chkSystemOnly = Column(Integer)
    chkShowCivilianOOB = Column(Integer)
    chkHostile = Column(Integer)
    chkFriendly = Column(Integer)
    chkAllied = Column(Integer)
    chkNeutral = Column(Integer)
    chkCivilian = Column(Integer)
    chkContactsCurrentSystem = Column(Integer)
    chkTranspondersOn = Column(Integer)
    RankScientist = Column(Text)
    RankAdministrator = Column(Text)
    chkUnexJP = Column(Integer, server_default=text("1"))
    chkJPSurveyStatus = Column(Integer, server_default=text("0"))
    chkSurveyLocationPoints = Column(Integer, server_default=text("0"))
    chkSurveyedSystemBodies = Column(Integer, server_default=text("0"))
    chkDistanceFromSelected = Column(Integer, server_default=text("0"))
    chkWarshipLocation = Column(Integer, server_default=text("1"))
    chkAllFleetLocations = Column(Integer, server_default=text("0"))
    chkKnownAlienForces = Column(Integer, server_default=text("1"))
    chkAlienControlledSystems = Column(Integer, server_default=text("1"))
    chkPopulatedSystem = Column(Integer, server_default=text("1"))
    chkShipyardComplexes = Column(Integer, server_default=text("1"))
    chkNavalHeadquarters = Column(Integer, server_default=text("1"))
    chkSectors = Column(Integer, server_default=text("0"))
    chkPossibleDormantJP = Column(Integer, server_default=text("1"))
    chkSecurityStatus = Column(Integer, server_default=text("0"))
    chkDiscoveryDate = Column(Integer, server_default=text("0"))
    chkHabRangeWorlds = Column(Integer, server_default=text("1"))
    chkLowCCNormalG = Column(Integer, server_default=text("1"))
    chkMediumCCNormalG = Column(Integer, server_default=text("1"))
    chkLowCCLowG = Column(Integer, server_default=text("0"))
    chkMediumCCLowG = Column(Integer, server_default=text("0"))
    SpaceStationPic = Column(Text)
    MaxOrdnanceTransferRate = Column(Integer, server_default=text("40"))
    CargoShuttleLoadModifier = Column(Integer, server_default=text("1"))
    GroundFormationConstructionRate = Column(Integer, server_default=text("100"))
    DesignThemeID = Column(Integer, server_default=text("1"))
    ResearchTargetCost = Column(Integer, server_default=text("0"))
    CurrentResearchTotal = Column(Integer, server_default=text("0"))
    chkPassive10 = Column(Integer)
    chkPassive100 = Column(Integer, server_default=text("0"))
    chkPassive1000 = Column(Integer, server_default=text("0"))
    GroundThemeID = Column(Integer, server_default=text("0"))
    MaximumOrbitalMiningDiameter = Column(Integer, server_default=text("100"))
    ColonyDensity = Column(Integer, server_default=text("5"))
    MissileThemeID = Column(Integer, server_default=text("0"))
    NeutralRace = Column(Boolean, server_default=text("0"))
    chkNumCometsPlanetlessSystem = Column(Integer, server_default=text("0"))
    chkPassive10000 = Column(Integer, server_default=text("0"))
    chkGroundSurveyLocations = Column(Integer, server_default=text("0"))
    chkML = Column(Integer, server_default=text("0"))
    chkBlocked = Column(Integer, server_default=text("0"))
    chkMilitaryRestricted = Column(Integer, server_default=text("0"))
    HideCMCPop = Column(Boolean, server_default=text("FALSE"))
    ShowPopStar = Column(Boolean, server_default=text("FALSE"))
    ShowPopSystemBody = Column(Boolean, server_default=text("FALSE"))
    PopByFunction = Column(Boolean, server_default=text("FALSE"))
    chkLostContactsOneYear = Column(Integer, server_default=text("0"))
    chkDisplayMineralSearch = Column(Integer, server_default=text("0"))
    chkLostContactsOneDay = Column(Integer, server_default=text("0"))
    chkHabRangeWorldsLowG = Column(Integer, server_default=text("0"))
    chkAllRace = Column(Integer, server_default=text("0"))
    chkDisplayAllForms = Column(Integer, server_default=text("0"))
    TonnageSent = Column(Float, server_default=text("0"))
    LastProgressionOrder = Column(Integer, server_default=text("0"))


t_FCT_RaceGroundCombat = Table(
    'FCT_RaceGroundCombat', metadata,
    Column('RaceID', Integer),
    Column('SystemBodyID', Integer),
    Column('ConsecutiveCombatRounds', Integer, server_default=text("1")),
    Column('GameID', Integer)
)

t_FCT_RaceJumpPointSurvey = Table(
    'FCT_RaceJumpPointSurvey', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('RaceID', Integer, nullable=False),
    Column('WarpPointID', Integer, nullable=False),
    Column('Explored', Integer, server_default=text("0")),
    Column('Charted', Integer, server_default=text("0")),
    Column('AlienUnits', Boolean, server_default=text("NULL")),
    Column('Hide', Integer, server_default=text("0"))
)


class FCTRaceMedal(Base):
    __tablename__ = 'FCT_RaceMedals'

    RaceID = Column(Integer, index=True, server_default=text("NULL"))
    MedalFileName = Column(Text, server_default=text("NULL"))
    MedalName = Column(Text, server_default=text("NULL"))
    MedalDescription = Column(Text, server_default=text("'Award Details'"))
    MedalPoints = Column(Integer, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    MultipleAwards = Column(Boolean, server_default=text("0"))
    MedalID = Column(Integer, primary_key=True, server_default=text("0"))
    Abbreviation = Column(Text)


class FCTRaceNameTheme(Base):
    __tablename__ = 'FCT_RaceNameThemes'

    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    NameThemeID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    Chance = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer)


t_FCT_RaceOperationalGroupElements = Table(
    'FCT_RaceOperationalGroupElements', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('RaceID', Integer, server_default=text("0")),
    Column('OperationalGroupID', Integer, server_default=text("0")),
    Column('NumShips', Integer, server_default=text("0")),
    Column('AutomatedDesignID', Integer, server_default=text("0")),
    Column('KeyElement', Boolean, server_default=text("FALSE"))
)

t_FCT_RaceSurveyLocation = Table(
    'FCT_RaceSurveyLocation', metadata,
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("0")),
    Column('SystemID', Integer, server_default=text("NULL")),
    Column('LocationNumber', Integer, server_default=text("NULL"))
)


class FCTRaceSysSurvey(Base):
    __tablename__ = 'FCT_RaceSysSurvey'

    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, primary_key=True, nullable=False)
    SystemID = Column(Integer, primary_key=True, nullable=False)
    Name = Column(Text, nullable=False, server_default=text("'Not Yet Named'"))
    DangerRating = Column(Integer, server_default=text("0"))
    SysTPStatus = Column(Boolean, server_default=text("0"))
    ControlRaceID = Column(Integer, server_default=text("0"))
    ForeignFleetRaceID = Column(Integer, server_default=text("0"))
    SectorID = Column(Integer, server_default=text("0"))
    NameThemeID = Column(Integer, server_default=text("0"))
    Discovered = Column(Text, server_default=text("'0'"))
    Xcor = Column(Integer, nullable=False, server_default=text("2000"))
    Ycor = Column(Integer, nullable=False, server_default=text("2000"))
    ClosedWP = Column(Integer, server_default=text("0"))
    SurveyDone = Column(Boolean, server_default=text("0"))
    SelectedBodyXcor = Column(Float, server_default=text("0"))
    SelectedBodyYcor = Column(Float, server_default=text("0"))
    KmPerPixel = Column(Float, server_default=text("0"))
    GeoSurveyDefaultDone = Column(Boolean, server_default=text("0"))
    DiscoveredTime = Column(Float, server_default=text("0"))
    NoAutoRoute = Column(Boolean, server_default=text("False"))
    MilitaryRestrictedSystem = Column(Boolean, server_default=text("FALSE"))
    SystemValue = Column(Integer, server_default=text("1"))
    AutoProtectionStatus = Column(Integer, server_default=text("0"))
    MineralSearchFlag = Column(Boolean, server_default=text("FALSE"))


class FCTRaceTech(Base):
    __tablename__ = 'FCT_RaceTech'

    GameID = Column(Integer, server_default=text("0"), primary_key=True)
    TechID = Column(Integer, server_default=text("0"), primary_key=True)
    RaceID = Column(Integer, server_default=text("0"), primary_key=True)
    Obsolete = Column(Boolean, server_default=text("NULL"))


#
# t_FCT_RaceTech = Table(
#     'FCT_RaceTech', metadata,
#     Column('GameID', Integer, server_default=text("0")),
#     Column('TechID', Integer, server_default=text("0")),
#     Column('RaceID', Integer, server_default=text("0")),
#     Column('Obsolete', Boolean, server_default=text("NULL"))
# )


class FCTRank(Base):
    __tablename__ = 'FCT_Ranks'

    RankID = Column(Integer, primary_key=True)
    RaceID = Column(Integer, server_default=text("0"))
    RankName = Column(Text, server_default=text("NULL"))
    Priority = Column(Integer, server_default=text("0"))
    RankType = Column(Integer, server_default=text("0"))
    RankAbbrev = Column(Text, server_default=text("NULL"))
    GameID = Column(Integer)


class FCTResearchProject(Base):
    __tablename__ = 'FCT_ResearchProject'

    ProjectID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    TechID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    PopulationID = Column(Integer, server_default=text("NULL"))
    Facilities = Column(Integer, server_default=text("NULL"))
    ResSpecID = Column(Integer, server_default=text("NULL"))
    ResearchPointsRequired = Column(Float, server_default=text("NULL"))
    Pause = Column(Boolean, server_default=text("NULL"))
    AssignNew = Column(Boolean, server_default=text("FALSE"))


t_FCT_ResearchQueue = Table(
    'FCT_ResearchQueue', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('PopulationID', Integer, server_default=text("NULL")),
    Column('TechSystemID', Integer, server_default=text("NULL")),
    Column('CurrentProjectID', Integer, server_default=text("NULL")),
    Column('ResearchOrder', Integer, server_default=text("NULL"))
)


class FCTRuinRace(Base):
    __tablename__ = 'FCT_RuinRace'

    RuinRaceID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    Title = Column(Text, server_default=text("NULL"))
    Name = Column(Text, server_default=text("NULL"))
    RacePic = Column(Text, server_default=text("NULL"))
    FlagPic = Column(Text, server_default=text("NULL"))
    Level = Column(Integer, server_default=text("NULL"))


t_FCT_STODetected = Table(
    'FCT_STODetected', metadata,
    Column('GameID', Integer),
    Column('ElementID', Integer),
    Column('DetectingRaceID', Integer)
)


class FCTSectorCommand(Base):
    __tablename__ = 'FCT_SectorCommand'

    SectorCommandID = Column(Integer, primary_key=True)
    RaceID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    SectorName = Column(String(100), server_default=text("NULL"))
    Colour = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))


class FCTShip(Base):
    __tablename__ = 'FCT_Ship'

    ShipID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    FleetID = Column(Integer, server_default=text("NULL"))
    ShipName = Column(Text, server_default=text("NULL"))
    SubFleetID = Column(Integer, server_default=text("0"))
    ActiveSensorsOn = Column(Boolean, server_default=text("0"))
    AssignedMSID = Column(Integer, server_default=text("0"))
    Autofire = Column(Integer, server_default=text("0"))
    BoardingCombatClock = Column(Integer, server_default=text("0"))
    Constructed = Column(Float, server_default=text("1"))
    CrewMorale = Column(Float, server_default=text("1"))
    CurrentCrew = Column(Integer, server_default=text("0"))
    CurrentShieldStrength = Column(Float, server_default=text("NULL"))
    DamageControlID = Column(Integer, server_default=text("0"))
    Destroyed = Column(Boolean, server_default=text("0"))
    FireDelay = Column(Integer, server_default=text("0"))
    Fuel = Column(Float, server_default=text("0"))
    GradePoints = Column(Float, server_default=text("0"))
    HoldTechData = Column(Integer, server_default=text("0"))
    KillTonnageCommercial = Column(Integer, server_default=text("0"))
    KillTonnageMilitary = Column(Integer, server_default=text("0"))
    LastLaunchTime = Column(Float, server_default=text("0"))
    LastOverhaul = Column(Float, server_default=text("0"))
    LastShoreLeave = Column(Float, server_default=text("0"))
    LaunchMorale = Column(Float, server_default=text("0"))
    MaintenanceState = Column(Integer, server_default=text("0"))
    MothershipID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("NULL"))
    RefuelPriority = Column(Integer, server_default=text("0"))
    RefuelStatus = Column(Integer, server_default=text("0"))
    ScrapFlag = Column(Boolean, server_default=text("0"))
    SensorDelay = Column(Integer, server_default=text("0"))
    ShieldsActive = Column(Boolean, server_default=text("0"))
    ShipClassID = Column(Integer, server_default=text("NULL"))
    ShipFuelEfficiency = Column(Float, server_default=text("NULL"))
    ShipNotes = Column(Text)
    ShippingLineID = Column(Integer, server_default=text("0"))
    SpeciesID = Column(Integer, server_default=text("NULL"))
    SquadronID = Column(Integer, server_default=text("0"))
    SyncFire = Column(Integer, server_default=text("0"))
    TFPoints = Column(Float, server_default=text("0"))
    TransponderActive = Column(Integer, server_default=text("0"))
    OrdnanceTransferStatus = Column(Integer, server_default=text("0"))
    HangarLoadType = Column(Integer, server_default=text("0"))
    ResupplyPriority = Column(Integer, server_default=text("0"))
    CurrentMaintSupplies = Column(Float, server_default=text("0"))
    AutomatedDamageControl = Column(Boolean, server_default=text("1"))
    TractorTargetShipID = Column(Integer, server_default=text("0"))
    TractorTargetShipyardID = Column(Integer, server_default=text("0"))
    TractorParentShipID = Column(Integer, server_default=text("0"))
    OverhaulFactor = Column(Float, server_default=text("1"))
    BioEnergy = Column(Float, server_default=text("0"))
    LastMissileHitTime = Column(Float, server_default=text("0"))
    LastBeamHitTime = Column(Float, server_default=text("0"))
    LastDamageTime = Column(Float, server_default=text("0"))
    LastPenetratingDamageTime = Column(Float, server_default=text("0"))
    AssignedFormationID = Column(Integer, server_default=text("0"))


t_FCT_ShipCargo = Table(
    'FCT_ShipCargo', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ShipID', Integer, server_default=text("0")),
    Column('CargoTypeID', Numeric, server_default=text("NULL")),
    Column('CargoID', Integer, server_default=text("0")),
    Column('Amount', Float, server_default=text("0")),
    Column('SpeciesID', Integer, server_default=text("0")),
    Column('StartingPop', Integer, server_default=text("0")),
    Column('Neutral', Boolean, server_default=text("0"))
)


class FCTShipClass(Base):
    __tablename__ = 'FCT_ShipClass'

    ShipClassID = Column(Integer, primary_key=True)
    ClassName = Column(Text, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    ActiveSensorStrength = Column(Integer, server_default=text("0"))
    ArmourThickness = Column(Integer, server_default=text("0"))
    ArmourWidth = Column(Integer, server_default=text("0"))
    BaseFailureChance = Column(Float, server_default=text("1"))
    CargoCapacity = Column(Integer, server_default=text("0"))
    ClassCrossSection = Column(Float, server_default=text("NULL"))
    ClassThermal = Column(Float, server_default=text("0"))
    Collier = Column(Integer, server_default=text("0"))
    ColonistCapacity = Column(Integer, server_default=text("0"))
    CommanderPriority = Column(Integer, server_default=text("0"))
    MilitaryEngines = Column(Boolean, server_default=text("NULL"))
    ControlRating = Column(Integer, server_default=text("1"))
    ConscriptOnly = Column(Integer, server_default=text("0"))
    Cost = Column(Float, server_default=text("0"))
    Crew = Column(Integer, server_default=text("0"))
    CrewQuartersHS = Column(Float, server_default=text("0"))
    DCRating = Column(Integer, server_default=text("0"))
    ECM = Column(Integer, server_default=text("0"))
    EMSensorStrength = Column(Integer, server_default=text("0"))
    EnginePower = Column(Integer, server_default=text("0"))
    ESMaxDACRoll = Column(Integer, server_default=text("0"))
    FighterClass = Column(Boolean, server_default=text("NULL"))
    Commercial = Column(Boolean, server_default=text("0"))
    FuelCapacity = Column(Integer, server_default=text("0"))
    FuelEfficiency = Column(Float, server_default=text("0"))
    FuelTanker = Column(Integer, server_default=text("0"))
    GeoSurvey = Column(Integer, server_default=text("0"))
    GravSurvey = Column(Integer, server_default=text("0"))
    Harvesters = Column(Integer, server_default=text("0"))
    HullDescriptionID = Column(Integer, server_default=text("25"))
    JGConstructionTime = Column(Integer, server_default=text("0"))
    JumpDistance = Column(Integer, server_default=text("0"))
    JumpRating = Column(Integer, server_default=text("0"))
    Locked = Column(Boolean, server_default=text("0"))
    MagazineCapacity = Column(Float, server_default=text("0"))
    MaxChance = Column(Integer, server_default=text("0"))
    MaxDACRoll = Column(Integer, server_default=text("0"))
    MaxSpeed = Column(Integer, server_default=text("1"))
    MaintModules = Column(Integer, server_default=text("0"))
    MinimumFuel = Column(Integer, server_default=text("0"))
    MiningModules = Column(Integer, server_default=text("0"))
    NameThemeID = Column(Integer, server_default=text("0"))
    NoArmour = Column(Integer, server_default=text("0"))
    Notes = Column(Text)
    MainFunction = Column(Integer, server_default=text("0"))
    Obsolete = Column(Integer, server_default=text("0"))
    OtherRaceClassID = Column(Integer, server_default=text("0"))
    ParasiteCapacity = Column(Float, server_default=text("0"))
    PassiveSensorStrength = Column(Integer, server_default=text("1"))
    PlannedDeployment = Column(Float, server_default=text("3"))
    PreTNT = Column(Boolean, server_default=text("0"))
    ProtectionValue = Column(Float, server_default=text("NULL"))
    RankRequired = Column(Integer, server_default=text("0"))
    ReactorPower = Column(Integer, server_default=text("0"))
    RecreationalModule = Column(Boolean, server_default=text("NULL"))
    RefuelPriority = Column(Integer, server_default=text("0"))
    RefuellingRate = Column(Integer, server_default=text("10000"))
    RefuellingHub = Column(Integer, server_default=text("0"))
    RequiredPower = Column(Integer, server_default=text("0"))
    SalvageRate = Column(Integer, server_default=text("0"))
    SensorReduction = Column(Float, server_default=text("1"))
    ShieldStrength = Column(Integer, server_default=text("0"))
    Size = Column(Float, server_default=text("0"))
    STSTractor = Column(Integer, server_default=text("0"))
    SupplyShip = Column(Integer, server_default=text("0"))
    Terraformers = Column(Integer, server_default=text("0"))
    TotalNumber = Column(Integer, server_default=text("0"))
    CargoShuttleStrength = Column(Integer, server_default=text("0"))
    TroopCapacity = Column(Integer, server_default=text("0"))
    WorkerCapacity = Column(Integer, server_default=text("0"))
    MaintPriority = Column(Integer, server_default=text("0"))
    CommercialHangar = Column(Boolean, server_default=text("0"))
    ClassShippingLineID = Column(Integer, server_default=text("0"))
    MoraleCheckRequired = Column(Boolean, server_default=text("False"))
    OrdnanceTransferHub = Column(Integer, server_default=text("0"))
    OrdnanceTransferRate = Column(Integer, server_default=text("0"))
    TroopTransportType = Column(Integer, server_default=text("0"))
    AutomatedDesignID = Column(Integer, server_default=text("0"))
    MinimumSupplies = Column(Integer, server_default=text("0"))
    ResupplyPriority = Column(Integer, server_default=text("0"))
    MaintSupplies = Column(Integer, server_default=text("0"))
    ELINTRating = Column(Integer, server_default=text("0"))
    DiplomacyRating = Column(Integer, server_default=text("0"))
    CommercialJumpDrive = Column(Integer, server_default=text("0"))
    BioEnergyCapacity = Column(Integer, server_default=text("0"))
    SeniorCO = Column(Integer, server_default=text("0"))
    RandomShipNameFromTheme = Column(Integer, server_default=text("0"))
    PrefixName = Column(Text)
    SuffixName = Column(Text)


class FCTShipDesignComponent(Base):
    __tablename__ = 'FCT_ShipDesignComponents'

    SDComponentID = Column(Integer, primary_key=True, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    Name = Column(Text, server_default=text("NULL"))
    NoScrap = Column(Boolean, server_default=text("NULL"))
    MilitarySystem = Column(Boolean, server_default=text("0"))
    ShippingLineSystem = Column(Boolean, server_default=text("NULL"))
    BeamWeapon = Column(Boolean, server_default=text("0"))
    Crew = Column(Integer, server_default=text("0"))
    Size = Column(Float, server_default=text("0"))
    Cost = Column(Float, server_default=text("0"))
    ComponentTypeID = Column(Integer, server_default=text("NULL"))
    ComponentValue = Column(Float, server_default=text("1"))
    PowerRequirement = Column(Integer, server_default=text("0"))
    RechargeRate = Column(Float, server_default=text("0"))
    ElectronicSystem = Column(Boolean, server_default=text("NULL"))
    ElectronicCTD = Column(Integer, server_default=text("100"))
    TrackingSpeed = Column(Integer, server_default=text("0"))
    SpecialFunction = Column(Integer, server_default=text("NULL"))
    MaxSensorRange = Column(Float, server_default=text("0"))
    Resolution = Column(Float, server_default=text("0"))
    HTK = Column(Integer, server_default=text("1"))
    FuelUse = Column(Float, server_default=text("0"))
    NoMaintFailure = Column(Boolean, server_default=text("0"))
    HangarReloadOnly = Column(Boolean, server_default=text("NULL"))
    ExplosionChance = Column(Float, server_default=text("0"))
    MaxExplosionSize = Column(Integer, server_default=text("0"))
    DamageOutput = Column(Integer, server_default=text("0"))
    NumberOfShots = Column(Integer, server_default=text("1"))
    RangeModifier = Column(Float, server_default=text("0"))
    MaxWeaponRange = Column(Integer, server_default=text("0"))
    SpinalWeapon = Column(Boolean, server_default=text("0"))
    JumpDistance = Column(Integer, server_default=text("0"))
    JumpRating = Column(Integer, server_default=text("0"))
    RateOfFire = Column(Integer, server_default=text("0"))
    MaxPercentage = Column(Integer, server_default=text("0"))
    FuelEfficiency = Column(Float, server_default=text("0"))
    IgnoreShields = Column(Boolean, server_default=text("0"))
    IgnoreArmour = Column(Boolean, server_default=text("0"))
    ElectronicOnly = Column(Boolean, server_default=text("NULL"))
    StealthRating = Column(Float, server_default=text("0"))
    CloakRating = Column(Float, server_default=text("0"))
    Weapon = Column(Boolean, server_default=text("NULL"))
    BGTech1 = Column(Integer, server_default=text("0"))
    BGTech2 = Column(Integer, server_default=text("0"))
    BGTech3 = Column(Integer, server_default=text("0"))
    BGTech4 = Column(Integer, server_default=text("0"))
    BGTech5 = Column(Integer, server_default=text("0"))
    BGTech6 = Column(Integer, server_default=text("0"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))
    SingleSystemOnly = Column(Integer)
    ShipyardRepairOnly = Column(Boolean, server_default=text("False"))
    ECCM = Column(Integer, server_default=text("0"))
    ArmourRetardation = Column(Float, server_default=text("0"))
    WeaponToHitModifier = Column(Float, server_default=text("1"))
    Prototype = Column(Integer, server_default=text("0"))
    TurretWeaponID = Column(Integer, server_default=text("0"))


t_FCT_ShipHistory = Table(
    'FCT_ShipHistory', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('Description', Text, server_default=text("NULL")),
    Column('GameTime', Float, server_default=text("NULL"))
)

t_FCT_ShipMeasurement = Table(
    'FCT_ShipMeasurement', metadata,
    Column('ShipID', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0")),
    Column('MeasurementType', Integer, server_default=text("0")),
    Column('Amount', Float, server_default=text("0")),
    Column('StrikeGroup', Boolean, server_default=text("FALSE"))
)

t_FCT_ShipOrdnanceTemplate = Table(
    'FCT_ShipOrdnanceTemplate', metadata,
    Column('ShipID', Integer, server_default=text("0")),
    Column('MissileID', Integer, server_default=text("0")),
    Column('Amount', Integer, server_default=text("0")),
    Column('GameID', Integer, server_default=text("0"))
)


class FCTShipTechDatum(Base):
    __tablename__ = 'FCT_ShipTechData'

    ShipID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    TechID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    TechPoints = Column(Float, server_default=text("NULL"))


t_FCT_ShipWeapon = Table(
    'FCT_ShipWeapon', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('MissileID', Integer, server_default=text("NULL")),
    Column('Amount', Integer, server_default=text("NULL"))
)


class FCTShippingLine(Base):
    __tablename__ = 'FCT_ShippingLines'

    ShippingLineID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    EmpireID = Column(Integer, server_default=text("NULL"))
    NPRace = Column(Boolean, server_default=text("NULL"))
    LineName = Column(Text, server_default=text("NULL"))
    ShortName = Column(Text, server_default=text("NULL"))
    ShipNum = Column(Integer, server_default=text("1"))
    WealthBalance = Column(Float, server_default=text("2000"))
    LastDividendPaid = Column(Float, server_default=text("0"))
    LastDividendTime = Column(Float, server_default=text("NULL"))
    MaxAssets = Column(Float, server_default=text("1000"))
    CommEngineID = Column(Integer, server_default=text("0"))
    CommercialEngines = Column(Integer, server_default=text("0"))


t_FCT_ShippingWealthData = Table(
    'FCT_ShippingWealthData', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('ShippingLineID', Integer, server_default=text("NULL")),
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('Contract', Boolean, server_default=text("NULL")),
    Column('Colonist', Boolean, server_default=text("0")),
    Column('Fuel', Boolean, server_default=text("0")),
    Column('Amount', Float, server_default=text("NULL")),
    Column('TradeGood', Integer, server_default=text("NULL")),
    Column('TradeTime', Float, server_default=text("NULL")),
    Column('OriginPop', Integer, server_default=text("0")),
    Column('DestinationPop', Integer, server_default=text("NULL"))
)


class FCTShipyard(Base):
    __tablename__ = 'FCT_Shipyard'

    ShipyardID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    TractorParentShipID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("NULL"))
    SYType = Column(Integer, server_default=text("1"))
    ShipyardName = Column(Text, server_default=text("NULL"))
    Slipways = Column(Float, server_default=text("1"))
    Capacity = Column(Float, server_default=text("1000"))
    BuildClassID = Column(Integer, server_default=text("0"))
    RetoolClassID = Column(Integer, server_default=text("0"))
    TaskType = Column(Integer, server_default=text("0"))
    RequiredBP = Column(Float, server_default=text("0"))
    CompletedBP = Column(Float, server_default=text("0"))
    PauseActivity = Column(Boolean, server_default=text("0"))
    DefaultFleetID = Column(Integer, server_default=text("0"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("NULL"))
    CapacityTarget = Column(Integer, server_default=text("0"))


class FCTShipyardTask(Base):
    __tablename__ = 'FCT_ShipyardTask'

    TaskID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))
    PopulationID = Column(Integer, server_default=text("0"))
    ShipyardID = Column(Integer, server_default=text("NULL"))
    TaskTypeID = Column(Integer, server_default=text("0"))
    Freighter = Column(Boolean, server_default=text("NULL"))
    FleetID = Column(Integer, server_default=text("0"))
    ShipID = Column(Integer, server_default=text("0"))
    ClassID = Column(Integer, server_default=text("0"))
    NPRShip = Column(Boolean, server_default=text("0"))
    RefitID = Column(Integer, server_default=text("0"))
    TotalBP = Column(Float, server_default=text("0"))
    CompletedBP = Column(Float, server_default=text("0"))
    Paused = Column(Boolean, server_default=text("NULL"))
    UnitName = Column(Text, server_default=text("NULL"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))


class FCTSpecies(Base):
    __tablename__ = 'FCT_Species'

    SpeciesID = Column(Integer, primary_key=True)
    GameID = Column(Integer, index=True, server_default=text("NULL"))
    TechSystemID = Column(Integer, server_default=text("0"))
    HomeworldID = Column(Integer, server_default=text("NULL"))
    DerivedSpeciesID = Column(Integer, server_default=text("0"))
    SpeciesName = Column(Text, server_default=text("NULL"))
    RacePic = Column(Text, server_default=text("NULL"))
    BreatheID = Column(Integer, server_default=text("10"))
    Oxygen = Column(Float, server_default=text("0.2"))
    OxyDev = Column(Float, server_default=text("0.5"))
    PressMax = Column(Float, server_default=text("NULL"))
    Temperature = Column(Float, server_default=text("15"))
    TempDev = Column(Float, server_default=text("20"))
    Gravity = Column(Float, server_default=text("1"))
    GravDev = Column(Float, server_default=text("0.5"))
    Xenophobia = Column(Integer, server_default=text("NULL"))
    Diplomacy = Column(Integer, server_default=text("NULL"))
    Translation = Column(Integer, server_default=text("NULL"))
    Militancy = Column(Integer, server_default=text("NULL"))
    Expansionism = Column(Integer, server_default=text("NULL"))
    Determination = Column(Integer, server_default=text("NULL"))
    Trade = Column(Integer, server_default=text("NULL"))
    SpecialNPRID = Column(Integer, server_default=text("0"))
    ProductionRateModifier = Column(Float, server_default=text("1"))
    ResearchRateModifier = Column(Float, server_default=text("1"))
    PopulationGrowthModifier = Column(Float, server_default=text("1"))
    PopulationDensityModifier = Column(Float, server_default=text("1"))


class FCTStar(Base):
    __tablename__ = 'FCT_Star'

    StarID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    SystemID = Column(Integer, server_default=text("0"))
    StarTypeID = Column(Integer, server_default=text("NULL"))
    Name = Column(Text, server_default=text("NULL"))
    Protostar = Column(Integer, server_default=text("0"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("0"))
    Component = Column(Integer, server_default=text("1"))
    OrbitingComponent = Column(Integer, server_default=text("0"))
    Bearing = Column(Float, server_default=text("0"))
    Luminosity = Column(Float, server_default=text("0"))
    OrbitalPeriod = Column(Float, server_default=text("0"))
    OrbitalDistance = Column(Float, server_default=text("0"))
    DisasterStatus = Column(Integer, server_default=text("0"))


class FCTSubFleet(Base):
    __tablename__ = 'FCT_SubFleets'

    SubFleetID = Column(Integer, primary_key=True, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    RaceID = Column(Integer, server_default=text("0"))
    ParentFleetID = Column(Integer, server_default=text("0"))
    ParentSubFleetID = Column(Integer, server_default=text("0"))
    SubFleetName = Column(Text, server_default=text("NULL"))
    AnchorFleetID = Column(Integer, server_default=text("0"))
    SpecificThreatID = Column(Integer, server_default=text("0"))
    AnchorFleetDistance = Column(Float, server_default=text("0"))
    AnchorFleetBearingOffset = Column(Integer, server_default=text("0"))
    GuardNearestHostileContact = Column(Boolean, server_default=text("FALSE"))
    GuardNearestKnownWarship = Column(Boolean, server_default=text("FALSE"))
    UseAnchorDestination = Column(Boolean, server_default=text("FALSE"))


class FCTSurveyLocation(Base):
    __tablename__ = 'FCT_SurveyLocation'

    SurveyLocationID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("0"))
    SystemID = Column(Integer, server_default=text("NULL"))
    LocationNumber = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("0"))


t_FCT_Survivors = Table(
    'FCT_Survivors', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('SpeciesID', Integer, server_default=text("NULL")),
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('SurvivorsShipName', Text, server_default=text("NULL")),
    Column('Crew', Integer, server_default=text("0")),
    Column('Wounded', Integer, server_default=text("0")),
    Column('RescueTime', Float, server_default=text("NULL")),
    Column('RescueSystemID', Integer, server_default=text("NULL")),
    Column('GradePoints', Float, server_default=text("NULL"))
)

t_FCT_SwarmResearch = Table(
    'FCT_SwarmResearch', metadata,
    Column('RaceID', Integer),
    Column('GameID', Integer),
    Column('ResearchPoints', Integer),
    Column('TechSystemID', Integer)
)


class FCTSystem(Base):
    __tablename__ = 'FCT_System'

    SystemID = Column(Integer, primary_key=True)
    SystemNumber = Column(Integer, nullable=False, server_default=text("0"))
    Age = Column(Float, server_default=text("NULL"))
    AbundanceModifier = Column(Integer, server_default=text("NULL"))
    Stars = Column(Integer, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))
    JumpPointSurveyPoints = Column(Integer, server_default=text("0"))
    SystemTypeID = Column(Integer, server_default=text("0"))
    DustDensity = Column(Integer, server_default=text("0"))
    SolSystem = Column(Numeric, server_default=text("NULL"))
    NoSensorChecks = Column(Integer, server_default=text("0"))


class FCTSystemBody(Base):
    __tablename__ = 'FCT_SystemBody'

    SystemBodyID = Column(Integer, primary_key=True)
    SystemID = Column(Integer, server_default=text("0"))
    StarID = Column(Integer, server_default=text("0"))
    GameID = Column(Integer, server_default=text("0"))
    Name = Column(Text, server_default=text("' '"))
    PlanetNumber = Column(Integer, server_default=text("0"))
    OrbitNumber = Column(Integer, server_default=text("0"))
    TrojanAsteroid = Column(Float, server_default=text("0"))
    OrbitalDistance = Column(Float, server_default=text("0"))
    CurrentDistance = Column(Float, server_default=text("0"))
    HeadingInward = Column(Boolean, server_default=text("NULL"))
    Bearing = Column(Float, server_default=text("0"))
    BodyClass = Column(Integer, server_default=text("0"))
    Density = Column(Float, server_default=text("0"))
    Radius = Column(Integer, server_default=text("0"))
    Gravity = Column(Float, server_default=text("0"))
    ParentBodyID = Column(Integer, server_default=text("0"))
    ParentBodyType = Column(Integer, server_default=text("0"))
    BodyTypeID = Column(Integer, server_default=text("0"))
    Mass = Column(Float, server_default=text("0"))
    EscapeVelocity = Column(Float, server_default=text("0"))
    Year = Column(Float, server_default=text("0"))
    TidalForce = Column(Float, server_default=text("0"))
    TidalLock = Column(Boolean, server_default=text("0"))
    Tilt = Column(Integer, server_default=text("0"))
    Eccentricity = Column(Float, server_default=text("0"))
    DayValue = Column(Float, server_default=text("0"))
    Roche = Column(Float, server_default=text("0"))
    TectonicActivity = Column(Integer, server_default=text("1"))
    Ring = Column(Boolean, server_default=text("0"))
    MagneticField = Column(Float, server_default=text("0"))
    BaseTemp = Column(Float, server_default=text("0"))
    SurfaceTemp = Column(Float, server_default=text("0"))
    HydroID = Column(Integer, server_default=text("1"))
    HydroExt = Column(Float, server_default=text("0"))
    AtmosPress = Column(Float, server_default=text("0"))
    Albedo = Column(Float, server_default=text("1"))
    GHFactor = Column(Float, server_default=text("1"))
    RGE = Column(Boolean, server_default=text("0"))
    Xcor = Column(Float, server_default=text("0"))
    Ycor = Column(Float, server_default=text("NULL"))
    PlanetIcon = Column(Text, server_default=text("NULL"))
    RuinID = Column(Integer, server_default=text("0"))
    RuinRaceID = Column(Integer, server_default=text("0"))
    RadiationLevel = Column(Float, server_default=text("0"))
    DustLevel = Column(Float, server_default=text("0"))
    AbandonedFactories = Column(Integer, server_default=text("0"))
    DominantTerrain = Column(Integer, server_default=text("0"))
    GroundMineralSurvey = Column(Integer, server_default=text("0"))
    AsteroidBeltID = Column(Integer, server_default=text("0"))
    AGHFactor = Column(Float, server_default=text("1"))


class FCTSystemBodyName(Base):
    __tablename__ = 'FCT_SystemBodyName'

    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    SystemBodyID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    Name = Column(String(50), server_default=text("NULL"))


t_FCT_SystemBodySurveys = Table(
    'FCT_SystemBodySurveys', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('SystemBodyID', Integer, server_default=text("NULL"))
)

t_FCT_TechProgressionRace = Table(
    'FCT_TechProgressionRace', metadata,
    Column('ProgressionOrder', Integer),
    Column('RaceID', Integer),
    Column('GameID', Integer)
)


class FCTTechSystem(Base):
    __tablename__ = 'FCT_TechSystem'

    TechSystemID = Column(Integer, primary_key=True)
    Name = Column(Text, server_default=text("NULL"))
    ComponentName = Column(Text, server_default=text("NULL"))
    CategoryID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("0"))
    TechTypeID = Column(Integer, server_default=text("0"))
    NoTechScan = Column(Boolean, server_default=text("0"))
    RuinOnly = Column(Boolean, server_default=text("0"))
    Prerequisite1 = Column(Integer, server_default=text("0"))
    Prerequisite2 = Column(Integer, server_default=text("0"))
    StartingSystem = Column(Boolean, server_default=text("NULL"))
    ConventionalSystem = Column(Boolean, server_default=text("NULL"))
    DevelopCost = Column(Integer, server_default=text("0"))
    AdditionalInfo = Column(Float, server_default=text("0"))
    AdditionalInfo2 = Column(Float, server_default=text("0"))
    AdditionalInfo3 = Column(Float, server_default=text("0"))
    AdditionalInfo4 = Column(Float, server_default=text("0"))
    TechDescription = Column(Text, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("0"))
    AutomaticResearch = Column(Boolean, server_default=text("FALSE"))


class FCTWaypoint(Base):
    __tablename__ = 'FCT_Waypoint'

    WaypointID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    OrbitBodyID = Column(Integer, server_default=text("0"))
    CreationTime = Column(Float, server_default=text("0"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    Number = Column(Integer, server_default=text("0"))
    WaypointType = Column(Integer, server_default=text("0"))
    Name = Column(Text)
    JumpPointID = Column(Integer, server_default=text("0"))


t_FCT_WealthData = Table(
    'FCT_WealthData', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('RaceID', Integer, server_default=text("NULL")),
    Column('Amount', Float, server_default=text("NULL")),
    Column('UseID', Integer, server_default=text("NULL")),
    Column('TimeUsed', Float, server_default=text("NULL"))
)

t_FCT_WeaponAssignment = Table(
    'FCT_WeaponAssignment', metadata,
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('WeaponID', Integer, server_default=text("NULL")),
    Column('WeaponNum', Integer, server_default=text("NULL")),
    Column('FCTypeID', Integer, server_default=text("0")),
    Column('FCNum', Integer, server_default=text("NULL")),
    Column('GameID', Integer, server_default=text("0"))
)

t_FCT_WeaponRecharge = Table(
    'FCT_WeaponRecharge', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('ShipID', Integer, server_default=text("NULL")),
    Column('WeaponID', Integer, server_default=text("NULL")),
    Column('WeaponNumber', Integer, server_default=text("1")),
    Column('RechargeRemaining', Float, server_default=text("0"))
)

t_FCT_WindowPosition = Table(
    'FCT_WindowPosition', metadata,
    Column('GameID', Integer, server_default=text("0")),
    Column('WindowName', Text, server_default=text("NULL")),
    Column('Left', Integer, server_default=text("0")),
    Column('Top', Integer, server_default=text("0"))
)


class FCTWreckComponent(Base):
    __tablename__ = 'FCT_WreckComponents'

    WreckID = Column(Integer, primary_key=True, nullable=False)
    ComponentID = Column(Integer, primary_key=True, nullable=False)
    GameID = Column(Integer, server_default=text("0"))
    Amount = Column(Integer, server_default=text("NULL"))


t_FCT_WreckTech = Table(
    'FCT_WreckTech', metadata,
    Column('GameID', Integer, server_default=text("NULL")),
    Column('WreckID', Integer, server_default=text("NULL")),
    Column('TechID', Integer, server_default=text("NULL")),
    Column('Percentage', Integer, server_default=text("NULL"))
)


class FCTWreck(Base):
    __tablename__ = 'FCT_Wrecks'

    WreckID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    RaceID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    OrbitBodyID = Column(Integer, server_default=text("0"))
    ClassID = Column(Integer, server_default=text("NULL"))
    Size = Column(Integer, server_default=text("0"))
    EffectiveSize = Column(Integer, server_default=text("0"))
    StarSwarmHatching = Column(Integer, server_default=text("0"))
    QueenStatus = Column(Integer, server_default=text("0"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))
    Duranium = Column(Float, server_default=text("0"))
    Neutronium = Column(Float, server_default=text("0"))
    Corbomite = Column(Float, server_default=text("0"))
    Tritanium = Column(Float, server_default=text("0"))
    Boronide = Column(Float, server_default=text("0"))
    Mercassium = Column(Float, server_default=text("0"))
    Vendarite = Column(Float, server_default=text("0"))
    Sorium = Column(Float, server_default=text("0"))
    Uridium = Column(Float, server_default=text("0"))
    Corundium = Column(Float, server_default=text("0"))
    Gallicite = Column(Float, server_default=text("0"))


class FGNickname(Base):
    __tablename__ = 'FGNicknames'

    NicknameID = Column(Integer, primary_key=True)
    Nickname = Column(String(22), server_default=text("NULL"))


class ForeignAid(Base):
    __tablename__ = 'ForeignAid'

    RaceID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    AlienID = Column(Integer, primary_key=True, nullable=False, server_default=text("NULL"))
    GameID = Column(Integer, server_default=text("NULL"))
    Amount = Column(Integer, server_default=text("NULL"))


class Formation(Base):
    __tablename__ = 'Formation'

    FormationID = Column(Integer, primary_key=True)
    EscortFleetID = Column(Integer, server_default=text("NULL"))
    ShipID = Column(Integer, server_default=text("NULL"))
    ProtectedFleetID = Column(Integer, server_default=text("NULL"))
    Distance = Column(Integer, server_default=text("NULL"))
    OffsetBearing = Column(Integer, server_default=text("NULL"))
    FleetName = Column(String(50), server_default=text("NULL"))


class GovType(Base):
    __tablename__ = 'GovType'

    GovTypeID = Column(Integer, primary_key=True)
    GovName = Column(String(30), server_default=text("NULL"))
    MissileBases = Column(Float, server_default=text("NULL"))
    TotalIndustrial = Column(Integer, server_default=text("NULL"))
    ResearchRate = Column(Float, server_default=text("1"))
    TrainingLevel = Column(Integer, server_default=text("1"))
    SYPerAcademy = Column(Integer, server_default=text("1"))
    MaxChance = Column(SmallInteger, server_default=text("NULL"))
    GrowthMod = Column(Float, server_default=text("1"))
    ConstMod = Column(Float, server_default=text("1"))
    Shipyards = Column(Integer, server_default=text("0"))
    Labs = Column(Integer, server_default=text("0"))
    Construction = Column(Integer, server_default=text("0"))
    Refineries = Column(Float, server_default=text("0"))
    Mines = Column(Integer, server_default=text("NULL"))
    Sensors = Column(Integer, server_default=text("NULL"))
    XenophobiaMod = Column(Integer, server_default=text("0"))
    DiplomacyMod = Column(Integer, server_default=text("0"))
    MilitancyMod = Column(Integer, server_default=text("0"))
    ExpansionismMod = Column(Integer, server_default=text("0"))
    DeterminationMod = Column(Integer, server_default=text("NULL"))
    TradeMod = Column(Integer, server_default=text("0"))
    UserDefined = Column(Boolean, server_default=text("NULL"))
    HA = Column(Integer, server_default=text("0"))
    MI = Column(Integer, server_default=text("0"))
    AI = Column(Integer, server_default=text("0"))
    GAR = Column(Integer, server_default=text("0"))


class MissileSery(Base):
    __tablename__ = 'MissileSeries'

    SeriesID = Column(Integer, primary_key=True)
    RaceID = Column(Integer, server_default=text("NULL"))
    SeriesName = Column(String(50), server_default=text("NULL"))


class PhoneticName(Base):
    __tablename__ = 'PhoneticNames'

    PhoneticID = Column(Integer, primary_key=True)
    PhoneticName = Column(String(50), server_default=text("NULL"))


class RaceCompare(Base):
    __tablename__ = 'RaceCompare'

    RaceID = Column(Integer, primary_key=True, server_default=text("NULL"))
    RaceName = Column(String(255), server_default=text("NULL"))
    Pop = Column(Float, server_default=text("NULL"))
    NavalSY = Column(Integer, server_default=text("NULL"))
    CommercialSY = Column(Integer, server_default=text("NULL"))
    Research = Column(Integer, server_default=text("NULL"))
    Factories = Column(Integer, server_default=text("NULL"))
    Mines = Column(Integer, server_default=text("NULL"))
    AM = Column(Integer, server_default=text("NULL"))
    Refineries = Column(Integer, server_default=text("NULL"))
    Naval = Column(Integer, server_default=text("NULL"))
    Commercial = Column(Integer, server_default=text("NULL"))
    Civilian = Column(Integer, server_default=text("NULL"))
    DSTS = Column(Integer, server_default=text("NULL"))
    Academies = Column(Integer, server_default=text("NULL"))
    Fuel = Column(Integer, server_default=text("NULL"))


class Wormhole(Base):
    __tablename__ = 'Wormholes'

    WormholeID = Column(Integer, primary_key=True)
    GameID = Column(Integer, server_default=text("NULL"))
    SystemID = Column(Integer, server_default=text("NULL"))
    Xcor = Column(Float, server_default=text("NULL"))
    Ycor = Column(Float, server_default=text("NULL"))


t_sqlite_sequence = Table(
    'sqlite_sequence', metadata,
    Column('name', NullType),
    Column('seq', NullType)
)

t_sqlite_stat1 = Table(
    'sqlite_stat1', metadata,
    Column('tbl', NullType),
    Column('idx', NullType),
    Column('stat', NullType)
)

t_sqlite_stat4 = Table(
    'sqlite_stat4', metadata,
    Column('tbl', NullType),
    Column('idx', NullType),
    Column('neq', NullType),
    Column('nlt', NullType),
    Column('ndlt', NullType),
    Column('sample', NullType)
)
