import warnings

from sqlalchemy import create_engine
from sqlalchemy.exc import SAWarning
from sqlalchemy.orm import sessionmaker

import config
from app.database.models import FCTGame

engine = None

Session = None

warnings.filterwarnings('ignore', r".*support Decimal objects natively", SAWarning, r'^sqlalchemy\.sql\.sqltypes$')


def connect():
    global engine, Session

    engine = create_engine(f'sqlite:///{config.DB_PATH}', echo=False)

    Session = sessionmaker(bind=engine)

    test_connection()


def get_session():
    return Session()


def test_connection():
    session = get_session()

    game = session.query(FCTGame.GameID).all()
