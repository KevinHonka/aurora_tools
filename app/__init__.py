import sys

from PySide6 import QtWidgets

from config import init_config
from app.ui.main.mainwindow import MainWindow

init_config()

app = QtWidgets.QApplication([])

main_window = MainWindow()

sys.exit(app.exec_())
