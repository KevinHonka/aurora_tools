from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QGridLayout, QPushButton, QLabel, QComboBox, QGroupBox, QVBoxLayout, QLineEdit, \
    QHBoxLayout, QRadioButton, QDialog
from qt_material import QtStyleTools

from app.ui.controller import update_weapons, update_turret_tracking, update_armor, update_turret_info_panel, \
    add_turret_from_design


class TurretWidget(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.mainLayout = QGridLayout(self)
        self.weaponTypeLabel = QLabel("Weapon type:", self)
        self.weaponLabel = QLabel("Weapon:", self)
        self.weaponTypeBox = QComboBox(self)
        self.weaponBox = QComboBox(self)
        self.armorBox = QComboBox(self)
        self.armorLabel = QLabel("Armor:", self)
        self.trackingSpeedTechLabel = QLabel("", self)
        self.fireControlSpeedLabel = QLabel("", self)
        self.speedGroupBox = QGroupBox(self)
        self.speedLayout = QVBoxLayout(self)
        self.trackingSpeedLabel = QLabel("Desired Tracking Speed", self)
        self.trackingSpeedLine = QLineEdit("1000", self)
        self.armorAmountLabel = QLabel("Turret Armor Strength", self)
        self.armorAmountLine = QLineEdit("0", self)
        self.turretButtonsLayout = QHBoxLayout(self)
        self.turretGroupBox = QGroupBox(self)
        self.turretSingleButton = QRadioButton("Single", self)
        self.turretDoubleButton = QRadioButton("Double", self)
        self.turretTripleButton = QRadioButton("Triple", self)
        self.turretQuadButton = QRadioButton("Quad", self)
        self.turretInfoLabelRight1 = QLabel("", self)
        self.turretInfoLabelLeft1 = QLabel("", self)
        self.turretInfoLabelRight2 = QLabel("", self)
        self.turretInfoLabelLeft2 = QLabel("", self)
        self.turretInfoBox = QGroupBox(self)
        self.turretInfoLayout = QHBoxLayout(self)
        self.nameLine = QLineEdit("", self)
        self.addProjectButton = QPushButton("Create", self)
        self.instantButton = QPushButton("Instant", self)
        self.buttonBoxLayout = QHBoxLayout(self)
        self.buttonBox = QGroupBox(self)
        self.nameLabel = QLabel("Name:", self)

        self.mainLayout.addWidget(self.weaponTypeLabel, 3, 0)
        self.mainLayout.addWidget(self.weaponTypeBox, 3, 1)
        self.mainLayout.addWidget(self.weaponLabel, 4, 0, 1, 2)
        self.mainLayout.addWidget(self.weaponBox, 5, 0, 1, 2)
        self.mainLayout.addWidget(self.armorLabel, 6, 0)
        self.mainLayout.addWidget(self.armorBox, 6, 1)
        self.mainLayout.addWidget(self.speedGroupBox, 7, 0, 1, 2)
        self.mainLayout.addWidget(self.trackingSpeedLabel, 8, 0)
        self.mainLayout.addWidget(self.trackingSpeedLine, 8, 1)
        self.mainLayout.addWidget(self.armorAmountLabel, 9, 0)
        self.mainLayout.addWidget(self.armorAmountLine, 9, 1)
        self.mainLayout.addWidget(self.turretGroupBox, 10, 0, 1, 2)
        self.mainLayout.addWidget(self.turretInfoBox, 11, 0, 1, 2)
        self.mainLayout.addWidget(self.nameLabel, 12, 0, 1, 2)
        self.mainLayout.addWidget(self.nameLine, 13, 0, 1, 2)
        self.mainLayout.addWidget(self.buttonBox, 14, 0, 1, 2)

        self.speedLayout.addWidget(self.trackingSpeedTechLabel)
        self.speedLayout.addWidget(self.fireControlSpeedLabel)
        self.speedGroupBox.setLayout(self.speedLayout)

        self.turretInfoLayout.addWidget(self.turretInfoLabelLeft1)
        self.turretInfoLayout.addWidget(self.turretInfoLabelLeft2)
        self.turretInfoLayout.addWidget(self.turretInfoLabelRight1)
        self.turretInfoLayout.addWidget(self.turretInfoLabelRight2)
        self.turretInfoBox.setLayout(self.turretInfoLayout)

        self.turretInfoLabelLeft1.setTextInteractionFlags(Qt.TextSelectableByMouse)
        self.turretInfoLabelLeft2.setTextInteractionFlags(Qt.TextSelectableByMouse)
        self.turretInfoLabelRight1.setTextInteractionFlags(Qt.TextSelectableByMouse)
        self.turretInfoLabelRight2.setTextInteractionFlags(Qt.TextSelectableByMouse)

        self.turretButtonsLayout.addWidget(self.turretSingleButton)
        self.turretButtonsLayout.addWidget(self.turretDoubleButton)
        self.turretButtonsLayout.addWidget(self.turretTripleButton)
        self.turretButtonsLayout.addWidget(self.turretQuadButton)
        self.turretGroupBox.setLayout(self.turretButtonsLayout)

        self.buttonBoxLayout.addWidget(self.addProjectButton)
        self.buttonBoxLayout.addWidget(self.instantButton)
        self.buttonBox.setLayout(self.buttonBoxLayout)

        self.turretSingleButton.setChecked(True)

        self.weaponTypeBox.addItems(["Laser", "Particle Beam", "Railgun", "Gauss Cannon"])

        self.weaponTypeBox.currentTextChanged.connect(
            lambda x: update_weapons(self.weaponBox, self.weaponTypeBox.currentText()))
        self.weaponBox.currentTextChanged.connect(
            lambda x: self.update_turret())

        self.turretSingleButton.clicked.connect(lambda x: self.update_turret())
        self.turretDoubleButton.clicked.connect(lambda x: self.update_turret())
        self.turretTripleButton.clicked.connect(lambda x: self.update_turret())
        self.turretQuadButton.clicked.connect(lambda x: self.update_turret())

        self.trackingSpeedLine.textChanged.connect(lambda x: self.update_turret())
        self.armorAmountLine.textChanged.connect(lambda x: self.update_turret())
        self.armorBox.currentTextChanged.connect(lambda x: self.update_turret())

        self.instantButton.clicked.connect(lambda x: self.create_turret(instant=True))
        self.addProjectButton.clicked.connect(lambda x: self.create_turret())

        self.setLayout(self.mainLayout)

    def create_turret(self, instant=False):

        number_weapons = 0

        if self.turretSingleButton.isChecked():
            number_weapons = 1
        elif self.turretDoubleButton.isChecked():
            number_weapons = 2
        elif self.turretTripleButton.isChecked():
            number_weapons = 3
        elif self.turretQuadButton.isChecked():
            number_weapons = 4

        turret = add_turret_from_design(self.nameLine.text(),
                                        self.weaponBox.currentText(),
                                        self.trackingSpeedLine.text(),
                                        number_weapons,
                                        self.armorAmountLine.text(),
                                        self.armorBox.currentText(),
                                        instant)

        if turret:
            dialog = TurretDialog()


    def update_turret(self):

        number_weapons = 0

        if self.turretSingleButton.isChecked():
            number_weapons = 1
        elif self.turretDoubleButton.isChecked():
            number_weapons = 2
        elif self.turretTripleButton.isChecked():
            number_weapons = 3
        elif self.turretQuadButton.isChecked():
            number_weapons = 4

        update_turret_info_panel(self.turretInfoLabelLeft1,
                                 self.turretInfoLabelLeft2,
                                 self.turretInfoLabelRight1,
                                 self.turretInfoLabelRight2,
                                 self.weaponBox.currentText(),
                                 self.trackingSpeedLine.text(),
                                 number_weapons,
                                 self.armorAmountLine.text(),
                                 self.armorBox.currentText())

    def update_all(self):
        update_weapons(self.weaponBox, self.weaponTypeBox.currentText())
        update_turret_tracking(self.trackingSpeedTechLabel, self.fireControlSpeedLabel)
        update_armor(self.armorBox)


class TurretDialog(QDialog, QtStyleTools):
    def __init__(self):
        super().__init__()

        dialog_layout = QVBoxLayout()
        dialog_text = QLabel("Turret created successfully")
        dialog_button = QPushButton("Ok")
        dialog_button.clicked.connect(self.close)

        self.setWindowTitle("Turret created")
        self.apply_stylesheet(self, theme='light_blue.xml')

        dialog_layout.addWidget(dialog_text)
        dialog_layout.addWidget(dialog_button)

        self.setLayout(dialog_layout)

        self.setWindowTitle("Turret created")
        self.setWindowModality(Qt.ApplicationModal)
        self.exec_()
