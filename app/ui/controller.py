import sys
from os import walk
from sqlite3 import OperationalError

import jinja2
from PySide6.QtWidgets import QComboBox, QLabel, QLineEdit
from sqlalchemy.exc import DatabaseError

import config
from app.database import connect, get_session
from app.tools import init_tools
from app.tools.commander import get_commanders
from app.tools.component import get_components, get_weapons
from app.tools.config import get_config
from app.tools.event import get_eventlog, get_types
from app.tools.exporter import export_empire, export_commanders, export_shipclasses, export_components, export_events
from app.tools.fleet import get_fleets
from app.tools.game import get_list as get_game_list, get_game
from app.tools.race import get_list as get_race_list
from app.tools.ship import get_shipclass
from app.tools.tech import get_tech, get_armor
from app.tools.turret import get_turret, calculate_rate_of_fire, add_turret
from app.ui.utils.widgets import Form


def get_template(template_name="empire"):
    template_loader = jinja2.FileSystemLoader(config.TEMPLATE_DIR)
    template_env = jinja2.Environment(loader=template_loader)

    return template_env.get_template(f"{template_name}.j2")


def get_templates(template_name):
    template_list = []

    for (dirpath, dirnames, filenames) in walk(config.EXPORT_TEMPLATE_DIR):
        if template_name:
            for filename in filenames:
                if template_name in filename:
                    template_list.append(filename.split(".")[0])
        else:
            template_list = filenames
        break

    return template_list


def do_export(export_dir, template, search_filter=None):
    if "empire" in template.lower():
        export_empire(export_dir=export_dir, template_name=template.lower())
    elif "commanders" in template.lower():
        export_commanders(export_dir=export_dir, template_name=template.lower(), search_filter=search_filter)
    elif "events" in template.lower():
        export_events(export_dir=export_dir, template_name=template.lower(), search_filter=search_filter)
    elif "shipclass" in template.lower():
        export_shipclasses(export_dir=export_dir, template_name=template.lower(), search_filter=search_filter)
    elif "components" in template.lower():
        export_components(export_dir=export_dir, template_name=template.lower(), search_filter=search_filter)


def get_game_date(line: QLineEdit, game_name):
    game = get_game(game_name)

    line.setText(str(game.StartYear))


def get_ranks(box: QComboBox):
    config = get_config()

    box.clear()
    box.addItem('')

    for rank_type, ranks in config.ranks.items():
        for rank in ranks:
            box.addItem(f"{rank_type} - {rank.RankName}")


def get_species(box: QComboBox):
    config = get_config()

    box.clear()
    box.addItem('')

    for species in config.species_list:
        box.addItem(species.SpeciesName)


def get_races(box: QComboBox, game_name):
    box.clear()
    if game_name:
        for race in get_race_list(game_name):
            box.addItem(race.RaceTitle)


def get_games(box: QComboBox):
    box.clear()

    for game in get_game_list():
        box.addItem(game.GameName)


def get_event_types(box: QComboBox):
    box.clear()

    box.addItem('')
    for event_type in get_types():
        box.addItem(event_type.Description)


def get_ship_types(box: QComboBox):
    box.clear()

    box.addItem('')
    for ship_type in get_shipclass():
        box.addItem(ship_type.ClassName)


def get_component_list(box: QComboBox):
    box.clear()

    box.addItem('')
    for component in get_components():
        box.addItem(component.name)


def update_commander_list(widget, search_filter, progressbar):
    progressbar.setLabelText("Loading Commanders from Database")
    progressbar.repaint()

    items = get_commanders(search_filter)

    template = get_template("commander")

    progressbar.setMaximum(len(items))

    widget.clear_layout()

    for item in items:
        output = template.render(commander=item)

        widget.scroll_layout.addWidget(Form(output))
        progressbar.setLabelText(f"Adding Commander: {item.name}")
        progressbar.setValue(progressbar.value() + 1)
        widget.scroll_layout.addSpacing(10)
    widget.update()


def update_event_list(widget, search_filter, progressbar):
    progressbar.setLabelText("Loading Events from Database")
    progressbar.repaint()

    items = get_eventlog(search_filter)

    template = get_template("event")

    progressbar.setMaximum(len(items))

    widget.clear_layout()

    for item in items:
        output = template.render(event=item)

        widget.scroll_layout.addWidget(Form(output))
        progressbar.setLabelText(f"Adding Event: {item['event']}")
        progressbar.setValue(progressbar.value() + 1)
        widget.scroll_layout.addSpacing(10)
    widget.update()


def update_shipclass_list(widget, search_filter, progressbar):
    progressbar.setLabelText("Loading Ship Designs from Database")
    progressbar.repaint()

    items = get_shipclass(search_filter)

    template = get_template("shipclass")

    progressbar.setMaximum(len(items))

    widget.clear_layout()

    for item in items:
        output = template.render(shipclass=item)

        widget.scroll_layout.addWidget(Form(output))
        progressbar.setLabelText(f"Adding Ship Design: {item.name}")
        progressbar.setValue(progressbar.value() + 1)
        widget.scroll_layout.addSpacing(10)
    widget.update()


def update_fleet_list(widget, search_filter, progressbar):
    progressbar.setLabelText("Loading Fleets from Database")
    progressbar.repaint()

    items = get_fleets(search_filter)

    template = get_template("fleet")

    progressbar.setMaximum(len(items))

    widget.clear_layout()

    for key, item in items.items():
        output = template.render(fleet_name=key, fleet=item)

        widget.scroll_layout.addWidget(Form(output))
        progressbar.setLabelText(f"Adding Fleet: {item.name}")
        progressbar.setValue(progressbar.value() + 1)
        widget.scroll_layout.addSpacing(10)
    widget.update()


def update_component_list(widget, search_filter, progressbar):
    progressbar.setLabelText("Loading Components from Database")
    progressbar.repaint()

    items = get_components(search_filter)

    template = get_template("component")

    progressbar.setMaximum(len(items))

    widget.clear_layout()

    for item in items:
        output = template.render(component=item)

        widget.scroll_layout.addWidget(Form(output))
        progressbar.setLabelText(f"Adding Component: {item.name}")
        progressbar.setValue(progressbar.value() + 1)
        widget.scroll_layout.addSpacing(10)
    widget.update()


def update_config_db(db_path):
    update_db_connection(db_path)


def update_game_name(game_name):
    config = get_config()
    config.GAME_NAME = game_name


def update_race_name(race_name):
    config = get_config()
    config.RACE_NAME = race_name


def update_start_date(start_date):
    config = get_config()
    config.START_DATE = start_date


def update_config(db_path, game_name, race_name, start_date):
    if db_path:
        update_config_db(db_path)
    if game_name:
        update_game_name(game_name)
    if race_name:
        update_race_name(race_name)

    if start_date:
        update_start_date(start_date)

    init_tools()


def update_db_connection(db_path):
    config.DB_PATH = db_path

    try:
        connect()
    except OperationalError as op_error:
        raise op_error
    except DatabaseError as db_error:
        raise db_error


def update_weapons(widget: QComboBox, weapon_type: str):
    weapons = get_weapons(weapon_type)

    widget.clear()
    for weapon_name, weapon_value in weapons.items():
        widget.addItem(weapon_name)


def update_turret_tracking(tracking: QLabel, fire_control: QLabel):
    tracking.setText(get_tech(75).Name)
    fire_control.setText(get_tech(5).Name)


def update_armor(armor_box: QComboBox):
    armor_box.clear()
    armor_list = get_armor()

    for armor_name, armor_value in armor_list.items():
        armor_box.addItem(armor_name)


def add_turret_from_design(turret_name, selected_weapon, tracking_speed, number_weapons, armor_amount, selected_armor,
                           instant):
    return add_turret(turret_name, selected_weapon, int(tracking_speed), number_weapons, int(armor_amount),
                      selected_armor, instant)


def update_turret_info_panel(turret_info_left1: QLabel, turret_info_left2: QLabel, turret_info_right1: QLabel,
                             turret_info_right2: QLabel,
                             selected_weapon, tracking_speed, number_weapons, armor_amount, selected_armor):
    try:
        session = get_session()
    except Exception as ex:
        return None

    if not selected_armor or not selected_weapon:
        return None

    # TODO: Catch Stupid Shit
    if not tracking_speed or int(tracking_speed) < 0:
        tracking_speed = 0
    if not armor_amount or int(armor_amount) < 0:
        armor_amount = 0

    turret = get_turret(selected_weapon, int(tracking_speed), number_weapons, int(armor_amount), selected_armor)

    total_weapon_cost = turret.weapon.Cost * turret.number_weapons

    infoLeft1 = f"Individual Weapon Size:\n" \
                f"Individual Weapon Cost:\n" \
                f"Total Weapon Size:\n" \
                f"Total Weapon Cost:\n" \
                f"Rotation Gear(%):\n" \
                f"Gear Size:\n" \
                f"Armor Cost:\n" \
                f"Armor Size:"

    infoLeft2 = f"{turret.weapon.Size}\n" \
                f"{turret.weapon.Cost:.2f}\n" \
                f"{turret.weapon.Size * turret.number_weapons}\n" \
                f"{total_weapon_cost:.2f}\n" \
                f"{turret.gear_percent:.2f}\n" \
                f"{turret.gear_size:.2f}\n" \
                f"{turret.armor_cost:.2f}\n" \
                f"{turret.armor_size:.2f}"

    infoRight1 = f"Damage Output:\n" \
                 f"Rate of Fire:\n" \
                 f"Range Modifier:\n" \
                 f"Maximum Range:\n" \
                 f"Turret Size:\n" \
                 f"HTK:\n" \
                 f"Power Requirement:\n" \
                 f"Recharge Rate:\n" \
                 f"Cost:\n" \
                 f"Crew:\n" \
                 f"Development Cost:\n\n" \
                 f"Materials Required:\n" \
                 f"Duranium:\n"

    infoRight2 = f"{turret.weapon.DamageOutput}x{turret.number_weapons}x{turret.weapon.NumberOfShots}\n" \
                 f"{calculate_rate_of_fire(turret.weapon)} sec\n" \
                 f"{turret.weapon.RangeModifier:.0f}\n" \
                 f"{turret.max_range:.0f} km\n" \
                 f"{turret.size:.2f} HS ({round(turret.size * 50)} tons)\n" \
                 f"{turret.hit_to_kill}\n" \
                 f"{turret.weapon.PowerRequirement * turret.number_weapons}\n" \
                 f"{(turret.weapon.RechargeRate * turret.number_weapons):.0f}\n" \
                 f"{turret.cost:.1f}\n" \
                 f"{turret.crew}\n" \
                 f"{round(turret.cost * 10):.0f} RP\n\n\n" \
                 f"{(turret.weapon.Duranium * turret.number_weapons) + (turret.gear_size * 5):.1f}\n"

    for material in ["Neutronium", "Corbomite", "Tritanium", "Boronide", "Mercassium", "Vendarite", "Sorium", "Uridium",
                     "Corundium", "Gallicite"]:

        if material == "Neutronium" and turret.armor_cost:
            infoRight1 = infoRight1 + f"{material}:\n"
            infoRight2 = infoRight2 + f"{turret.armor_cost:.2f}\n"
        elif material in turret.weapon.__dict__.keys() and turret.weapon.__dict__[material]:
            infoRight1 = infoRight1 + f"{material}:\n"
            infoRight2 = infoRight2 + f"{(turret.weapon.__dict__[material] * turret.number_weapons):.2f}\n"

    turret_info_left1.setText(infoLeft1)
    turret_info_left2.setText(infoLeft2)
    turret_info_right1.setText(infoRight1)
    turret_info_right2.setText(infoRight2)


def exit_app():
    sys.exit()
