from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QFormLayout, \
    QLabel, QScrollArea, QGridLayout, QLineEdit, QComboBox, QDialog
from qt_material import QtStyleTools

from app.ui.controller import update_commander_list, get_ranks, get_species
from app.ui.utils.widgets import LoadingDialog


class CommanderSearchListWidget(QWidget):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Commanders")

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_searchbar()

        self.init_commanderlist()

    def init_searchbar(self):
        search_widget = QWidget()
        search_layout = QHBoxLayout()

        filter_button = QPushButton("Filter")
        filter_button.clicked.connect(self.show_filter_dialog)

        self.search_filter = QLineEdit()
        search_button = QPushButton("Search")
        search_button.clicked.connect(lambda x: self.commanderlist.update_list(self.search_filter.text()))

        search_layout.setContentsMargins(5, 5, 5, 5)
        search_layout.addWidget(self.search_filter)
        search_layout.addWidget(search_button)

        search_layout.addWidget(filter_button)

        search_widget.setLayout(search_layout)
        self.layout.addWidget(search_widget)

    def init_commanderlist(self):
        self.commanderlist = CommanderListWidget(self.search_filter)
        self.layout.addWidget(self.commanderlist)

    def show_filter_dialog(self):
        filter_dialog = CommanderSearchForm(self.commanderlist.update_list)
        filter_dialog.exec_()

    def show_load_dialog(self):
        load_dialog = LoadingDialog(self.commanderlist.update_list)
        load_dialog.exec_()

    def update_all(self):
        pass


class CommanderListWidget(QWidget):

    def __init__(self, search_filter: QLineEdit):
        super().__init__()

        self.search_filter = search_filter

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_ui()

    def init_ui(self):
        self.widget = QWidget()
        self.scroll_area = QScrollArea()
        self.scroll_layout = QVBoxLayout()

        self.widget.setLayout(self.scroll_layout)

        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.widget)

        self.layout.addWidget(self.scroll_area)

    def interal_update_list(self, search_filter, progressbar: LoadingDialog):
        update_commander_list(self, search_filter, progressbar)

    def update_list(self, search_filter):

        self.search_filter.setText(search_filter)

        ld = LoadingDialog(self.interal_update_list, search_filter)
        ld.exec_()

    def clear_layout(self):
        while self.scroll_layout.count():
            child = self.scroll_layout.takeAt(0)
            if child and child.widget():
                child.widget().deleteLater()


class CommanderSearchForm(QDialog, QtStyleTools):
    def __init__(self, func):
        super().__init__()
        self.layout = QGridLayout()

        self.func = func

        self.setWindowTitle("Commander Filter")

        self.setLayout(self.layout)
        self.apply_stylesheet(self, theme='light_blue.xml')

        self.init_ui()

        self.fill_boxes()

    def init_ui(self):
        name_label = QLabel("Name:")
        rank_label = QLabel("Rank:")
        species_label = QLabel("Species:")

        confirm_button = QPushButton("Search")
        confirm_button.clicked.connect(self.pass_filter)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.close)

        self.name_text = QLineEdit()
        self.rank_box = QComboBox()
        self.species_box = QComboBox()

        self.layout.addWidget(name_label, 0, 0)
        self.layout.addWidget(self.name_text, 0, 1)
        self.layout.addWidget(rank_label, 1, 0)
        self.layout.addWidget(self.rank_box, 1, 1)
        self.layout.addWidget(species_label, 2, 0)
        self.layout.addWidget(self.species_box, 2, 1)
        self.layout.addWidget(confirm_button, 3, 0)
        self.layout.addWidget(cancel_button, 3, 1)

    def fill_boxes(self):
        get_ranks(self.rank_box)
        get_species(self.species_box)

    def pass_filter(self):

        search_filter = ''
        text = self.name_text.text()
        rank = self.rank_box.currentText()
        species = self.species_box.currentText()

        if text:
            search_filter += f'name={text}'

        if rank:
            if search_filter:
                search_filter += ','

            tmp = rank.split("-")[1].strip()

            search_filter += f'rank={tmp}'

        if species:
            if search_filter:
                search_filter += ','

            search_filter += f'species={species}'

        self.func(search_filter)
        self.close()
