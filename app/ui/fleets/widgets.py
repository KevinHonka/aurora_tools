from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QFormLayout, \
    QLabel, QScrollArea, QDialog, QGridLayout, QComboBox
from qt_material import QtStyleTools

from app.ui.controller import update_fleet_list, get_ship_types
from app.ui.utils.widgets import LoadingDialog


class FleetSearchListWidget(QWidget):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Fleets")

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_searchbar()

        self.init_fleetlist()

    def init_searchbar(self):
        search_widget = QWidget()
        search_layout = QHBoxLayout()

        filter_button = QPushButton("Filter")
        filter_button.clicked.connect(self.show_filter_dialog)

        self.search_filter = QLineEdit()
        search_button = QPushButton("Search")
        search_button.clicked.connect(lambda x: self.fleetlist.update_list(self.search_filter.text()))

        search_layout.setContentsMargins(5, 5, 5, 5)
        search_layout.addWidget(self.search_filter)
        search_layout.addWidget(search_button)

        search_layout.addWidget(filter_button)

        search_widget.setLayout(search_layout)
        self.layout.addWidget(search_widget)

    def init_fleetlist(self):
        self.fleetlist = FleetListWidget(self.search_filter)
        self.layout.addWidget(self.fleetlist)

    def show_filter_dialog(self):
        filter_dialog = FleetSearchForm(self.fleetlist.update_list)
        filter_dialog.exec_()

    def show_load_dialog(self):
        load_dialog = LoadingDialog(self.fleetlist.update_list)
        load_dialog.exec_()

    def update_all(self):
        pass


class FleetListWidget(QWidget):

    def __init__(self, search_filter: QLineEdit):
        super().__init__()

        self.search_filter = search_filter

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_ui()

    def init_ui(self):
        self.widget = QWidget()
        self.scroll_area = QScrollArea()
        self.scroll_layout = QVBoxLayout()

        self.widget.setLayout(self.scroll_layout)

        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.widget)

        self.layout.addWidget(self.scroll_area)

    def interal_update_list(self, search_filter, progressbar: LoadingDialog):
        update_fleet_list(self, search_filter, progressbar)

    def update_list(self, search_filter):

        self.search_filter.setText(search_filter)

        ld = LoadingDialog(self.interal_update_list, search_filter)
        ld.exec_()

    def clear_layout(self):
        while self.scroll_layout.count():
            child = self.scroll_layout.takeAt(0)
            if child and child.widget():
                child.widget().deleteLater()


class FleetForm(QWidget):

    def __init__(self, fleet_name, fleet_data):
        super().__init__()
        self.layout = QFormLayout()

        self.setLayout(self.layout)

        self.process_data(fleet_name, fleet_data)

    def process_data(self, fleet_name, fleet_data):
        self.layout.addRow(QLabel(f"Fleet:"), QLabel(f"{fleet_name}"))

        for ship in fleet_data:
            self.layout.addRow(QLabel(f"- {ship.name} - {ship.shipclass}"))


class FleetSearchForm(QDialog, QtStyleTools):
    def __init__(self, func):
        super().__init__()
        self.layout = QGridLayout()

        self.func = func

        self.setWindowTitle("Fleet Filter")

        self.setLayout(self.layout)
        self.apply_stylesheet(self, theme='light_blue.xml')

        self.init_ui()

        self.fill_boxes()

    def init_ui(self):
        name_label = QLabel("Name:")
        commercial_label = QLabel("Commercial:")
        ship_label = QLabel("Shipclass:")

        confirm_button = QPushButton("Search")
        confirm_button.clicked.connect(self.pass_filter)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.close)

        self.name_text = QLineEdit()
        self.ship_box = QComboBox()
        self.commercial_box = QComboBox()
        self.commercial_box.addItems(['True', 'False'])

        self.layout.addWidget(name_label, 0, 0)
        self.layout.addWidget(self.name_text, 0, 1)
        self.layout.addWidget(ship_label, 1, 0)
        self.layout.addWidget(self.ship_box, 1, 1)
        self.layout.addWidget(commercial_label, 2, 0)
        self.layout.addWidget(self.commercial_box, 2, 1)
        self.layout.addWidget(confirm_button, 3, 0)
        self.layout.addWidget(cancel_button, 3, 1)

    def fill_boxes(self):
        get_ship_types(self.ship_box)

    def pass_filter(self):

        search_filter = ''
        text = self.name_text.text()
        ship_class = self.ship_box.currentText()
        commercial = self.commercial_box.currentText()

        if text:
            search_filter += f'name={text}'

        if commercial:
            if search_filter:
                search_filter += ','

            search_filter += f'commercial={commercial}'

        if ship_class:
            if search_filter:
                search_filter += ','

            search_filter += f'ship_class={ship_class}'

        self.func(search_filter)
        self.close()
