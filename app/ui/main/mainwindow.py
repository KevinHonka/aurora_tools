from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon, QAction, QPixmap
from PySide6.QtWidgets import QMainWindow, QToolBar, QFileDialog, QMessageBox, \
    QStackedWidget, QToolButton, QDialog, QLabel, QVBoxLayout
from qt_material import QtStyleTools
from sqlalchemy.exc import OperationalError, DatabaseError

import config
from app.database import connect
from app.ui.commanders.widgets import CommanderSearchListWidget
from app.ui.components.widgets import ComponentSearchListWidget
from app.ui.config.widgets import ConfigDialog
from app.ui.events.widgets import EventSearchListWidget
from app.ui.export.widgets import ExportDialog
from app.ui.fleets.widgets import FleetSearchListWidget
from app.ui.ships.widgets import ShipSearchListWidget
from app.ui.turrets.widgets import TurretWidget
from config import get_icon


class MainWindow(QMainWindow, QtStyleTools):
    BASE_WINDOW_TITLE = "AuroraTools"

    def __init__(self):
        super().__init__()

        self.apply_stylesheet(self, theme='light_blue.xml')

        self.create_menu()
        self.create_toolbar()
        self.create_centralwidget()

        self.init_interface()

        self.init_config()

    def init_config(self):
        self.config_dialog = ConfigDialog()
        self.show_config()

    def init_interface(self):
        self.setWindowTitle(self.BASE_WINDOW_TITLE)

        self.resize(800, 600)

        self.show()

    def create_centralwidget(self):
        self.central_widget = QStackedWidget()

        # Adding all Widgets to the stack
        self.commander_widget = CommanderSearchListWidget()
        self.event_widget = EventSearchListWidget()
        self.fleet_widget = FleetSearchListWidget()
        self.ship_widget = ShipSearchListWidget()
        self.component_widget = ComponentSearchListWidget()
        self.turret_widget = TurretWidget()

        self.central_widget.addWidget(self.commander_widget)
        self.central_widget.addWidget(self.event_widget)
        self.central_widget.addWidget(self.ship_widget)
        self.central_widget.addWidget(self.fleet_widget)
        self.central_widget.addWidget(self.component_widget)
        self.central_widget.addWidget(self.turret_widget)

        self.setCentralWidget(self.central_widget)

    def create_menu(self):
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu("File")
        config_menu = main_menu.addMenu("Configuration")
        export_menu = main_menu.addMenu("Export (BETA)")
        help_menu = main_menu.addMenu("Help")

        help_action = QAction("Information", self)
        help_action.triggered.connect(self.show_help)

        config_action = QAction("Change Configuration", self)
        config_action.triggered.connect(self.show_config)

        export_empire_action = QAction("Empire", self)
        export_empire_action.triggered.connect(lambda x: self.show_export(export_empire_action.text()))
        export_menu.addAction(export_empire_action)

        export_commander_action = QAction("Commanders", self)
        export_commander_action.triggered.connect(lambda x: self.show_export(export_commander_action.text()))
        export_menu.addAction(export_commander_action)

        export_components_action = QAction("Components", self)
        export_components_action.triggered.connect(lambda x: self.show_export(export_components_action.text()))
        export_menu.addAction(export_components_action)

        export_shipclass_action = QAction("Shipclass", self)
        export_shipclass_action.triggered.connect(lambda x: self.show_export(export_shipclass_action.text()))
        export_menu.addAction(export_shipclass_action)

        export_events_action = QAction("Events", self)
        export_events_action.triggered.connect(lambda x: self.show_export(export_events_action.text()))
        export_menu.addAction(export_events_action)

        help_menu.addAction(help_action)
        config_menu.addAction(config_action)

    def create_toolbar(self):
        left_toolbar = QToolBar('Menu')
        left_toolbar.setMovable(False)

        turret_button = QToolButton()
        turret_button.setIcon(QIcon(QPixmap(get_icon("turret"))))
        turret_button.setToolTip("Turrets")
        turret_button.clicked.connect(lambda x: self.switch_widget(5, 'Turrets', ship_button))

        weapon_button = QToolButton()
        weapon_button.setIcon(QIcon(QPixmap(get_icon("weapons"))))
        weapon_button.setToolTip("Weapons")
        # weapon_button.clicked.connect(lambda x: self.switch_widget(self.weapon_widget))

        event_button = QToolButton()
        event_button.setToolTip("Events")
        event_button.setIcon(QIcon(QPixmap(get_icon("eventlog"))))
        event_button.clicked.connect(lambda x: self.switch_widget(1, 'Events', ship_button))

        commander_button = QToolButton()
        commander_button.setToolTip("Commanders")
        commander_button.setIcon(QIcon(QPixmap(get_icon("commander"))))
        commander_button.clicked.connect(lambda x: self.switch_widget(0, 'Commanders', ship_button))

        ship_button = QToolButton()
        ship_button.setToolTip("Shipclasses")
        ship_button.setIcon(QIcon(QPixmap(get_icon("rocket"))))
        ship_button.clicked.connect(lambda x: self.switch_widget(2, "Shipclasses", ship_button))

        component_button = QToolButton()
        component_button.setToolTip("Components")
        component_button.setIcon(QIcon(QPixmap(get_icon("components"))))
        component_button.clicked.connect(lambda x: self.switch_widget(4, "Components", component_button))

        fleet_button = QToolButton()
        fleet_button.setToolTip("Fleets")
        fleet_button.setIcon(QIcon(QPixmap(get_icon("rockets"))))
        fleet_button.clicked.connect(lambda x: self.switch_widget(3, "Fleets", fleet_button))

        left_toolbar.addWidget(event_button)
        left_toolbar.addSeparator()
        left_toolbar.addWidget(commander_button)
        left_toolbar.addSeparator()
        left_toolbar.addWidget(turret_button)
        left_toolbar.addWidget(weapon_button)
        left_toolbar.addWidget(component_button)
        left_toolbar.addSeparator()
        left_toolbar.addWidget(fleet_button)
        left_toolbar.addWidget(ship_button)

        self.addToolBar(Qt.LeftToolBarArea, left_toolbar)

    def switch_widget(self, index, title, button: QToolButton):
        self.central_widget.setCurrentIndex(index)
        button.focusWidget()
        self.setWindowTitle(f"{self.BASE_WINDOW_TITLE} - {title}")
        widget = self.central_widget.widget(index)
        widget.update_all()

    def show_config(self):
        self.config_dialog.exec_()

    def show_error(self, error):
        error_box = QMessageBox()
        error_box.setWindowTitle("Error")
        error_box.setIcon(QMessageBox.Critical)
        error_box.setText("an error occured while loading the Aurora4x Database")
        error_box.setDetailedText(f"Exception: {error}")

        error_box.exec_()

    def show_export(self, export_name):

        search_filter = None

        try:
            search_filter = self.central_widget.currentWidget().search_filter.text()
        except Exception as ex:
            pass

        export = ExportDialog(export_name, search_filter)
        export.exec_()

    def show_help(self):
        information_box = QDialog()
        information_layout = QVBoxLayout()

        information_box.setWindowTitle("Information")

        information_box.setLayout(information_layout)
        information_label = QLabel("If you need help, please visit:")
        url_label = QLabel("<a href=\"https://gitlab.com/KevinHonka/aurora_tools\">Gitlab Repository</a>")
        url_label.setTextFormat(Qt.RichText)
        url_label.setTextInteractionFlags(Qt.TextBrowserInteraction)
        url_label.setOpenExternalLinks(True)
        information_label.setTextInteractionFlags(Qt.TextSelectableByMouse)

        information_layout.addWidget(information_label)
        information_layout.addWidget(url_label)

        information_box.exec_()

    def get_auroradb(self):
        dlg = QFileDialog(filter="DB files (*.db)")
        dlg.setFileMode(QFileDialog.AnyFile)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            config.DB_PATH = filenames[0]

            try:
                connect()
            except OperationalError as op_error:
                self.show_error(op_error)
            except DatabaseError as db_error:
                self.show_error(db_error.args[0])
