from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLineEdit, QLabel, QWidget, QPushButton, QFileDialog, \
    QComboBox
from qt_material import QtStyleTools

from app.ui.controller import update_config, exit_app, update_config_db, get_games, get_races, get_game_date


class ConfigDialog(QDialog, QtStyleTools):

    def __init__(self):
        super().__init__()

        self.apply_stylesheet(self, theme='light_blue.xml')

        self.setWindowTitle("Configuration")
        self.setWindowModality(Qt.ApplicationModal)

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setWindowFlag(Qt.WindowCloseButtonHint, False)

        self.init_ui()
        self.init_dialog()

    def init_dialog(self):
        self.dlg = QFileDialog(filter="DB files (*.db)")
        self.dlg.setFileMode(QFileDialog.AnyFile)

    def init_ui(self):
        config_label_layout = QVBoxLayout()
        config_field_layout = QVBoxLayout()
        config_layout = QHBoxLayout()

        config_label_widget = QWidget()
        config_field_widget = QWidget()
        config_widget = QWidget()

        config_widget.setLayout(config_layout)

        config_label_widget.setLayout(config_label_layout)
        config_field_widget.setLayout(config_field_layout)

        self.confirm_button = QPushButton("Confirm")
        self.confirm_button.setDisabled(True)
        cancel_button = QPushButton("Cancel")
        button_layout = QHBoxLayout()
        button_widget = QWidget()

        self.confirm_button.clicked.connect(self.update_config)
        cancel_button.clicked.connect(exit_app)

        button_layout.addWidget(self.confirm_button)
        button_layout.addWidget(cancel_button)
        button_layout.setContentsMargins(5, 5, 5, 5)
        button_widget.setLayout(button_layout)

        db_layout = QHBoxLayout()
        self.db_textfield = QLineEdit()
        db_button = QPushButton("...")
        db_button.clicked.connect(self.set_auroradb)
        db_widget = QWidget()
        db_widget.setLayout(db_layout)

        db_layout.addWidget(self.db_textfield)
        db_layout.addWidget(db_button)

        date_label = QLabel("Start date")
        self.date_textfield = QLineEdit()
        self.date_textfield.setReadOnly(True)

        game_label = QLabel("Game")
        self.game_box = QComboBox()
        self.game_box.currentTextChanged.connect(self.update_racebox)
        self.game_box.setDisabled(True)

        config_label_layout.addWidget(game_label)
        config_field_layout.addWidget(self.game_box)

        config_label_layout.addWidget(date_label)
        config_field_layout.addWidget(self.date_textfield)

        race_label = QLabel("Race")
        self.race_box = QComboBox()
        self.race_box.setDisabled(True)

        config_label_layout.addWidget(race_label)
        config_field_layout.addWidget(self.race_box)

        config_layout.addWidget(config_label_widget)
        config_layout.addWidget(config_field_widget)

        self.layout.addWidget(db_widget)
        self.layout.addWidget(config_widget)
        self.layout.addWidget(button_widget)

    def update_config(self):
        db_path = self.db_textfield.text()
        game = self.game_box.currentText()
        race = self.race_box.currentText()
        date = self.date_textfield.text()

        update_config(db_path, game, race, date)
        self.close()

    def update_confirm_button(self):

        if not self.race_box.currentText() or not self.game_box.currentText() or not self.db_textfield.text():
            self.confirm_button.setDisabled(True)
        else:
            self.confirm_button.setDisabled(False)

    def update_racebox(self):

        game_text = self.game_box.currentText()

        if game_text:
            get_races(self.race_box, game_text)
            self.update_dateline()
            self.race_box.setDisabled(False)
            self.update_confirm_button()

    def update_gamebox(self):
        get_games(self.game_box)
        self.game_box.setDisabled(False)
        self.update_confirm_button()

    def update_dateline(self):
        game_text = self.game_box.currentText()

        if game_text:
            get_game_date(self.date_textfield, game_text)

    def set_auroradb(self):
        if self.dlg.exec_():
            filenames = self.dlg.selectedFiles()
            self.db_textfield.setText(filenames[0])

            update_config_db(self.db_textfield.text())
            self.update_gamebox()
            self.update_confirm_button()
