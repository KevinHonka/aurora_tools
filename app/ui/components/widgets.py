from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLineEdit, QPushButton, QFormLayout, \
    QLabel, QScrollArea, QDialog, QGridLayout, QComboBox
from qt_material import QtStyleTools

from app.ui.controller import update_component_list
from app.ui.utils.widgets import LoadingDialog


class ComponentSearchListWidget(QWidget):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Components")

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_searchbar()

        self.init_componentlist()

    def init_searchbar(self):
        search_widget = QWidget()
        search_layout = QHBoxLayout()

        filter_button = QPushButton("Filter")
        filter_button.clicked.connect(self.show_filter_dialog)

        self.search_filter = QLineEdit()
        search_button = QPushButton("Search")
        search_button.setShortcut('Enter')
        search_button.clicked.connect(lambda x: self.componentlist.update_list(self.search_filter.text()))

        search_layout.addWidget(self.search_filter)
        search_layout.addWidget(search_button)

        search_layout.addWidget(filter_button)

        search_widget.setLayout(search_layout)
        self.layout.addWidget(search_widget)

    def init_componentlist(self):
        self.componentlist = ComponentListWidget(self.search_filter)
        self.layout.addWidget(self.componentlist)

    def show_filter_dialog(self):
        filter_dialog = ComponentSearchForm(self.componentlist.update_list)
        filter_dialog.exec_()

    def show_load_dialog(self):
        load_dialog = LoadingDialog(self.componentlist.update_list)
        load_dialog.exec_()

    def update_all(self):
        pass


class ComponentListWidget(QWidget):

    def __init__(self, search_filter: QLineEdit):
        super().__init__()

        self.search_filter = search_filter

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.init_ui()

    def init_ui(self):
        self.widget = QWidget()
        self.scroll_area = QScrollArea()
        self.scroll_layout = QVBoxLayout()

        self.widget.setLayout(self.scroll_layout)

        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.widget)

        self.layout.addWidget(self.scroll_area)

    def interal_update_list(self, search_filter, progressbar: LoadingDialog):
        update_component_list(self, search_filter, progressbar)

    def update_list(self, search_filter):

        self.search_filter.setText(search_filter)

        ld = LoadingDialog(self.interal_update_list, search_filter)
        ld.exec_()

    def clear_layout(self):
        while self.scroll_layout.count():
            child = self.scroll_layout.takeAt(0)
            if child and child.widget():
                child.widget().deleteLater()


class ComponentSearchForm(QDialog, QtStyleTools):
    def __init__(self, func):
        super().__init__()
        self.layout = QGridLayout()

        self.func = func

        self.setWindowTitle("Component Filter")

        self.setLayout(self.layout)
        self.apply_stylesheet(self, theme='light_blue.xml')

        self.init_ui()

    def init_ui(self):
        name_label = QLabel("Name:")
        obsolete_label = QLabel("Obsolete:")

        confirm_button = QPushButton("Search")
        confirm_button.clicked.connect(self.pass_filter)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.close)

        self.name_text = QLineEdit()
        self.obsolete_box = QComboBox()
        self.obsolete_box.addItems(["True", "False"])

        self.layout.addWidget(name_label, 0, 0)
        self.layout.addWidget(self.name_text, 0, 1)
        self.layout.addWidget(obsolete_label, 1, 0)
        self.layout.addWidget(self.obsolete_box, 1, 1)
        self.layout.addWidget(confirm_button, 3, 0)
        self.layout.addWidget(cancel_button, 3, 1)

    def pass_filter(self):

        search_filter = ''
        text = self.name_text.text()
        obsolete = self.obsolete_box.currentText()

        if text:
            search_filter += f'name={text}'

        if obsolete:
            if search_filter:
                search_filter += ','

            search_filter += f'obsolete={obsolete}'

        self.func(search_filter)
        self.close()
