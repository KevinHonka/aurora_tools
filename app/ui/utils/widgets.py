from PySide6.QtCore import QTimer, Qt
from PySide6.QtWidgets import QProgressDialog, QLabel
from qt_material import QtStyleTools


class LoadingDialog(QProgressDialog, QtStyleTools):

    def __init__(self, func, search_filter=None):
        super().__init__()
        self.setWindowTitle("Loading...")
        self.search_filter = search_filter
        self.setFixedSize(500, 300)
        self.timer = QTimer()
        self.timer.singleShot(20, self.run_func)
        self.func = func
        self.setMinimum(0)
        self.setValue(1)
        self.setCancelButton(None)

        self.apply_stylesheet(self, theme='light_blue.xml')

    def run_func(self):
        self.func(self.search_filter, self)
        self.close()


class Form(QLabel):

    def __init__(self, data):
        super().__init__()

        self.setTextInteractionFlags(Qt.TextSelectableByMouse)

        self.setText(data)
