from PySide6.QtCore import Qt
from PySide6.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QWidget, QPushButton, QComboBox, QFileDialog, \
    QLineEdit
from qt_material import QtStyleTools

from app.ui.controller import get_templates, do_export


class ExportDialog(QDialog, QtStyleTools):

    def __init__(self, export_type=None, search_filter=None):
        super().__init__()

        self.apply_stylesheet(self, theme='light_blue.xml')

        self.export_type = export_type
        self.search_filter = search_filter

        self.setWindowTitle(f"Export {export_type}")
        self.setWindowModality(Qt.ApplicationModal)

        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setWindowFlag(Qt.WindowCloseButtonHint, False)

        self.init_ui()

    def init_ui(self):
        config_label_layout = QVBoxLayout()
        config_field_layout = QVBoxLayout()
        config_layout = QHBoxLayout()

        config_label_widget = QWidget()
        config_field_widget = QWidget()
        config_widget = QWidget()

        config_widget.setLayout(config_layout)

        config_label_widget.setLayout(config_label_layout)
        config_field_widget.setLayout(config_field_layout)

        self.confirm_button = QPushButton("Confirm")
        self.confirm_button.setDisabled(True)
        cancel_button = QPushButton("Cancel")
        button_layout = QHBoxLayout()
        button_widget = QWidget()

        self.confirm_button.clicked.connect(self.export)
        cancel_button.clicked.connect(self.close)

        button_layout.addWidget(self.confirm_button)
        button_layout.addWidget(cancel_button)
        button_layout.setContentsMargins(5, 5, 5, 5)
        button_widget.setLayout(button_layout)

        export_layout = QHBoxLayout()
        self.export_dir_line = QLineEdit()
        self.export_dir_line.setReadOnly(True)
        self.export_dir_line.textChanged.connect(self.update_confirm_button)
        export_button = QPushButton("...")
        export_button.clicked.connect(self.set_exportdir)
        export_widget = QWidget()
        export_widget.setLayout(export_layout)

        export_layout.addWidget(self.export_dir_line)
        export_layout.addWidget(export_button)

        template_label = QLabel("Template:")
        self.template_box = QComboBox()
        self.template_box.currentTextChanged.connect(self.update_confirm_button())
        self.template_box.addItems(get_templates(self.export_type.lower()))

        config_label_layout.addWidget(template_label)
        config_field_layout.addWidget(self.template_box)

        config_layout.addWidget(config_label_widget)
        config_layout.addWidget(config_field_widget)

        self.layout.addWidget(export_widget)
        self.layout.addWidget(config_widget)
        self.layout.addWidget(button_widget)

    def update_confirm_button(self):

        if not self.template_box.currentText() or not self.export_dir_line.text():
            self.confirm_button.setDisabled(True)
        else:
            self.confirm_button.setDisabled(False)

    def export(self):
        do_export(self.export_dir_line.text(), self.template_box.currentText(), self.search_filter)
        self.close()

    def set_exportdir(self):

        path = QFileDialog.getExistingDirectory(self, "Choose Export Directory")

        self.export_dir_line.setText(path)
